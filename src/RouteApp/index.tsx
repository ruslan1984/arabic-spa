import React, { FC } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import AuthScreen from "@screens/AuthScreen";
import AdminApp from "@adminScreen/AdminApp";

import Settings from "@screens/Admin/Settings/Settings";
//import AuthRoute from "./AuthRoute";
//import AdminRoute from "./AdminRoute";

import GrammarBase from "@/screens/Admin/Grammar/Detail/Base";
import GrammarTestCard from "@grammarScreen/Detail/Test/Card";
import GrammarTestList from "@grammarScreen/Detail/Test/List";
import GrammarTestBasket from "@grammarScreen/Detail/Test/Basket";
import GrammarTesting from "@grammarScreen/Detail/Test/Testing";
import GrammarWords from "@grammarScreen/Detail/Words";
import GrammarArabicText from "@grammarScreen/Detail/ArabicText";
import GrammarList from "@/screens/Admin/Grammar/List";
import GrammarSettings from "@grammarScreen/Settings/GrammarSettings";

import OrthographyList from "@orthographyScreen/List";
import OrthographyBase from "@orthographyScreen/Detail/Base";
import OrthographyHarf from "@orthographyScreen/Detail/Harf";
import OrthographySound from "@orthographyScreen/Detail/Sound";

import routes from "./routes";

//import history from "./history";

export const RouteApp: FC = () => {
  return (
    //<Router history={history}>
    <BrowserRouter>
      <Routes>
        {String(routes.admin())}
        <Route path={routes.auth()} element={<AuthScreen />} />

        <Route path={routes.admin()} element={<AdminApp />} />
        <Route path={routes.grammar.List()} element={<GrammarList />} />
        <Route path={routes.grammar.Base()} element={<GrammarBase />} />
        {/*<Route
          path={routes.grammar.Basket()}
          element={<GrammarBasket/>}
        />*/}
        <Route path={routes.grammar.Settings()} element={<GrammarSettings />} />

        <Route path={routes.grammar.TestList()} element={<GrammarTestList />} />
        <Route path={routes.grammar.Testing()} element={<GrammarTesting />} />
        <Route
          path={routes.grammar.TestBasket()}
          element={<GrammarTestBasket />}
        />
        <Route path={routes.grammar.TestCard()} element={<GrammarTestCard />} />
        <Route path={routes.grammar.Words()} element={<GrammarWords />} />
        <Route
          path={routes.grammar.ArabicText()}
          element={<GrammarArabicText />}
        />

        <Route path={routes.orthography.List()} element={<OrthographyList />} />

        <Route path={routes.orthography.Id()} element={<OrthographyBase />} />
        <Route path={routes.orthography.Harf()} element={<OrthographyHarf />} />
        <Route
          path={routes.orthography.Sound()}
          element={<OrthographySound />}
        />

        <Route path={routes.admin()} element={<AdminApp />} />
        <Route path={routes.admin()} element={<AdminApp />} />

        <Route path="/admin/settings" element={<Settings />} />
        {/*<Navigate to={routes.admin()} />*/}
      </Routes>
    </BrowserRouter>
    //</Router>
  );
};

export default RouteApp;
