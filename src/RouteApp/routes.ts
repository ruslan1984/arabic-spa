const auth = "/auth";
const admin = "/admin";
const grammar = "/grammar";
const base = "/base";
const basket = "/basket";
const settings = "/settings";
const test = "/test";
const testing = "/testing";
const words = "/words";
const questions = "/questions";
const arabic_text = "/arabic_text";

const orthography = "/orthography";
const harf = "/harf";
const sound = "/sound";

export default {
  auth: (): string => auth,
  admin: (): string => admin,

  grammar: {
    List: (): string => admin + grammar,
    Base: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + base,
    Id: (id: string | number = ":id"): string => admin + grammar + "/" + id,
    Basket: (): string => admin + grammar + basket,
    Settings: (): string => admin + grammar + settings,
    TestList: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + test,
    TestBasket: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + basket,
    Testing: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + testing,
    TestCard: (
      id: string | number = ":id",
      testId: string | number = ":testId"
    ): string => admin + grammar + "/" + id + test + "/" + testId,
    TestCreate: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + test + "/" + 0,
    Words: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + words,
    Questions: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + questions,
    ArabicText: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + arabic_text,
    WordsBasket: (id: string | number = ":id"): string =>
      admin + grammar + "/" + id + words,
  },

  orthography: {
    List: (): string => admin + orthography,
    Base: (id: string | number = ":id"): string =>
      admin + orthography + "/" + id + base,
    Id: (id: string | number = ":id"): string => admin + orthography + "/" + id,
    Harf: (id: string | number = ":id"): string =>
      admin + orthography + "/" + id + harf,
    Sound: (id: string | number = ":id"): string =>
      admin + orthography + "/" + id + sound,
  },
};
