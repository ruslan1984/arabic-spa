import React, { FC, ReactNode } from "react";
import { Route, Navigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { isAuth } from "@helpers/tokens";
import { CheckState } from "@modules/Auth/types";
import routes from "@routes/routes";
import { actions } from "@modules/Auth/reducer";

interface IAuthRoute {
  exact?: boolean;
  children?: ReactNode;
  path: string;
  component: () => JSX.Element | any;
  computedMatch?: any;
  url?: string;
}

export const AuthRoute: FC<IAuthRoute> = ({
  component: Component,
  ...rest
}: IAuthRoute): any => {
  const dispatch = useDispatch();
  const { status } = useSelector((state: any) => state.auth);

  //if (status !== CheckState.succeed && isAuth()) {
  //  dispatch(actions.login());
  //}

  return isAuth() ? (
    <Navigate to={routes.admin()} />
  ) : (
    <Route path={rest.path} element={<Component />} />
  );
};

export default AuthRoute;
