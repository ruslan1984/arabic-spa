import { combineReducers } from "redux";
//import { createStore } from "redux-dynamic-modules";
//import { getSagaExtension } from "redux-dynamic-modules-saga";
import { reducer as authReducer } from "@modules/Auth/reducer";
import { reducer as grammarList } from "@modules/Grammar/List/reducer";
//import { reducer as grammarDetail } from "@grammar/Detail/reducer";
import { reducer as grammarDetailBase } from "@modules/Grammar/Detail/Base/reducer";
import { reducer as testList } from "@modules/Test/TestList/reducer";
import { reducer as testCard } from "@modules/Test/TestCard/reducer";
import { reducer as words } from "@modules/Words/reducer";
import { reducer as questionsGrammarReducer } from "@components/Questions/reducer";
import { reducer as pageSettingsReducer } from "@components/Settings/reducer";
import { reducer as orthographyList } from "@modules/Orthography/List/reducer";
import { reducer as orthographyDetailBase } from "@modules/Orthography/Detail/Base/reducer";
import { reducer as orthographyDetailHarf } from "@modules/Orthography/Detail/Harf/reducer";
import { reducer as orthographyDetailSound } from "@modules/Orthography/Detail/Sound/reducer";
import { reducer as grammarArabicSentence } from "@modules/Grammar/Detail/ArabicSentence/reducer";

import { reducer as settingsReducer } from "@screens/Admin/Settings/reducer";
//import { getLoginModule } from "@modules/Auth/modules";
//import { grammarTestListModule } from "@components/Test/QusetionList/modules";
//import { grammarTestCardModule } from "@modules/Test/TestCard/modules";

//export const store = createStore(
//  {
//    extensions: [getSagaExtension({})],
//  },
//  getLoginModule(),
//  //grammarTestListModule(),
//  grammarTestCardModule()
//);

export const reducer = combineReducers({
  grammarList,
  //grammarDetail,
  grammarDetailBase,
  grammarArabicSentence,
  auth: authReducer,
  testList,
  testCard,
  words,
  questionsGrammarReducer,
  pageSettingsReducer,
  orthographyList,
  orthographyDetailBase,
  orthographyDetailHarf,
  orthographyDetailSound,
  settingsReducer,
});

export type reducerType = ReturnType<typeof reducer>;
export default reducer;
