import { useMemo } from "react";
import { reducer } from "@modules/Grammar/List/reducer";

import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { fork } from "redux-saga/effects";

// import { loginSaga } from "@auth/saga";
import { grammarListSaga } from "@modules/Grammar/List/saga";
import { grammarDetailSaga } from "@modules/Grammar/Detail/Base/saga";
//import { testQuestionDetailSaga } from "@components/Test/QuestionCard/saga";
//import { testQuestionListSaga } from "@components/Test/QuestionList/saga";
//import { grammarTestingSaga } from "@components/Test/Testing/saga";

let store: any;
const initialState = {
  list: [],
};

function initStore(preloadedState = initialState) {
  const sagaMiddleware = createSagaMiddleware();
  const store = configureStore({
    reducer,
    middleware: [sagaMiddleware],
  });

  function* rootSaga() {
    // yield fork(loginSaga);
    yield fork(grammarListSaga);
    yield fork(grammarDetailSaga);
    //yield fork( testQuestionDetailSaga );
    //yield fork( testQuestionListSaga );
    //yield fork( grammarTestingSaga );
  }
  sagaMiddleware.run(rootSaga);
  return store;
}

export const initializeStore = (preloadedState: any) => {
  let _store = store ?? initStore(preloadedState);
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    });
    store = undefined;
  }
  if (typeof window === "undefined") return _store;
  if (!store) store = _store;
  return _store;
};

export function useStore(initialState: any) {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
}
