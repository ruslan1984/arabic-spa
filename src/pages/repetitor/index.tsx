import { fetchGet } from "@site/fetch";
import Repetitor, { IRepetitor } from "@site/SitePages/Repetitor";

export default function repetitor({
  seo,
  contact,
  menu,
}: IRepetitor): JSX.Element {
  return <Repetitor seo={seo} contact={contact} menu={menu} url="repetitor" />;
}

export const getStaticProps = async () => {
  try {
    const fetchData = await fetchGet(`/api/tutor_page`);
    return {
      props: fetchData.body,
    };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};
