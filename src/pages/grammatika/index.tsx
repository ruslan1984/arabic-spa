import React from "react";
import { fetchGet } from "@site/fetch";
import GrammarList, { IGrammarList } from "@site/Grammar/List";

export default function grammarList({
  seo,
  list,
  contact,
  menu,
  url,
}: IGrammarList): JSX.Element {
  return (
    <GrammarList
      list={list}
      seo={seo}
      url={url}
      contact={contact}
      menu={menu}
    />
  );
}

export const getStaticProps = async () => {
  try {
    const fetchData = await fetchGet("/api/grammatika");
    return { props: fetchData.body };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};
