import React from "react";
import { fetchGet } from "@site/fetch";
import GrammarDetail, { IGrammarDetail } from "@site/Grammar/Detail";

export function grammarDetail({
  seo,
  list,
  contact,
  menu,
  page,
  currentPage,
  nextPage,
  scrollMenu,
  detail,
  test,
  words,
  questions,
}: IGrammarDetail): JSX.Element {
  return (
    <GrammarDetail
      seo={seo}
      list={list}
      page={page}
      currentPage={currentPage}
      nextPage={nextPage}
      scrollMenu={scrollMenu}
      contact={contact}
      menu={menu}
      detail={detail}
      test={test}
      words={words}
      questions={questions}
    />
  );
}

export const getStaticPaths = async () => {
  try {
    const fetchData = await fetchGet("/api/grammatika");
    const paths = fetchData.body.list.map((item: { code: string }) => ({
      params: { code: item.code },
    }));
    return { paths, fallback: false };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export const getStaticProps = async (data: { params: { code: string } }) => {
  try {
    const code = data.params.code;
    const fetchData = await fetchGet(`/api/grammatika/${code}`);
    return { props: { ...fetchData.body, currentPage: code } };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export default grammarDetail;
