import React from "react";
import { fetchGet } from "@site/fetch";
import MyWords from "@site/SitePages/MyWords";
import {
  TSeo,
  TArabicWordsData,
  TMobileMenu,
  TContact,
} from "@site/SitePages/ArabicWords/types";

interface IArabicTextList {
  data: {
    url: string;
    seo: TSeo;
    data: TArabicWordsData;
    menu: TMobileMenu[];
    contact: TContact;
  };
}

export function arabicTextList({
  data: { url, seo, menu, contact },
}: IArabicTextList): JSX.Element {
  return <MyWords contact={contact} seo={seo} url={url} menu={menu} />;
}

export const getStaticProps = async () => {
  try {
    const fetchData = await fetchGet("/api/arabic_word_list/1");
    return { props: { ["data"]: { ...fetchData.body, currentPage: 1 } } };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export default arabicTextList;
