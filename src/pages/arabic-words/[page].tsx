import React from "react";
import { fetchGet } from "@site/fetch";
import ArabicWords from "@site/SitePages/ArabicWords";
import {
  TSeo,
  TArabicWordsData,
  TMobileMenu,
  TContact,
} from "@site/SitePages/ArabicWords/types";

interface IArabicTextList {
  url: string;
  seo: TSeo;
  data: TArabicWordsData;
  menu: TMobileMenu[];
  contact: TContact;
  code?: string;
}

export function arabicTextList({
  url,
  seo,
  data,
  menu,
  contact,
  code,
}: IArabicTextList): JSX.Element {
  return (
    <ArabicWords
      contact={contact}
      seo={seo}
      url={url}
      data={data}
      menu={menu}
      code={code}
    />
  );
}

export const getStaticPaths = async () => {
  try {
    const { body: pagesCount } = await fetchGet(
      "/api/arabic_word_list_pages_count/3"
    );
    const paths = Array.from(Array(pagesCount).keys()).map((item: number) => ({
      params: { page: String(item + 1) },
    }));
    return { paths: [...paths], fallback: false };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export const getStaticProps = async (data: { params?: any }) => {
  try {
    const page = data.params.page;

    const fetchData = await fetchGet(`/api/arabic_word_list/${page}`);
    return { props: { ...fetchData.body, code: page } };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export default arabicTextList;
