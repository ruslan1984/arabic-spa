import React from "react";
import { fetchGet } from "@site/fetch";
import OrthographyDetail, {
  IOrthographyDetail,
} from "@site/Orthography/Detail";
import Alphabet from "@site/Orthography/Alphabet";

export function orthographyDetail({
  seo,
  list,
  currentPage,
  nextPage,
  page,
  scrollMenu,
  detail,
  contact,
  menu,
}: IOrthographyDetail): JSX.Element {
  if (currentPage == "arabskiy-alfavit") {
    return (
      <Alphabet
        seo={seo}
        list={list}
        currentPage={currentPage}
        nextPage={nextPage}
        page={page}
        scrollMenu={scrollMenu}
        contact={contact}
        isAlphabet={detail.isAlphabet}
        menu={menu}
        detail={detail}
      />
    );
  }
  return (
    <OrthographyDetail
      seo={seo}
      list={list}
      currentPage={currentPage}
      nextPage={nextPage}
      page={page}
      scrollMenu={scrollMenu}
      contact={contact}
      isAlphabet={detail.isAlphabet}
      menu={menu}
      detail={detail}
    />
  );
}

export const getStaticPaths = async () => {
  try {
    const fetchData = await fetchGet("/api/arabskie-bukvy");
    const paths = fetchData.body.list.map((item: { code: string }) => ({
      params: { code: item.code },
    }));
    return { paths, fallback: false };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export const getStaticProps = async (data: { params: { code: string } }) => {
  try {
    const code = data.params.code;
    const fetchData = await fetchGet(`/api/arabskie-bukvy/${code}`);
    return { props: { ...fetchData.body, currentPage: code } };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export default orthographyDetail;
