import React from "react";
import { fetchGet } from "@site/fetch";
import OrthographyList, { IOrthographyList } from "@site/Orthography/List";

export default function orphographyList({
  seo,
  list,
  contact,
  menu,
  url,
}: IOrthographyList): JSX.Element {
  return (
    <OrthographyList
      list={list}
      seo={seo}
      url={url}
      contact={contact}
      menu={menu}
    />
  );
}

export const getStaticProps = async () => {
  try {
    const fetchData = await fetchGet("/api/arabskie-bukvy");
    return { props: fetchData.body };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};
