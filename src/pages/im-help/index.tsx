import React from "react";
import { fetchGet } from "@site/fetch";
import ImHelp from "@site/SitePages/ImHelp";
import { TSeo, TMobileMenu, TContact } from "@site/types";

interface IImHelp {
  url: string;
  seo: TSeo;
  menu: TMobileMenu[];
  contact: TContact;
}

export default function ImHelpSadaca({
  seo,
  menu,
  contact,
}: IImHelp): JSX.Element {
  return <ImHelp seo={seo} menu={menu} contact={contact} url="im-help" />;
}

export const getStaticProps = async (data: any) => {
  try {
    const fetchData = await fetchGet(`/api/tutor_page`);
    return {
      props: fetchData.body,
    };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};
