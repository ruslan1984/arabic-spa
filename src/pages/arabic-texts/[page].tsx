import React from "react";
import { fetchGet } from "@site/fetch";
import ArabicTexts from "@site/SitePages/ArabicTexts";
import {
  TSeo,
  TArabicTextsData,
  TMobileMenu,
  TContact,
} from "@site/SitePages/ArabicTexts/types";

interface IArabicTextList {
  url: string;
  seo: TSeo;
  data: TArabicTextsData;
  menu: TMobileMenu[];
  contact: TContact;
}

export function arabicTextList({
  url,
  seo,
  data,
  menu,
  contact,
}: IArabicTextList): JSX.Element {
  return (
    <ArabicTexts
      contact={contact}
      seo={seo}
      url={url}
      data={data}
      menu={menu}
    />
  );
}

export const getStaticPaths = async () => {
  try {
    const { body: pagesCount } = await fetchGet(
      "/api/arabic_text_list_pages_count/3"
    );
    const paths = Array.from(Array(pagesCount).keys()).map((item: number) => ({
      params: { page: String(item + 1) },
    }));
    return { paths: [...paths], fallback: false };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export const getStaticProps = async (data: {
  params?: any;
  preview?: boolean;
  previewData?: any;
}) => {
  try {
    const page = data.params.page;
    const fetchData = await fetchGet(`/api/arabic_text_list/${page}`);
    return { props: { ...fetchData.body, currentPage: page } };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export default arabicTextList;
