import React from "react";
import { fetchGet } from "@site/fetch";
import ArabicTexts from "@site/SitePages/ArabicTexts";
import {
  TSeo,
  TArabicTextsData,
  TMobileMenu,
  TContact,
} from "@site/SitePages/ArabicTexts/types";

interface IArabicTextList {
  data: {
    url: string;
    seo: TSeo;
    data: TArabicTextsData;
    menu: TMobileMenu[];
    contact: TContact;
  };
}

export function arabicTextList({
  data: { url, seo, data, menu, contact },
}: IArabicTextList): JSX.Element {
  return (
    <ArabicTexts
      contact={contact}
      seo={seo}
      url={url}
      data={data}
      menu={menu}
    />
  );
}

export const getStaticProps = async () => {
  try {
    const fetchData = await fetchGet("/api/arabic_text_list/1");
    return { props: { ["data"]: { ...fetchData.body, currentPage: 1 } } };
  } catch (err: unknown) {
    console.error((err as Error).message);
  }
};

export default arabicTextList;
