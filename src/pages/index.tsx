import Index, { IIndex } from "@site/Index";
import { fetchGet } from "@site/fetch";

export default function indexPage({ menu, seo }: IIndex) {
  return <Index seo={seo} menu={menu} />;
}

export const getStaticProps = async () => {
  try {
    const fetchData = await fetchGet(`/api/index`);
    return {
      props: { ...fetchData.body, currentPage: 1 },
    };
  } catch (err: any) {
    console.error(err?.message);
  }
};
