import React from "react";
import { fetchGet } from "@site/fetch";
import Page404 from "@site/SitePages/404Page";

export default function indexPage(props: any) {
  return <Page404 {...props.data} />;
}

export const getStaticProps = async () => {
  try {
    const code = 1;
    const fetchData = await fetchGet(`/api/index`);
    return {
      props: { data: { ...fetchData.body, currentPage: code } },
    };
  } catch (err: any) {
    console.error(err?.message);
  }
};
