import React, { FC } from "react";
import { RouteApp } from "./RouteApp";

const App: FC = () => {
  return <RouteApp />;
};

export default App;
