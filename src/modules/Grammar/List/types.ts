export enum Loading {
  start,
  ok,
}
export type GrammarList = Array<GrammarListItem>;
export type GrammarListItem = {
  id: number;
  name: string;
  code: string;
  sort: number;
};
export type GrammarListType = {
  list: GrammarList;
  loading: Loading;
};
