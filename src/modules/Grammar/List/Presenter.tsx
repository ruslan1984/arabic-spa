import React, { FC } from "react";
import { Page, ListLink, H1 } from "@components/Elements/elements";
import { GrammarList, Loading } from "./types";
import routes from "@routes/routes";
import { Grid } from "./elements";

interface ListProps {
  list: GrammarList;
  loading: Loading;
}

const Presenter: FC<ListProps> = (props: ListProps) => {
  return (
    <Page loading={props.loading}>
      <H1>Грамматика список</H1>
      <hr />
      <Grid>
        {props.list.map((item) => (
          <React.Fragment key={item.id.toString()}>
            <ListLink to={routes.grammar.Base(item.id)}>{item.name}</ListLink>
            <ListLink to={routes.grammar.TestList(item.id)}>Тесты</ListLink>
            <ListLink to={routes.grammar.ArabicText(item.id)}>
              Арабский текст
            </ListLink>
          </React.Fragment>
        ))}
      </Grid>
    </Page>
  );
};

export default Presenter;
