import React, { Component } from "react";
import Presenter from "./Presenter";
import { connect } from "react-redux";
import { setBasket } from "./reducer";
import { GrammarList, Loading } from "./types";
import { reducerType } from "@store/reducers";

interface GrammarListProps {
  list: GrammarList;
  setBasket: () => void;
  loading: Loading;
}

export class GrammarBasket extends Component<GrammarListProps> {
  async componentDidMount() {
    await this.props.setBasket();
  }
  render() {
    return <Presenter list={this.props.list} loading={this.props.loading} />;
  }
}

const mapStateToProps = (state: reducerType) => {
  const { list, loading } = state.grammarList;
  return {
    list,
    loading,
  };
};
const mapDispatchToProps = {
  setBasket,
};

export default connect(mapStateToProps, mapDispatchToProps)(GrammarBasket);
