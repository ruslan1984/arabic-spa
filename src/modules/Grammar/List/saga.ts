import { takeEvery, put, call } from "redux-saga/effects";
import { actions, setList, setBasket } from "./reducer";
import { fetchGet } from "@helpers/fetch";
import { Loading, GrammarListItem } from "./types";

export function* setListSaga(url: string) {
  try {
    yield put(actions.setLoading(Loading.start));
    console.log("url", url);
    const fetchData: GrammarListItem[] = yield call(
      async () => await fetchGet(url)
    );
    yield put(actions.setData(fetchData));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* setGramarList() {
  yield setListSaga("/api/grammar");
}

export function* setBasketGramarList() {
  yield setListSaga("/api/grammar/basket");
}

export function* grammarListSaga() {
  yield takeEvery(setList, setGramarList);
  yield takeEvery(setBasket, setBasketGramarList);
}
