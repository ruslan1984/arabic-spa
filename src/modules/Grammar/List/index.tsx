import React, { useEffect } from "react";
import Presenter from "./Presenter";
import { setList } from "./reducer";
import { useSelector, useDispatch } from "react-redux";
import { reducerType } from "@store/reducers";

export const List = () => {
  const { list, loading } = useSelector(
    (state: reducerType) => state.grammarList
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setList());
  }, []);

  return <Presenter list={list} loading={loading} />;
};

export default List;
