import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import { Loading } from "@components/types";
import { TArabicWord, TInputName, TArabicSentenceReducer } from "./types";

const emptyData = {
  arabicSentence: [],
  hideArabicSentence: true,
};

const defaultState: TArabicSentenceReducer = {
  saving: Loading.no,
  loading: Loading.no,
  data: emptyData,
};
const emptyItem = {
  ar: "",
  rus: "",
  comment: "",
};
export const requestArabicSentence = createAction<number>(
  "requestArabicSentence"
);
export const saveArabicSentence = createAction<number>("saveArabicSentence");

export const arabicSentenceSlice = createSlice({
  name: "arabicSentence",
  initialState: defaultState,
  reducers: {
    setData: (state, { payload }: PayloadAction<TArabicWord[]>) => {
      const data = { ...state.data, arabicSentence: payload || [] };
      return { ...state, data };
    },
    setItem: (
      state,
      {
        payload: { id, name, value },
      }: PayloadAction<{
        id: number;
        name: string;
        value: string;
      }>
    ) => {
      const currentArabicSentence: TArabicWord[] = [
        ...state.data.arabicSentence,
      ];
      const arabicSentence: TArabicWord[] | undefined =
        currentArabicSentence?.map((item: TArabicWord) => {
          if (Number(id) === Number(item.id)) {
            return { ...item, [name as TInputName]: value };
          }
          return item;
        });
      const data = { ...state.data, arabicSentence };
      return { ...state, data: data };
    },

    setLoading: (state, { payload }: PayloadAction<Loading>) => ({
      ...state,
      loading: payload,
    }),
    setSaving: (state, { payload }: PayloadAction<Loading>) => ({
      ...state,
      saving: payload,
    }),
    addItem: (
      state,
      {
        payload: { id },
      }: PayloadAction<{
        id: number;
      }>
    ) => {
      const arabicSentence: TArabicWord[] = [...state.data.arabicSentence];
      const index = arabicSentence.findIndex((item) => item.id === id);
      const maxId: number = arabicSentence.reduce(
        (max, current) => (max > current.id ? max : current.id),
        0
      );
      id === 0
        ? arabicSentence.unshift({ ...emptyItem, id: maxId + 1 })
        : arabicSentence.splice(index + 1, 0, { ...emptyItem, id: maxId + 1 });
      const data = { ...state.data, arabicSentence };
      return { ...state, data: data };
    },
    deleteItem: (
      state,
      {
        payload: { id },
      }: PayloadAction<{
        id: number;
      }>
    ) => {
      const currentArabicSentence: TArabicWord[] = [
        ...state.data.arabicSentence,
      ];
      const arabicSentence: TArabicWord[] | undefined =
        currentArabicSentence?.map((item: TArabicWord) => {
          if (Number(id) === Number(item.id)) {
            return { ...item, deleted: !item?.deleted };
          }
          return item;
        });
      const data = { ...state.data, arabicSentence };
      return { ...state, data: data };
    },
    pasteFromBuffer: (
      state,
      {
        payload: { bufferText },
      }: PayloadAction<{
        bufferText: string;
      }>
    ) => {
      const currentArabicSentence: TArabicWord[] = [
        ...state.data.arabicSentence,
      ];
      const maxId: number = currentArabicSentence.reduce(
        (max, current) => (max > current.id ? max : current.id),
        0
      );
      const words = bufferText.split(" ");
      const newWords: TArabicWord[] = words.map(
        (ar: string, index: number) => ({
          ...emptyItem,
          ar,
          id: maxId + index + 1,
        })
      );
      const arabicSentence: TArabicWord[] = [
        ...currentArabicSentence,
        ...newWords,
      ];
      const data = { ...state.data, arabicSentence };
      return { ...state, data };
    },
    setHideArabicSentence: (
      state,
      { payload: hideArabicSentence }: PayloadAction<boolean>
    ) => {
      const data = { ...state.data, hideArabicSentence };
      return { ...state, data };
    },
  },
});
export const { actions, reducer } = arabicSentenceSlice;
