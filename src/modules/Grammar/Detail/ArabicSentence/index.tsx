import React, {
  FC,
  useEffect,
  useState,
  useCallback,
  ChangeEvent,
  FormEvent,
} from "react";
import { useSelector, useDispatch } from "react-redux";
import { reducerType } from "@store/reducers";
import { actions, requestArabicSentence, saveArabicSentence } from "./reducer";
import Presenter from "./Presenter";
import { TArabicWord, TInputName } from "./types";

interface IBase {
  lessonId: number;
}

const ArabicSentence: FC<IBase> = ({ lessonId }: IBase): JSX.Element => {
  const dispatch = useDispatch();
  const {
    data: { arabicSentence, hideArabicSentence },
    loading,
    saving,
  } = useSelector((state: reducerType) => state.grammarArabicSentence);
  const [editItem, setEditItem] = useState(-1);
  const [changingText, setChangingText] = useState<string>();

  useEffect(() => {
    dispatch(requestArabicSentence(lessonId));
  }, []);

  const closeClick = useCallback(() => {
    setEditItem(-1);
  }, []);

  const onChangeInput = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const id = Number(e.target.id);
    const value = e.target.value;
    const name: TInputName = e.target.name as TInputName;
    dispatch(actions.setItem({ id, name, value }));
  }, []);

  const onChangeText = useCallback(
    (comment: string) => {
      if (editItem >= 0 && comment != changingText) {
        setChangingText(comment);
        dispatch(
          actions.setItem({
            id: editItem,
            name: "comment",
            value: comment,
          })
        );
      }
    },
    [editItem]
  );

  const onEditModeClick = useCallback(
    (e: ChangeEvent<HTMLDivElement>) => {
      const comment = e.target.closest(".comment");
      if (!comment) return;
      const id = Number(comment.getAttribute("id"));
      if (Number(editItem) === id) {
        setEditItem(-1);
      } else {
        const text: TArabicWord | undefined = arabicSentence.find(
          (item) => id === Number(item.id)
        );
        if (text?.comment != changingText) {
          setChangingText(text?.comment);
        }
        setEditItem(id);
      }
    },
    [editItem, arabicSentence]
  );

  const onDeleteItem = useCallback((e: any) => {
    const id = Number(e.target.id);
    dispatch(actions.deleteItem({ id }));
  }, []);

  const onAddItem = useCallback((e: any) => {
    const id = Number(e.target.id);
    dispatch(actions.addItem({ id }));
  }, []);

  const pasteFromBuffer = async () => {
    const bufferText = await navigator.clipboard.readText();
    const text = bufferText.replace(/ +/g, " ").trim();
    const result = confirm(text);
    if (result) {
      dispatch(actions.pasteFromBuffer({ bufferText }));
    }
  };

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    dispatch(saveArabicSentence(lessonId));
  };

  const setHideArabicSentence = useCallback(() => {
    dispatch(actions.setHideArabicSentence(!hideArabicSentence));
  }, [hideArabicSentence]);

  return (
    <Presenter
      loading={loading}
      saving={saving}
      arabicSentence={arabicSentence}
      editItem={editItem}
      changingText={changingText}
      onChangeInput={onChangeInput}
      onChangeText={onChangeText}
      onEditModeClick={onEditModeClick}
      onDeleteItem={onDeleteItem}
      onAddItem={onAddItem}
      pasteFromBuffer={pasteFromBuffer}
      onSubmit={onSubmit}
      hideArabicSentence={hideArabicSentence}
      setHideArabicSentence={setHideArabicSentence}
      closeClick={closeClick}
    />
  );
};

export default ArabicSentence;
