import { Loading } from "@components/types";

export type TArabicWord = {
  id: number;
  ar: string;
  rus?: string;
  comment?: string;
  deleted?: boolean;
};

export type TInputName = "ar" | "rus";

export type TArabicSentenceReducer = {
  saving: Loading;
  loading: Loading;
  data: {
    arabicSentence: TArabicWord[];
    hideArabicSentence: boolean;
  };
};
