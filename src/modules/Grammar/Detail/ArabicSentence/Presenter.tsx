import React, { FC, ChangeEvent, memo, MouseEvent, FormEvent } from "react";
import cn from "classnames";
import {
  Input,
  Button,
  Page,
  Checkbox,
  Footer,
} from "@components/Elements/elements";
import Editor from "@components/Editor";
import { Loading } from "@components/types";
import { TArabicWord } from "./types";
import ArabicText from "@modules/ArabicText";
import {
  ArabicTextBlock,
  ArabicTextItem,
  Comment,
  PlusBtn,
  DelBtn,
  PlusFirstBtn,
  PasteFromBufferBtn,
  Line,
  DetailEditor,
  CloseBtn,
  ArInput,
} from "./elements";

interface IArabicText {
  arabicSentence?: TArabicWord[];
  editItem: number;
  changingText?: string;
  loading: Loading;
  saving: Loading;
  hideArabicSentence: boolean;
  onDeleteItem: (e: MouseEvent<HTMLElement>) => void;
  onAddItem: (e: MouseEvent<HTMLElement>) => void;
  onChangeInput: (e: ChangeEvent<HTMLInputElement>) => void;
  onChangeText: (comment: string) => void;
  onEditModeClick: (e: any) => void;
  pasteFromBuffer: () => void;
  onSubmit: (e: FormEvent) => void;
  setHideArabicSentence: () => void;
  closeClick: () => void;
}

const Presenter: FC<IArabicText> = ({
  arabicSentence,
  editItem,
  changingText,
  loading,
  saving,
  hideArabicSentence,
  setHideArabicSentence,
  onChangeInput,
  onChangeText,
  onEditModeClick,
  onDeleteItem,
  onAddItem,
  pasteFromBuffer,
  onSubmit,
  closeClick,
}: IArabicText) => {
  return (
    <Page loading={loading}>
      <Line>
        <label>
          <Checkbox
            checked={Boolean(hideArabicSentence)}
            onChange={setHideArabicSentence}
          />
          Скрыть на сайте
        </label>
      </Line>
      <PasteFromBufferBtn onClick={pasteFromBuffer}>
        &#10052;
      </PasteFromBufferBtn>
      <hr />
      <ArabicText arabicText={arabicSentence || []} />
      <hr />
      <form onSubmit={onSubmit}>
        <ArabicTextBlock dir="rtl">
          <PlusFirstBtn id="0" onClick={onAddItem}>
            &#8853;
          </PlusFirstBtn>

          {arabicSentence &&
            arabicSentence?.length > 0 &&
            arabicSentence.map(
              ({ id, ar, rus, comment, deleted }: TArabicWord) => (
                <ArabicTextItem key={id}>
                  <DelBtn
                    id={String(id)}
                    deleted={deleted}
                    onClick={onDeleteItem}
                  >
                    &#10006;
                  </DelBtn>
                  <PlusBtn id={String(id)} onClick={onAddItem}>
                    &#8853;
                  </PlusBtn>
                  <ArInput
                    name="ar"
                    id={String(id)}
                    value={ar || ""}
                    onChange={onChangeInput}
                  />
                  <Input
                    name="rus"
                    id={String(id)}
                    dir="ltr"
                    onChange={onChangeInput}
                    value={rus || ""}
                  />
                  <Comment
                    onClick={onEditModeClick}
                    id={String(id)}
                    className={cn("comment", { active: editItem == id })}
                    dangerouslySetInnerHTML={{ __html: comment || "" }}
                  />
                </ArabicTextItem>
              )
            )}
          {editItem >= 0 ? (
            <DetailEditor dir="ltr">
              <Editor
                name="comment"
                value={changingText || ""}
                onChange={onChangeText}
              />
              <CloseBtn onClick={closeClick}>&#215;</CloseBtn>
            </DetailEditor>
          ) : null}
        </ArabicTextBlock>

        <Footer>
          <Button type="submit" saving={saving}>
            Сохранить
          </Button>
        </Footer>
      </form>
    </Page>
  );
};

export default memo(Presenter);
