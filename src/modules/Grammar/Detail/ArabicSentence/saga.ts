import { takeEvery, put, call, select } from "redux-saga/effects";
import { actions, requestArabicSentence, saveArabicSentence } from "./reducer";
import { Loading } from "@components/types";
import { reducerType } from "@store/reducers";
import { fetchGet, fetchPut, fetchOk, fetchDelete } from "@helpers/fetch";

export function* requestArabicSentenceSaga({ payload: id }: any) {
  try {
    yield put(actions.setLoading(Loading.start));
    const {
      arabicSentence,
      hideArabicSentence,
    }: { arabicSentence: string; hideArabicSentence: boolean } = yield call(
      fetchGet,
      `/api/grammar/${id}`
    );
    const sentence = arabicSentence ? JSON.parse(arabicSentence) : [];
    yield put(actions.setData(sentence));
    yield put(actions.setHideArabicSentence(hideArabicSentence));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* saveArabicSentenceSaga({ payload: id }: { payload: number }) {
  try {
    yield put(actions.setSaving(Loading.start));
    const state: reducerType = yield select();
    const { arabicSentence, hideArabicSentence } =
      state.grammarArabicSentence.data;
    const saveArabicSentence = arabicSentence.filter(
      (item) => !item?.deleted && item.ar
    );
    yield fetchPut(`/api/grammar/${id}`, {
      arabicSentence: saveArabicSentence,
      hideArabicSentence,
    });
    yield put(actions.setData(saveArabicSentence));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

export function* grammarArabicSentenceSaga() {
  yield takeEvery(requestArabicSentence, requestArabicSentenceSaga);
  yield takeEvery(saveArabicSentence, saveArabicSentenceSaga);
}
