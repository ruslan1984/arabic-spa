import React, { FormEvent, ChangeEvent, MouseEvent } from "react";
import { withKnobs } from "@storybook/addon-knobs";
import { actions } from "@storybook/addon-actions";
import Presenter from "./Presenter";
import { TArabicWord } from "./types";
import { Loading } from "@components/types";

export default {
  title: "Grammar",
  decorators: [withKnobs],
};

const arabicText: TArabicWord[] = [
  {
    id: 1,
    ar: "ar",
    rus: "Rus",
    comment: "comment",
  },
  {
    id: 2,
    ar: "ar1",
    rus: "Rus2",
    comment: "comment3",
  },
];

export const GrammarArabicSentence = () => {
  return (
    <Presenter
      arabicSentence={arabicText}
      editItem={-1}
      changingText=""
      hideArabicSentence={false}
      setHideArabicSentence={() => {
        actions("setHideArabicSentence");
      }}
      onChangeInput={(e: ChangeEvent<HTMLInputElement>) => {
        actions("onChangeInput");
      }}
      onChangeText={(comment: string) => {
        actions("onChangeText");
      }}
      onEditModeClick={(e: any) => {
        actions("onEditModeClick");
      }}
      onDeleteItem={(e: MouseEvent<HTMLElement>) => {
        actions("onDeleteItem");
      }}
      onAddItem={(e: MouseEvent<HTMLElement>) => {
        actions("onAddItem");
      }}
      pasteFromBuffer={() => {
        actions("pasteFromBuffer");
      }}
      onSubmit={(e: FormEvent) => {
        e.preventDefault();
        actions("onSubmit");
      }}
      loading={Loading.ok}
      saving={Loading.ok}
      closeClick={() => actions("closeClick")}
    />
  );
};
