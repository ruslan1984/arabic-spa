import styled from "@emotion/styled";
import { Input } from "@components/Elements/elements";

export const ArabicTextBlock = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  margin: 15px 0 5px;
  padding-right: 12.5px;
  padding-bottom: 150px;
`;
export const ArabicTextItem = styled.div`
  width: 200px;
  margin: 12.5px;
  position: relative;
`;

export const Comment = styled.div`
  padding: 10px;
  border: 1px solid #eee;
  min-height: 20px;
  cursor: pointer;
  transition: 0.5s;
  text-indent: 20px;
  p {
    text-indent: 20px;
  }
  &.active {
    border: 1px solid #6fe36f;
  }
  &:hover {
    background: #efefef;
  }
`;

const Btn = styled.div`
  position: absolute;
  font-size: 20px;
  cursor: pointer;
  transition: 0.5s;
`;

export const DelBtn = styled(Btn)`
  top: -25px;
  left: 3px;
  color: red;
  &:hover {
    color: #ae0000;
  }
  ${({ deleted }: { deleted?: boolean }) => {
    if (deleted) {
      return " color: black; transform: rotate(25deg);";
    }
  }};
`;
export const PlusBtn = styled(Btn)`
  top: 50px;
  left: -20px;
  color: green;
  &:hover {
    color: #015e01;
  }
`;
export const PlusFirstBtn = styled(Btn)`
  top: 65px;
  right: 5px;
  color: green;
  &:hover {
    color: #015e01;
  }
`;

export const PasteFromBufferBtn = styled(Btn)`
  color: #9089ff;
  position: static;
  &:hover {
    color: #3d32f8;
  }
`;

export const Line = styled.div`
  display: flex;
  margin: 5px 0;
`;

export const DetailEditor = styled.div`
  position: fixed;
  display: flex;
  flex: 1;
  aligin-items: center;
  justify-content: center;
  z-index: 1500;
  bottom: 0;
  left: 0;
  top: 0;
  width: 100%;
  box-sizing: border-box;
  background: white;
  padding: 20px 10px 60px;
  box-shadow: 0px 1px 5px black;
  .quill {
    height: 90%;
  }
  & > div:nth-child(1) {
    flex: 1;
  }
`;

export const CloseBtn = styled.div`
  font-size: 2rem;
  margin-left: 1rem;
  cursor: pointer;
`;

export const ArInput = styled(Input)`
  font-size: 1.7rem;
  padding: 5px 10px;
`;
