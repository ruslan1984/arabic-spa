import { takeEvery, put, call, select } from "redux-saga/effects";
import { actions, saveData, requestData } from "./reducer";
import { Loading } from "@components/types";
import { fetchGet, fetchPut, fetchOk, fetchDelete } from "@helpers/fetch";
import { reducerType } from "@store/reducers";
import { GrammarDetail } from "./types";

function* newGramarData() {
  const gl = {
    id: 0,
    name: "",
    title: "",
    code: "",
    metaKeywords: "",
    metaDescription: "",
    grammarText: "",
    arabicText: "",
    sort: 50,
    hideArabicText: false,
  };
  yield put(actions.setData(gl));
}

function* getGramarData(id: number) {
  try {
    yield put(actions.setLoading(Loading.start));
    const fetchData: GrammarDetail = yield call(
      async () => await fetchGet(`/api/grammar/${id}`)
    );
    yield put(actions.setData(fetchData));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

//function* setGramarDetail(data: any) {
//  const id = data.payload;
//  if (id == 0) {
//    yield newGramarData();
//  } else {
//    yield getGramarData(id);
//  }
//}

function* requestDataSaga({ payload: id }: { payload: number }) {
  try {
    yield put(actions.setLoading(Loading.start));
    const fetchData: GrammarDetail = yield call(
      async () => await fetchGet(`/api/grammar/${id}`)
    );
    yield put(actions.setData(fetchData));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

function* save(id: number) {
  try {
    yield put(actions.setSaving(Loading.start));
    const state: reducerType = yield select();
    const { updatedData } = state.grammarDetailBase;
    yield fetchPut(`/api/grammar/${id}`, updatedData);
    yield put(actions.clearUpdatedData());
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

function* add(id: number, data: any) {
  try {
    yield put(actions.setSaving(Loading.start));

    //const fetchData = yield call(
    //  async () => await fetchPut(`/api/grammar`, data)
    //);
    //if (fetchOk(fetchData.status)) {
    //  yield put(actions.setData(fetchData.data));
    //} else {
    //  alert("error");
    //}
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

function* saveDataSaga({ payload: id }: any) {
  //const { id } = data.payload;
  //const grammarData = data.payload;
  if (id > 0) {
    yield save(id);
  } else {
    //yield add(id, grammarData);
  }
}

function* removeGrammar(data: any) {
  const grammarId = data.payload;
  try {
    yield put(actions.setRemoving(Loading.start));

    //const fetchData = yield call(
    //  async () => await fetchDelete(`/api/grammar/${grammarId}`)
    //);
    //if (!fetchOk(fetchData.status)) {
    //  alert("error");
    //}
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setRemoving(Loading.ok));
  }
}

function* recoverGrammar(data: any) {
  const grammarId = data.payload;
  try {
    yield put(actions.setRemoving(Loading.start));
    //const fetchData = yield call(
    //  async () => await fetchPut(`/api/grammar/recover/${grammarId}`, data)
    //);
    //if (fetchOk(fetchData.status)) {
    //  yield put(actions.fetch(fetchData.data));
    //} else {
    //  alert("error");
    //}
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setRemoving(Loading.ok));
  }
}

export function* grammarDetailSaga() {
  //yield takeEvery(actions.setFetchId, setGramarDetail);
  //yield takeEvery(actions.setData, setGramarDetail);
  yield takeEvery(saveData, saveDataSaga);
  yield takeEvery(requestData, requestDataSaga);
  yield takeEvery(actions.removeGrammar, removeGrammar);
  yield takeEvery(actions.recoverGrammar, recoverGrammar);
}
