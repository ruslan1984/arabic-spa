import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import {
  GrammarDetailType,
  GrammarDetail,
  GrammarUpdateData,
  GrammarDetailUpdated,
  NameType,
} from "./types";
import { Loading } from "@components/types";

export const defaultState: GrammarDetailType = {
  data: {
    id: 0,
    name: "",
    title: "",
    code: "",
    metaKeywords: "",
    metaDescription: "",
    grammarText: "",
    arabicText: "",
    sort: 50,
    deletedAt: null,
    hideArabicText: false,
  },
  updatedData: null,
  loading: Loading.no,
  saving: Loading.no,
  removing: Loading.no,
  recovering: Loading.no,
};

export const saveData = createAction<number>("saveData");
export const requestData = createAction<number>("requestData");

export const grammarDetailBaseSlice = createSlice({
  name: "grammarDetailBase",
  initialState: defaultState,
  reducers: {
    fetch: (state, { payload }: PayloadAction<GrammarDetail>) => {
      return { ...state, data: payload };
    },
    setId: (state, { payload }: PayloadAction<number>) => {
      const data = { ...state.data, id: payload };
      return { ...state, data };
    },
    setFetchId: (state, { payload }: PayloadAction<number>) => {
      const data = { ...state.data, id: payload };
      return { ...state, data };
    },
    setUpdatedData: (
      state,
      { payload: { name, value } }: PayloadAction<{ name: string; value: any }>
    ) => {
      const updatedData: any = {
        ...state.updatedData,
      };
      if (name) updatedData[name] = value;
      return { ...state, updatedData };
    },
    clearUpdatedData: (state) => ({
      ...state,
      updatedData: null,
    }),
    updateValue: (
      state,
      {
        payload: { name, value },
      }: PayloadAction<{ name: NameType; value: string | boolean }>
    ) => {
      return {
        ...state,
        data: {
          ...state.data,
          [name]: value,
        },
        updatedData: {
          ...state.updatedData,
          [name]: value,
        },
      };
    },
    saveData: (state, { payload }: PayloadAction<GrammarDetail>) => {
      const data = { ...state.data, ...payload };
      return { ...state, data };
    },
    setData: (state, { payload }: PayloadAction<GrammarDetail>) => {
      const data = { ...state.data, ...payload };
      return { ...state, data };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    setSaving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, saving: payload };
    },
    setRemoving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, removing: payload };
    },
    setRecovering: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, recovering: payload };
    },
    removeGrammar: (state, { payload }: PayloadAction<number>) => {
      const data = { ...state.data };
      //  data["deletedAt"] = "now";
      return { ...state };
    },
    recoverGrammar: (state, { payload }: PayloadAction<number>) => {
      const data = { ...state.data };
      //  data["deletedAt"] = null;
      return { ...state };
    },
  },
});
export const { actions, reducer } = grammarDetailBaseSlice;
