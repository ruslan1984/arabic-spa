import React, { FC, FormEvent } from "react";
import {
  Label,
  Input,
  Line,
  Page,
  Button,
  RemoveButton,
  RecoverButton,
  AddLink,
  Checkbox,
  Footer,
} from "@components/Elements/elements";
import Editor from "@components/Editor";
import { Loading } from "@components/types";
import { GrammarDetail } from "./types";

interface DetailProps {
  data: GrammarDetail;
  loading: Loading;
  saving: Loading;
  recovering: Loading;
  removing: Loading;
  removeGrammar: () => void;
  recoverGrammar: () => void;
  onSubmit: (e: FormEvent) => void;
  inputChange: (name: string) => (e: FormEvent) => void;
  editorChange: (text: string, name: string) => void;
  checkboxChange: any;
}

export const Presenter: FC<DetailProps> = (props: DetailProps) => {
  return (
    <Page loading={props.loading}>
      <h1>Грамматика</h1>
      <form action="" onSubmit={props.onSubmit}>
        <Line>
          <Label htmlFor="">Название</Label>
          <Input
            name="name"
            type="text"
            value={props.data.name || ""}
            onChange={props.inputChange("name")}
          />
        </Line>
        <Line>
          <Label htmlFor="">Код</Label>
          <Input
            name="code"
            type="text"
            value={props.data.code || ""}
            onChange={props.inputChange("code")}
          />
        </Line>
        <Line>
          <Label htmlFor="">Сортировка</Label>
          <Input
            type="text"
            value={props.data?.sort || "100"}
            onChange={props.inputChange("sort")}
            name="sort"
          />
        </Line>
        <h2>Грамматика</h2>
        <Editor
          name="grammarText"
          value={props.data?.grammarText || ""}
          onChange={props.editorChange}
        />
        <hr />

        <h2>Арабский текст</h2>
        <Line>
          <label>
            <Checkbox
              name="hideArabicText"
              checked={Boolean(props.data?.hideArabicText)}
              onChange={props.checkboxChange}
            />
            Скрыть на сайте
          </label>
        </Line>
        <Editor
          name="arabicText"
          value={props.data?.arabicText || ""}
          onChange={props.editorChange}
        />
        <h2>SEO</h2>
        <Line>
          <Label htmlFor="">title</Label>
          <Input
            name="title"
            type="text"
            value={props.data?.title || ""}
            onChange={props.inputChange("title")}
          />
        </Line>
        <Line>
          <Label htmlFor="">metaKeywords</Label>
          <Input
            name="metaKeywords"
            type="text"
            value={props.data?.metaKeywords || ""}
            onChange={props.inputChange("metaKeywords")}
          />
        </Line>
        <Line>
          <Label htmlFor="">metaDescription</Label>
          <Input
            name="metaDescription"
            type="text"
            value={props.data?.metaDescription || ""}
            onChange={props.inputChange("metaDescription")}
          />
        </Line>
        <Footer>
          <Button type="submit" saving={props.saving}>
            Сохранить
          </Button>

          {props.data.deletedAt ? (
            <RecoverButton
              saving={props.recovering}
              onClick={props.recoverGrammar}
              type="button"
            >
              Восстановить
            </RecoverButton>
          ) : (
            <RemoveButton
              saving={props.removing}
              onClick={props.removeGrammar}
              type="button"
            >
              Удалить
            </RemoveButton>
          )}
          <AddLink to={"../0/base"}>Добавить</AddLink>
        </Footer>
      </form>
    </Page>
  );
};

export default Presenter;
