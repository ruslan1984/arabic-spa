import { Loading } from "@components/types";

export type GrammarDetail = {
  id: number | undefined;
  name: string | undefined;
  title?: string | undefined;
  code: string | undefined;
  metaKeywords?: string | undefined;
  metaDescription?: string | undefined;
  grammarText: string | undefined;
  arabicText?: string | undefined;
  deletedAt?: string | null | undefined;
  sort: number | undefined;
  hideArabicText: boolean | undefined;
};

export type NameType =
  | "name"
  | "code"
  | "sort"
  | "title"
  | "hideArabicText"
  | "grammarText"
  | "arabicText"
  | "metaKeywords"
  | "metaDescription";

export type GrammarDetailUpdated = {
  updatedAt?: string;
  name?: string;
  title?: string;
  code?: string;
  metaKeywords?: string;
  metaDescription?: string;
  grammarText?: string;
  arabicText?: string;
  deletedAt?: string | null;
  createUserId?: number;
  sort?: number;
  hideArabicText?: boolean;
};
export type GrammarUpdateData = {
  name: string;
  value: string;
};
export type GrammarDetailType = {
  loading: Loading;
  saving: Loading;
  removing: Loading;
  recovering: Loading;
  data: GrammarDetail;
  //updatedList: GrammarDetailUpdated | null;
  updatedData: GrammarDetailUpdated | null;
};
