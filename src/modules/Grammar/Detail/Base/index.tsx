import React, { FC, useEffect, useCallback, FormEvent } from "react";
import { actions, saveData, requestData } from "./reducer";
import { reducerType } from "@store/reducers";
import { useSelector, useDispatch } from "react-redux";
import Presenter from "./Presenter";
import { GrammarDetail, NameType } from "./types";

interface IBase {
  lessonId: number;
}

const Base: FC<IBase> = ({ lessonId }: IBase): JSX.Element => {
  const dispatch = useDispatch();
  const { data, loading, saving, removing, recovering } = useSelector(
    (state: reducerType) => state.grammarDetailBase
  );

  useEffect(() => {
    dispatch(requestData(lessonId));
    dispatch(actions.clearUpdatedData());
  }, [lessonId]);

  const removeGrammar = async () => {
    await dispatch(actions.removeGrammar(lessonId));
  };
  const recoverGrammar = async () => {
    await dispatch(actions.recoverGrammar(lessonId));
  };
  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    await dispatch(saveData(lessonId));
  };

  const inputChange = (name: string) => (e: FormEvent) => {
    const nameType = name as NameType;
    const value = (e.target as HTMLInputElement)?.value;
    dispatch(actions.updateValue({ name: nameType, value }));
  };
  const editorChange = (text: string, name: string) => {
    const nameType = name as NameType;

    dispatch(actions.updateValue({ name: nameType, value: text }));
  };

  const checkboxChange = useCallback(
    (e: FormEvent<HTMLInputElement>) => {
      const name = e.currentTarget?.name as NameType;
      const value = !data[name];
      dispatch(actions.updateValue({ name, value }));
    },
    [data]
  );

  return (
    <Presenter
      data={data}
      loading={loading}
      saving={saving}
      removing={removing}
      recovering={recovering}
      inputChange={inputChange}
      editorChange={editorChange}
      onSubmit={onSubmit}
      checkboxChange={checkboxChange}
      removeGrammar={removeGrammar}
      recoverGrammar={recoverGrammar}
    />
  );
};

export default Base;
