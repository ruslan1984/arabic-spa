import React, {
  FC,
  FormEvent,
  useCallback,
  useState,
  memo,
  useEffect,
  ChangeEvent,
} from "react";
import { TArabicWord } from "@modules/Grammar/Detail/ArabicSentence/types";
import Presenter from "./Presenter";

interface IBase {
  arabicText: TArabicWord[];
}

const ArabicSentence: FC<IBase> = ({ arabicText }: IBase): JSX.Element => {
  const [activeId, setActiveId] = useState(-1);

  useEffect(() => {
    document.addEventListener("mousedown", handleClick);
  }, []);

  useEffect(() => {
    return () => document.removeEventListener("mousedown", handleClick);
  }, []);

  const handleClick = useCallback((e: any) => {
    const translationBlock = e.target.closest(".translationBlock");
    if (!translationBlock) {
      setActiveId(-1);
    }
  }, []);

  const wordClick = useCallback(
    (e: FormEvent) => {
      const id = Number(e.currentTarget.getAttribute("id"));
      if (id !== activeId) {
        setActiveId(id);
      }
    },
    [activeId]
  );

  const closeClick = useCallback(() => {
    setActiveId(-1);
  }, []);
  return (
    <Presenter
      arabicText={arabicText}
      activeId={activeId}
      wordClick={wordClick}
      closeClick={closeClick}
    />
  );
};

export default memo(ArabicSentence);
