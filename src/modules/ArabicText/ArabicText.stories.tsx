import React, { FormEvent, ChangeEvent, MouseEvent } from "react";
import { withKnobs } from "@storybook/addon-knobs";
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport";
import Presenter from "./index";
import { TArabicWord } from "@modules/Grammar/Detail/ArabicSentence/types";

export default {
  title: "Grammar",
  decorators: [withKnobs],
};

const arabicText: TArabicWord[] = [
  {
    id: 1,
    ar: "ar",
    rus: "Rus",
    comment: "comment",
  },
  {
    id: 2,
    ar: "ar1",
    rus: "Rus2",
  },
  {
    id: 3,
    ar: "غَنِيَّة",
    rus: "Богатая",
    comment: `<div><span>غَنِيَّ - богатый</span></div>
    <div><span>ة - окончание признак женского рода</span></div>`,
  },
];

export const GrammarArabicText = () => {
  return <Presenter arabicText={arabicText} />;
};
