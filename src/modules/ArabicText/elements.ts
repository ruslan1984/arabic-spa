import styled from "@emotion/styled";
import { tabletLg } from "@site/media";
import { keyframes } from "@emotion/react";
import { closeIcon } from "@/components/Elements/icons";

const showAnimation = keyframes`
	from { 
    transform: translateY(100px); 
	}
  to { 
    transform: translateY(0); 
  }
`;

export const ArabicTextBlock = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const WordBlock = styled.div`
  position: relative;
`;

export const Word = styled.div`
  margin: 3px 1px;
  font-size: 1.8rem;
  cursor: pointer;
  transition: 0.5s;
  padding: 5px 3px;
  border-radius: 8px;
  ${({
    active,
    withTransation,
  }: {
    active?: boolean;
    withTransation?: boolean;
  }) => {
    let result = "";
    if (active) {
      result = " background: #f3dbbb;";
    }
    if (withTransation) {
      result += `&:hover {
        background: var(--bthMenu-hover);
      }`;
    }
    return result;
  }}
`;

export const Translation = styled.div`
  text-align: center;
  min-width: 100px;
  font-size: 1.2rem;
`;

export const Comment = styled.div`
  font-weight: 400;
  text-align: left;
  border-top: 1px solid #ddd;
  margin-top: 10px;
  padding: 5px;
  min-width: 250px;
  &:before {
    content: "Комментарий:";
    font-weight: 600;
  }
  p {
    text-indent: 0;
    text-align: left;
  }
`;

export const TranslationBlock = styled.div`
  font-size: 1rem;
  transition: 0.5s;
  padding: 10px;
  display: flex;
  position: absolute;
  background: var(--main-bg);
  display: none;
  z-index: 1001;
  box-shadow: 2px 3px 7px #797979;
  box-sizing: border-box;
  min-width: 200px;
  border-radius: 8px;
  &.active {
    display: block;
  }
  @media (max-width: ${tabletLg()}) {
    position: fixed;
    width: 100%;
    bottom: 0;
    left: 0;
    border-radius: 0;
    padding-bottom: 60px;
    z-index: 500;
    animation: none;
    &.active {
      animation: ${showAnimation} forwards 0.5s ease;
    }
  }
`;

export const CloseBtn = styled.div`
  box-shadow: 2px 3px 7px #797979;
  width: 28px;
  height: 28px;
  font-size: 1rem;
  border-radius: 20px;
  position: absolute;
  text-align: center;
  cursor: pointer;
  top: -18px;
  right: -30px;
  background: var(--main-bg);
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: url(${closeIcon});
  background-size: 12px;
  background-repeat: no-repeat;
  background-position: center;
  @media (max-width: ${tabletLg()}) {
    top: -32px;
    right: 10px;
  }
`;
