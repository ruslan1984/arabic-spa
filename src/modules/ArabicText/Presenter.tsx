import React, { FC, memo, FormEvent } from "react";
import { TArabicWord } from "@modules/Grammar/Detail/ArabicSentence/types";
import {
  ArabicTextBlock,
  WordBlock,
  Translation,
  Word,
  Comment,
  TranslationBlock,
  CloseBtn,
} from "./elements";
import cn from "classnames";

interface IArabicText {
  arabicText: TArabicWord[];
  activeId: number;
  wordClick: (e: FormEvent) => void;
  closeClick: () => void;
}

const Presenter: FC<IArabicText> = ({
  arabicText,
  activeId,
  wordClick,
  closeClick,
}: IArabicText) => {
  return (
    <ArabicTextBlock dir="rtl">
      {arabicText.map((item) => (
        <WordBlock key={item.id}>
          <Word
            active={item.id === activeId && Boolean(item?.rus)}
            withTransation={Boolean(item?.rus)}
            id={String(item.id)}
            onClick={wordClick}
          >
            {item.ar}
          </Word>
          {item?.rus || item?.comment ? (
            <TranslationBlock
              id={String(item.id)}
              className={cn("translationBlock", {
                active: item.id === activeId,
              })}
            >
              <CloseBtn onClick={closeClick} />
              {item?.rus ? (
                <Translation dir="ltr">{item.rus}</Translation>
              ) : null}
              {item?.comment ? (
                <Comment
                  dir="ltr"
                  dangerouslySetInnerHTML={{
                    __html: item.comment,
                  }}
                />
              ) : null}
            </TranslationBlock>
          ) : null}
        </WordBlock>
      ))}
    </ArabicTextBlock>
  );
};

export default memo(Presenter);
