import React, { FC, memo, FormEvent, useEffect, useState } from "react";
import {
  Input,
  Select,
  Button,
  RemoveButton,
  RecoverButton,
} from "@components/Elements/elements";
import { WordLine, WordBlock, WordTypeBlock, Form } from "./elements";
import { WordType } from "./types";
//import { useFormik } from "formik";

interface IWord {
  lessonId: number;
  data: WordType;
  //grammarList?: () => ReactNode;
  onSave: (data: WordType) => void;
  removeWord?: (id: number) => void;
  //recoverWord?: (id: number) => void;
}

const Presenter: FC<IWord> = ({ data, lessonId, onSave }: IWord) => {
  const [formData, setFormData] = useState<WordType>({
    id: 0,
    rusWord: "",
    arWord: "",
    rusWordMn: "",
    arWordMn: "",
    wordType: "ism",
    figSimpol: "A",
  });

  useEffect(() => {
    setFormData(data);
  }, [lessonId, data]);

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    onSave(formData);
  };
  const handleChange = (e: FormEvent) => {
    const target = e.target as HTMLInputElement;
    setFormData({
      ...formData,
      [target.name]: target.value,
    });
  };

  return (
    <Form onSubmit={handleSubmit}>
      <div>id {data.id}</div>
      <WordLine>
        <WordBlock>
          <div>Рус ед</div>
          <Input
            type="text"
            name="rusWord"
            value={formData.rusWord || ""}
            onChange={handleChange}
          />
        </WordBlock>
        <WordBlock>
          <div>Ар ед</div>
          <Input
            type="text"
            value={formData.arWord}
            name="arWord"
            onChange={handleChange}
          />
        </WordBlock>
        <WordBlock>
          <div>Рус мн</div>
          <Input
            type="text"
            value={formData.rusWordMn || ""}
            name="rusWordMn"
            onChange={handleChange}
          />
        </WordBlock>
        <WordBlock>
          <div>Ар мн</div>
          <Input
            //id={id | 0}
            type="text"
            value={formData.arWordMn || ""}
            name="arWordMn"
            onChange={handleChange}
          />
        </WordBlock>
      </WordLine>
      <WordLine>
        <WordTypeBlock>
          <Select
            name="wordType"
            defaultValue={formData.wordType || "ism"}
            onChange={handleChange}
          >
            <option value="ism">ism</option>
            <option value="figl">figl</option>
            <option value="harf">harf</option>
          </Select>
        </WordTypeBlock>
        <WordTypeBlock>
          <Select
            defaultValue={formData.figSimpol || "-"}
            name="figSimpol"
            //id={props.id | 0}
            onChange={handleChange}
          >
            <option value="-">-</option>
            <option value="A">A</option>
            <option value="I">I</option>
            <option value="U">U</option>
          </Select>
        </WordTypeBlock>
        <WordBlock>
          <Select
            defaultValue={formData.lessonId}
            name="lessonId"
            onChange={handleChange}
          ></Select>
        </WordBlock>
      </WordLine>
      <Button saving={1} type="submit">
        Сохранить
      </Button>
      {(() => {
        if (data.deletedAt) {
          return (
            <RecoverButton
              saving={data.removing}
              //onClick={recoverWord}
              type="button"
            >
              Восстановить
            </RecoverButton>
          );
        } else {
          return (
            <RemoveButton
              saving={data.removing}
              //onClick={removeWord}
              type="button"
            >
              Удалить
            </RemoveButton>
          );
        }
      })()}
    </Form>
  );
};

export default memo(Presenter);
