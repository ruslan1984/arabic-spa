import React from "react";
import { connect } from "react-redux";
import { actions, saveWord } from "./reducer";
import { reducerType } from "@store/reducers";
import { Page } from "@components/Elements/elements";
import { WordsHeader } from "./elements";
import { WordsBase } from "./WordsBase";
// import Presenter from "./Presenter";
import { WordType, UpdatedData, GrammarItem, SaveWordType } from "./types";
import { Loading } from "@components/types";

interface WordProps {
  data: WordType[];
  grammarList: GrammarItem[];
  loading: Loading;
  saveWord: (data: SaveWordType) => void;
  removeWord: (id: number) => void;
  recoverWord: (id: number) => void;
  setUpdateData: (data: UpdatedData) => void;
  setLessonId: (lessonId: number) => void;
}

export class Basket extends WordsBase<WordProps> {
  componentDidMount() {
    const lessonId = 1; //(this.props.match.params as any).id;
    this.props.setLessonId(lessonId);
  }
  render() {
    return (
      <Page loading={this.props.loading}>
        <WordsHeader>
          <h1>Корзина</h1>
          {/* <Link to=".">{"<<"}</Link> */}
        </WordsHeader>
        {/*{this.wordList()}*/}
      </Page>
    );
  }
}

const mapStateToProps = (state: reducerType) => {
  const { data, loading, grammarList } = state.words;
  return {
    grammarList,
    data,
    loading,
  };
};

const mapDispatchToProps = {
  //setUpdateData: actions.setUpdateData,
  setLessonId: actions.setBasketLessonId,
  saveWord: saveWord,
  removeWord: actions.removeWord,
  recoverWord: actions.recoverWord,
};

//export default connect(mapStateToProps, mapDispatchToProps)(Basket);
export default Basket;
