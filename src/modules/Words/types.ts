import { Loading } from "@components/types";
export type WordType = {
  id: number;
  arWord: string;
  arWordMn?: string | "";
  rusWord: string;
  rusWordMn?: string | "";
  wordType: "ism" | "figl" | "harf";
  figSimpol?: "A" | "I" | "U";
  lessonId?: number;
  deletedAt?: string;
  saving?: Loading;
  removing?: Loading;
};

export type UpdatingData = {
  id: number;
  name: string;
  value: string;
};

export type UpdatedData = {
  id?: number;
  arWord?: string;
  arWordMn?: string | "";
  rusWord?: string;
  rusWordMn?: string | "";
  wordType?: "ism" | "figl" | "harf";
  figSimpol?: "A" | "I" | "U";
  saving?: Loading;
  lessonId?: number;
  deletedAt?: string;
};
export type GrammarItem = {
  id: number;
  name: string;
};
export type ReducerType = {
  data: WordType[];
  grammarList: Array<GrammarItem>;
  updatedData: UpdatedData[];
  lessonId: number;
  loading: Loading;
  removing: Loading;
};
export type SaveWordType = {
  id: number;
  data: UpdatedData;
};
export type SaveType = {
  id: number;
  saving: Loading;
};
export type RemoveType = {
  id: number;
  removing: Loading;
};
