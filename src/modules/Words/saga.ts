import { takeEvery, put, call } from "redux-saga/effects";
import { actions, saveWord } from "./reducer";
import { Loading } from "@components/types";
import { WordType } from "./types";
import { fetchGet, fetchPut } from "@helpers/fetch";

export function* setLessonId(data: any) {
  const lessonId = data.payload;
  if (lessonId == 0) return;
  try {
    yield put(actions.setLoading(Loading.start));
    const url = `/api/grammar/${lessonId}/words_with_grammar`;
    const { data }: { data: WordType[]; grammarList: [] } = yield call(
      fetchGet,
      url
    );
    yield put(actions.setData(data));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* setBasketLessonId(data: any) {
  try {
    yield put(actions.setLoading(Loading.start));
    const lessonId = data.payload;
    //const fetchData = yield call(() => {
    //  return fetch(process.env.HOST + `/api/word/basket/${lessonId}`, {
    //    method: "GET",
    //    headers: {
    //      "Content-Type": "application/json;charset=utf-8",
    //      Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
    //    },
    //  }).then((res) => res.json());
    //});
    //yield put(actions.setData(fetchData.data));
    //yield put(actions.setGrammarList(fetchData.grammarList));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* save(wordId: number, word: any) {
  try {
    yield put(actions.setSaving({ id: wordId, saving: Loading.start }));
    yield fetchPut(`/api/word/${wordId}`, word);
    yield put(actions.setData(word));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving({ id: wordId, saving: Loading.ok }));
  }
}

export function* add(wordId: number, word: any) {
  try {
    yield put(actions.setSaving({ id: wordId, saving: Loading.start }));
    yield call(async () => {
      await fetch(process.env.REACT_APP_HOST + `/api/word/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
        },
        body: JSON.stringify(word),
      });
    });
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving({ id: wordId, saving: Loading.ok }));
  }
}

export function* saveWordSaga({ payload: word }: any) {
  const wordId = word?.id;
  if (wordId == 0) {
    yield add(wordId, word);
  } else {
    yield save(wordId, word);
  }
}

export function* removeWord(data: any) {
  const wordId = data.payload;
  try {
    yield put(actions.setRemoving({ id: wordId, removing: Loading.start }));
    yield call(async () => {
      await fetch(process.env.HOST + `/api/word/${wordId}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
        },
      });
    });
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setRemoving({ id: wordId, removing: Loading.ok }));
  }
}
export function* recoverWord(data: any) {
  const wordId = data.payload;
  try {
    yield put(actions.setRemoving({ id: wordId, removing: Loading.start }));
    yield call(async () => {
      await fetch(process.env.HOST + `/api/word/recover/${wordId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
        },
      });
    });
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setRemoving({ id: wordId, removing: Loading.ok }));
  }
}

export function* wordsGrammarSaga() {
  yield takeEvery(actions.setLessonId, setLessonId);
  yield takeEvery(actions.setBasketLessonId, setBasketLessonId);
  yield takeEvery(saveWord, saveWordSaga);
  yield takeEvery(actions.removeWord, removeWord);
  yield takeEvery(actions.recoverWord, recoverWord);
}
