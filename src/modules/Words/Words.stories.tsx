import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import Presenter from "./Presenter";

export default {
  title: "Grammar",
  decorators: [withKnobs],
};
const data = {
  id: 1,
  arWord: "حُجْرَةٌ",
  arWordMn: "حُجُرَاتٌ",
  rusWord: "Комната",
  rusWordMn: null,
  wordType: "ism",
  figSimpol: null,
  lessonId: 1,
  deletedAt: "1",
  saving: 0,
};
const grammarList = () => [
  <option key={1} value={1}>
    {"name1"}
  </option>,
  <option key={2} value={2}>
    {"name2"}
  </option>,
];

export const GrammarWords = () => {
  return <>Поправить</>; //<Presenter grammarList={grammarList} {...data} />;
};
