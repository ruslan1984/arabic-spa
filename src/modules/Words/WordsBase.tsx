import React, { Component } from "react";
import Presenter from "./Presenter";
import { WordType, UpdatedData, GrammarItem, SaveWordType } from "./types";
import { Loading } from "@components/types";

interface WordProps {
  lessonId: number;
  data: WordType[];
  grammarList: GrammarItem[];
  loading: Loading;
  addWord: () => void;
  saveWord: (data: SaveWordType) => void;
  removeWord: (id: number) => void;
  recoverWord: (id: number) => void;
  setUpdateData: (data: UpdatedData) => void;
  setLessonId: (lessonId: number) => void;
}

export class WordsBase<T> extends Component<WordProps> {
  onSave = (id: number, data: UpdatedData) => {
    this.props.saveWord({ id, data });
  };
  grammarList = () => {
    if (this.props.grammarList) {
      return this.props.grammarList.map((item, id) => (
        <option key={id} value={item.id}>
          {item.name}
        </option>
      ));
    }
  };

  //wordList() {
  //  if (this.props.data.length > 0) {
  //    return this.props.data.map((item) => (
  //      <Presenter
  //        key={item.id.toString()}
  //        {...item}
  //        onSave={this.onSave}
  //        removeWord={this.props.removeWord}
  //        recoverWord={this.props.recoverWord}
  //        grammarList={this.grammarList}
  //      />
  //    ));
  //  } else {
  //    return <div>Пусто</div>;
  //  }
  //}
}

export default WordsBase;
