import React, { FC, useCallback, useEffect, memo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Page } from "@components/Elements/elements";
import { actions, saveWord } from "./reducer";
import { reducerType } from "@store/reducers";
import { WordType } from "./types";
//import { Loading } from "@components/types";
//import { WordsBase } from "./WordsBase";
import { WordsHeader } from "./elements";
import Presenter from "./Presenter";

//import formik from "formik";

interface IWords {
  lessonId: number;
  //data: WordType[];
  //grammarList: GrammarItem[];
  //loading: Loading;
  //saveWord: (data: SaveWordType) => void;
  //removeWord: (id: number) => void;
  //recoverWord: (id: number) => void;
  //setUpdateData: (data: UpdatedData) => void;
  //setLessonId: (lessonId: number) => void;
  //addWord: () => void;
}

const Words: FC<IWords> = ({ lessonId }: IWords) => {
  const dispatch = useDispatch();
  const { data, grammarList, loading } = useSelector(
    (state: reducerType) => state.words
  );
  useEffect(() => {
    dispatch(actions.setLessonId(lessonId));
  }, []);

  const onSave = useCallback((data: WordType) => {
    dispatch(saveWord(data));
  }, []);

  const wordList = useCallback(() => {
    if (data.length > 0) {
      return data.map((item) => (
        <Presenter
          lessonId={lessonId}
          key={item.id}
          data={item}
          onSave={onSave}
          //removeWord={dispatch(actions.removeWord)}
          //recoverWord={dispatch(actions.recoverWord)}
          //grammarList={grammarList}
        />
      ));
    } else {
      return <div>Пусто</div>;
    }
  }, [data]);
  return (
    <Page loading={loading}>
      <WordsHeader>
        <h1>Слова</h1>
        <Link to={"words/basket"}>Корзина</Link>
      </WordsHeader>
      {wordList()}
      {/*<Button onClick={dispatch(actions.addWord)}>Добавить слово</Button>*/}
    </Page>
  );
};

export default memo(Words);
