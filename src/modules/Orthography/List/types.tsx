export enum Loading {
  start,
  ok,
}
export type OrthographyList = Array<OrthographyListItem>;
export type OrthographyListItem = {
  id: number;
  name: string;
  code: string;
  sort: number;
};
export type OrthographyListType = {
  list: OrthographyList;
  loading: Loading;
};
