import React, { FC } from "react";
import { Page, ListLink, H1 } from "@components/Elements/elements";
import { OrthographyList, Loading } from "./types";
import routes from "@routes/routes";
import { Grid } from "./styles";

interface ListProps {
  list: OrthographyList;
  loading: Loading;
}

const Presenter: FC<ListProps> = (props: ListProps) => {
  return (
    <Page loading={props.loading}>
      <H1>Орфография список</H1>
      <Grid>
        {props.list.map((item) => (
          <React.Fragment key={item.id.toString()}>
            <ListLink to={routes.orthography.Id(item.id)}>{item.name}</ListLink>
            <ListLink to={routes.orthography.Harf(item.id)}>Написание</ListLink>
            <ListLink to={routes.orthography.Sound(item.id)}>Звучание</ListLink>
          </React.Fragment>
        ))}
      </Grid>
    </Page>
  );
};

export default Presenter;
