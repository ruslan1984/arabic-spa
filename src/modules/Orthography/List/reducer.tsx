import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import { OrthographyListType, OrthographyList, Loading } from "./types";

export const defaultState: OrthographyListType = {
  list: [],
  loading: Loading.ok,
};
export const setList = createAction("setList");
export const setBasket = createAction("setBasket");
export const orthographyListSlice = createSlice({
  name: "orthographyList",
  initialState: defaultState,
  reducers: {
    setData: (state, { payload }: PayloadAction<OrthographyList>) => {
      return { ...state, list: payload };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
  },
});
export const { actions, reducer } = orthographyListSlice;
