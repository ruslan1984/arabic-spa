import styled from "@emotion/styled";

export const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
`;
