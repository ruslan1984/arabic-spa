import { takeEvery, put, call } from "redux-saga/effects";
import { actions, setList, setBasket } from "./reducer";
import { Query } from "@helpers/graphql";
import { Loading, OrthographyListItem } from "./types";

export function* setListSaga(query: string) {
  try {
    yield put(actions.setLoading(Loading.start));
    const fetchData: {
      orthographies: OrthographyListItem[];
    } = yield call(async () => await Query(query));
    yield put(actions.setData(fetchData.orthographies));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* setOrthographyList() {
  yield setListSaga("{orthographies {id name code } }");
}

export function* setBasketOrthographyList() {
  yield setListSaga("{orthographiesDeleted {id name code } }");
}

export function* orthographyListSaga() {
  yield takeEvery(setList, setOrthographyList);
  yield takeEvery(setBasket, setBasketOrthographyList);
}
