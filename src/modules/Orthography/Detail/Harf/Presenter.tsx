import React, { FC, FormEvent, useState, useEffect } from "react";
import {
  Label,
  Input,
  Page,
  Button,
  RemoveButton,
  Footer,
} from "@components/Elements/elements";
import { Line, ImgHarf, HarfLine } from "./styles";
import { Loading } from "@components/types";
import { HarfType } from "./types";
import HarfBlock from "./components/HarfLine";

interface IHarf {
  data: HarfType;
  saving: Loading;
  onSubmit: (e: FormEvent) => void;
  //removeImage: (harf: string) => void;
}

export const Presenter: FC<IHarf> = ({ data, saving, onSubmit }: IHarf) => {
  const [formData, setFormData] = useState<HarfType>();
  useEffect(() => {
    setFormData(data);
  }, [data]);

  return (
    <>
      <h2>Орфография. Графическое начертание.</h2>
      <Page loading={Loading.ok}>
        <form action="" onSubmit={onSubmit}>
          <HarfLine>
            <Line>
              <Label htmlFor="">Название по арабски</Label>
              <Input
                type="text"
                value={formData?.harfName}
                //onChange={handleChange}
                name="harfName"
              />
            </Line>
          </HarfLine>
          <HarfBlock
            name="harfFree"
            nameImg="harfFreeImg"
            title="Свободное"
            value={formData?.harfFree || ""}
            harfImg={formData?.harfFreeImg || ""}
          />
          <HarfBlock
            name="harfFirst"
            nameImg="harfFirstImg"
            title="В начале"
            value={formData?.harfFirst || ""}
            harfImg={formData?.harfFirstImg || ""}
          />
          <HarfBlock
            name="harfCenter"
            nameImg="harfCenterImg"
            title="В середине"
            value={formData?.harfCenter || ""}
            harfImg={formData?.harfCenterImg || ""}
          />
          <HarfBlock
            name="harfLast"
            nameImg="harfLastImg"
            title="В конце"
            value={formData?.harfLast || ""}
            harfImg={formData?.harfLastImg || ""}
          />
          <Footer>
            <Button type="submit" saving={saving}>
              Сохранить
            </Button>
          </Footer>
        </form>
      </Page>
    </>
  );
};

export default Presenter;
