import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import {
  OrthographyDetailType,
  HarfType,
  UpdatedHarfDataType,
  HarfNameType,
} from "./types";
import { Loading } from "@components/types";

const emptyData = {
  harfName: "",
  harfFree: "",
  harfFirst: "",
  harfCenter: "",
  harfLast: "",
  harfFreeImg: "",
  harfFirstImg: "",
  harfCenterImg: "",
  harfLastImg: "",
};

export const defaultState: OrthographyDetailType = {
  data: emptyData,
  loading: Loading.no,
  saving: Loading.no,
  removing: Loading.no,
  recovering: Loading.no,
  updatedHarfData: {},
};

export const saveData = createAction<number>("saveData");

export const slice = createSlice({
  name: "orthographyDetailHarf",
  initialState: defaultState,
  reducers: {
    setId: (state, { payload }: PayloadAction<number>) => {
      const data = { ...state.data, id: payload };
      return { ...state, data };
    },
    saveData: (state, { payload }: PayloadAction<HarfType>) => {
      const data = { ...state.data, ...payload };
      return { ...state, data };
    },
    setData: (state, { payload }: PayloadAction<HarfType>) => {
      const data = { ...state.data, ...payload };
      return { ...state, data };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },

    setSaving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, saving: payload };
    },

    updateValue: (
      state,
      {
        payload: { name, value },
      }: PayloadAction<{ name: HarfNameType; value: string }>
    ) => {
      const data = { ...state.data };
      data[name] = value;
      return { ...state, data };
    },
    setUpdatedHarfData: (
      state,
      {
        payload: { name, value },
      }: PayloadAction<{ name: HarfNameType; value: string | null }>
    ) => {
      const updatedHarfData: UpdatedHarfDataType = {
        ...state.updatedHarfData,
      };
      if (name) updatedHarfData[name] = value;
      return { ...state, updatedHarfData };
    },
    clear: (state) => ({ ...state, data: emptyData }),
  },
});
export const { actions, reducer } = slice;
