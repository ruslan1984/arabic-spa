import { Loading } from "@components/types";

export type HarfType = {
  harfName: string;
  harfFree: string;
  harfFirst: string;
  harfCenter: string;
  harfLast: string;
  harfFreeImg: string;
  harfFirstImg: string;
  harfCenterImg: string;
  harfLastImg: string;
};

export type HarfNameType =
  | "harfName"
  | "harfFree"
  | "harfFirst"
  | "harfCenter"
  | "harfLast"
  | "harfFreeImg"
  | "harfFirstImg"
  | "harfCenterImg"
  | "harfLastImg";

export type UpdatedHarfDataType = {
  harfName?: string | null;
  harfFree?: string | null;
  harfFirst?: string | null;
  harfCenter?: string | null;
  harfLast?: string | null;
  harfFreeImg?: string | null;
  harfFirstImg?: string | null;
  harfCenterImg?: string | null;
  harfLastImg?: string | null;
};

export type OrthographyDetailType = {
  data: HarfType;
  loading: Loading;
  saving: Loading;
  removing: Loading;
  recovering: Loading;
  updatedHarfData: UpdatedHarfDataType;
};
