import React, { FC, FormEvent } from "react";
import { Label, Input, RemoveButton } from "@components/Elements/elements";
import { HarfLine, Line, ImgHarf } from "../../styles";
import DragAndDrop from "@components/DragAndDrop";

interface IPresenter {
  value: string;
  harfImg?: string;
  title: string;
  onDrop: (value: string) => void;
  updateText: (e: FormEvent) => void;
  remove: () => void;
}

const Presenter: FC<IPresenter> = ({
  value,
  harfImg,
  title,
  onDrop,
  updateText,
  remove,
}: IPresenter) => {
  return (
    <HarfLine>
      <Line>
        <Label htmlFor="">{title}</Label>
        <Input type="text" defaultValue={value} onChange={updateText} />
      </Line>
      <Line>
        <DragAndDrop
          expansion={["gif", "webp", "svg"]}
          readAsDataURL
          onDropResult={onDrop}
        />
        <ImgHarf active={Boolean(harfImg)} src={harfImg} alt="" />
        <RemoveButton onClick={remove} type="button">
          Удалить
        </RemoveButton>
      </Line>
    </HarfLine>
  );
};

export default Presenter;
