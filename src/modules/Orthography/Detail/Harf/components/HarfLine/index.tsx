import React, { FC, FormEvent } from "react";
import Presenter from "./Presenter";
import { useDispatch } from "react-redux";
import { actions } from "../../reducer";
import { HarfNameType } from "../../types";

interface IHarfLine {
  name: HarfNameType;
  nameImg: HarfNameType;
  value: string;
  harfImg?: string;
  title: string;
}

const HarfLine: FC<IHarfLine> = ({
  name,
  nameImg,
  value,
  harfImg,
  title,
}: IHarfLine) => {
  const dispatch = useDispatch();
  const onDrop = (value: string) => {
    dispatch(actions.setUpdatedHarfData({ name: nameImg, value }));
    dispatch(actions.updateValue({ name: nameImg, value }));
  };

  const updateText = (e: FormEvent) => {
    const value = (e.target as HTMLInputElement).value;
    dispatch(actions.setUpdatedHarfData({ name, value }));
    dispatch(actions.updateValue({ name, value }));
  };
  const remove = () => {
    dispatch(actions.setUpdatedHarfData({ name: nameImg, value: "" }));
    dispatch(actions.updateValue({ name: nameImg, value: "" }));
  };

  return (
    <Presenter
      value={value}
      harfImg={harfImg}
      title={title}
      onDrop={onDrop}
      updateText={updateText}
      remove={remove}
    />
  );
};

export default HarfLine;
