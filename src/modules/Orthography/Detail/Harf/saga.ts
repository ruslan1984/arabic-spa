import { takeEvery, put, call, select } from "redux-saga/effects";
import { actions, saveData } from "./reducer";
import { Loading } from "@components/types";
import { Query } from "@helpers/graphql";
import { reducerType } from "@store/reducers";
import { objToString } from "@helpers/functions";

export function* setOrthographyDetail(data: any) {
  try {
    const id = data.payload;
    yield put(actions.setLoading(Loading.start));
    const query = ` { 
      orthography(id: ${id}) {
          id
          harfName
          harfFree
          harfFirst
          harfCenter
          harfLast
          harfFreeImg
          harfFirstImg
          harfCenterImg
          harfLastImg
      }
    }`;
    const fetchData: {
      orthography: any;
    } = yield call(async () => await Query(query));
    yield put(actions.setData(fetchData.orthography));
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

function* saveDataSaga({ payload: lessonId }: { payload: number }) {
  try {
    yield put(actions.setSaving(Loading.start));
    const state: reducerType = yield select();
    const { updatedHarfData } = state.orthographyDetailHarf;
    const res = objToString(updatedHarfData);
    if (res) {
      const query = `mutation {
          updateOrthography(id: ${lessonId}, ${res}
            ) { id }
        }`;
      yield call(async () => await Query(query));
    }
    //yield put(actions.setData(dataSound));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

export function* orthographyDetailHarfSaga() {
  yield takeEvery(actions.setId, setOrthographyDetail);
  yield takeEvery(saveData, saveDataSaga);
}
