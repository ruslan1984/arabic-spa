import React, { FC, FormEvent, useEffect } from "react";
import { actions, saveData } from "./reducer";
import { reducerType } from "@store/reducers";
import Presenter from "./Presenter";
import { useSelector, useDispatch } from "react-redux";

interface IOrphographyHarf {
  lessonId: number;
}

const OrphographyHarf: FC<IOrphographyHarf> = ({
  lessonId,
}: IOrphographyHarf) => {
  const dispatch = useDispatch();
  const { data, loading, saving, removing, recovering } = useSelector(
    (state: reducerType) => state.orthographyDetailHarf
  );
  useEffect(() => {
    dispatch(actions.setId(lessonId));
  }, [lessonId]);

  useEffect(
    () => () => {
      dispatch(actions.clear());
    },
    [lessonId]
  );

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    dispatch(saveData(lessonId));
  };

  return <Presenter data={data} saving={saving} onSubmit={onSubmit} />;
};
export default OrphographyHarf;
