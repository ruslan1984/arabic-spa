import styled from "@emotion/styled";
import { boolean } from "@storybook/addon-knobs";

export const ImgHarf = styled.img`
  width: 50px;
  display: ${({ active }: { active?: boolean }) => (active ? "block" : "none")};
`;
export const Line = styled.div`
  display: flex;
  margin: 5px 0;
`;

export const HarfLine = styled.div`
  border-bottom: 1px solid #e0dede;
  margin: 10px 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
}
`;
