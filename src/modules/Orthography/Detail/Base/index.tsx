import React, { FC, FormEvent, useCallback, useState, useEffect } from "react";
import { actions, saveData } from "./reducer";
import { reducerType } from "@store/reducers";
import { useSelector, useDispatch } from "react-redux";
import Presenter from "./Presenter";
import { OrthographyDetail, NameType } from "./types";

interface IOrphographyBase {
  lessonId: number;
}

const OrphographyBase: FC<IOrphographyBase> = ({
  lessonId,
}: IOrphographyBase) => {
  const dispatch = useDispatch();
  const { data, loading, saving } = useSelector(
    (state: reducerType) => state.orthographyDetailBase
  );

  const [formData, setFormData] = useState<OrthographyDetail>(data);

  useEffect(() => {
    dispatch(actions.clearUpdatedData());
    dispatch(actions.setId(lessonId));
  }, [lessonId]);

  useEffect(() => {
    setFormData(data);
  }, [data]);

  useEffect(
    () => () => {
      dispatch(actions.clear());
    },
    []
  );
  const setFormValue = useCallback(
    (dataName: string) => (e: FormEvent) => {
      const value = (e.target as HTMLInputElement)?.value;
      if (value) {
        const name = dataName as NameType;
        if (name === "isAlphabet") {
          dispatch(actions.updateValue({ name, value: !data.isAlphabet }));
        } else {
          setFormData({
            ...formData,
            [name]: (e.target as HTMLInputElement)?.value,
          });
          dispatch(actions.updateValue({ name, value }));
        }
      }
    },
    [formData]
  );

  const setTextValue = (value: string, dataName: string) => {
    const name = dataName as NameType;
    if (formData && formData[name] !== value) {
      dispatch(actions.updateValue({ name, value }));
    }
  };

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    dispatch(saveData());
    dispatch(actions.clearUpdatedData());
  };

  const removeOrthography = () => {
    //alert("1");
  };

  const recoverOrthography = () => {
    //alert("1");
  };

  return (
    <Presenter
      formData={formData}
      saving={saving}
      loading={loading}
      setFormValue={setFormValue}
      setTextValue={setTextValue}
      onSubmit={onSubmit}
      removeOrthography={removeOrthography}
      recoverOrthography={recoverOrthography}
    />
  );
};
export default OrphographyBase;
