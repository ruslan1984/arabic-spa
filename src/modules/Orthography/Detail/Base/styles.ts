import styled from "@emotion/styled";

export const ImgHarf = styled.img`
  width: 50px;
  ${(props: { active?: boolean }) => {
    if (!props.active) {
      return "display: none";
    }
  }};
`;
export const Line = styled.div`
  display: flex;
  margin: 5px 0;
`;

export const HarfLine = styled.div`
  border-bottom: 1px solid #e0dede;
  margin: 10px 0;
`;
