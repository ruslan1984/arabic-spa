import { Loading } from "@components/types";

export type OrthographyDetail = {
  id: number | undefined;
  name: string | undefined;
  code: string | undefined;
  sort: string | undefined;
  textAbout: string | undefined;
  textForReading: string | undefined;
  title: string | undefined;
  isAlphabet: boolean | undefined;
  metaKeywords: string | undefined;
  metaDescription: string | undefined;
  deletedAt: string | null | undefined;
};

export type NameType =
  | "name"
  | "code"
  | "sort"
  | "isAlphabet"
  | "textAbout"
  | "textForReading"
  | "title"
  | "metaKeywords"
  | "metaDescription";

export type UpdatedDataType = {
  name?: string | null;
  code?: string | null;
  sort?: string | null;
  isAlphabet?: boolean | null;
  textAbout?: string | null;
  textForReading?: string | null;
  title?: string | null;
  metaKeywords?: string | null;
  metaDescription?: string | null;
};

export type OrthographyDetailType = {
  data: OrthographyDetail;
  loading: Loading;
  saving: Loading;
  updatedData: UpdatedDataType;
};
