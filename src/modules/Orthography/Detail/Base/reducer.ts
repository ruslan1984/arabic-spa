import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import {
  OrthographyDetailType,
  OrthographyDetail,
  NameType,
  UpdatedDataType,
} from "./types";
import { Loading } from "@components/types";
import { boolean } from "@storybook/addon-knobs";

export const defaultState: OrthographyDetailType = {
  data: {
    id: -1,
    name: "",
    code: "",
    sort: "",
    textAbout: "",
    textForReading: "",
    title: "",
    metaKeywords: "",
    metaDescription: "",
    isAlphabet: true,
    deletedAt: null,
  },
  updatedData: {},
  loading: Loading.no,
  saving: Loading.no,
};

export const saveData = createAction("saveData");

export const slice = createSlice({
  name: "orthographyDetailBase",
  initialState: defaultState,
  reducers: {
    setId: (state, { payload }: PayloadAction<number>) => {
      const data = { ...state.data, id: payload };
      return { ...state, data };
    },
    saveData: (state, { payload }: PayloadAction<OrthographyDetail>) => {
      const data = { ...state.data, ...payload };
      return { ...state, data };
    },
    setData: (state, { payload }: PayloadAction<OrthographyDetail>) => {
      const data = { ...state.data, ...payload };
      return { ...state, data };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    setUpdatedData: (
      state,
      { payload }: PayloadAction<{ name: NameType; value: any }>
    ) => {
      const updatedData: UpdatedDataType = {
        ...state.updatedData,
      };

      const name: NameType = payload.name;
      const value: any = payload.value;
      if (name) updatedData[name] = value;
      return { ...state, updatedData };
    },
    clearUpdatedData: (state) => ({ ...state, updatedData: {} }),
    updateValue: (
      state,
      {
        payload: { name, value },
      }: PayloadAction<{ name: NameType; value: any }>
    ) => {
      const data = { ...state.data };
      data[name] = value;
      const updatedData: UpdatedDataType = {
        ...state.updatedData,
      };
      if (name) updatedData[name] = value;
      return { ...state, data, updatedData };
    },
    setSaving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, saving: payload };
    },

    removeOrthography: (state) => {
      const data = { ...state.data };
      //data[ "deletedAt" ] = "now";
      return { ...state };
      //return { ...state, data };
    },
    recoverOrthography: (state) => {
      const data = { ...state.data };
      //data[ "deletedAt" ] = null;
      return { ...state };
      //return { ...state, data };
    },
    clear: (state) => {
      const correntData = { ...state.data };
      const obj: any = [];
      const data = Object.entries(correntData).reduce((res, [key, value]) => {
        if (value && key) {
          res[key] = "";
        }
        return res;
      }, obj);
      return { ...state, data };
    },
  },
});
export const { actions, reducer } = slice;
