import { takeEvery, put, call, select } from "redux-saga/effects";
import { actions, saveData } from "./reducer";
import { Query } from "@helpers/graphql";
import { Loading } from "@components/types";
import { reducerType } from "@store/reducers";
import { objToString, strToObj } from "@helpers/functions";

export function* newOrthographyData() {
  const gl = {
    id: 0,
    name: "",
    title: "",
    code: "",
    metaKeywords: "",
    metaDescription: "",
    harfName: "",
    harfFree: "",
    harfFirst: "",
    harfCenter: "",
    harfLast: "",
    harfFreeImg: "",
    harfFirstImg: "",
    harfCenterImg: "",
    harfLastImg: "",
    harfNameSound: "",
    harfFathaSound: "",
    harfKesraSound: "",
    harfDammaSound: "",
    harfSakenSound: "",
    imgTell: "",
    textAbout: "",
    textForReading: "",
    deletedAt: null,
    isAlphabet: true,
    sort: 10,
  };
  //yield put(actions.setData(gl));
}
export function* getOrthographyData(id: number) {
  try {
    yield put(actions.setLoading(Loading.start));

    const query = ` { 
      orthography(id: ${id}) {
          id
          name 
          code
          title
          metaKeywords
          metaDescription
          textAbout
          textForReading
          isAlphabet
          sort
      }
    }`;

    const fetchData: {
      orthography: any;
    } = yield call(async () => await Query(query));

    const data = strToObj(fetchData.orthography);
    yield put(actions.setData(data));
    yield put(actions.setLoading(Loading.ok));
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}
export function* setOrthographyDetail(data: any) {
  const id = data.payload;
  if (id == 0) {
    yield newOrthographyData();
  } else {
    yield getOrthographyData(id);
  }
}

export function* saveDataSaga() {
  try {
    yield put(actions.setSaving(Loading.start));
    const state: reducerType = yield select();
    const { updatedData, data } = state.orthographyDetailBase;
    const res = objToString(updatedData);
    if (res) {
      const query = `mutation {
          updateOrthography(id: ${data.id}, ${res}
            ) { id }
        }`;
      yield call(async () => await Query(query));
    }
    yield put(actions.setData(data));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

export function* add(id: number, data: any) {
  try {
    yield put(actions.setSaving(Loading.start));
    //const fetchData = yield call(
    //  async () => await fetchPost(`/api/orthography/${id}`, data)
    //);
    //if (fetchOk(fetchData.status)) {
    //  yield put(actions.setData(fetchData.data));
    //} else {
    //  alert("error");
    //}
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

//export function* saveDataSaga(data: any) {
//  const { id } = data.payload;
//  const OrthographyData = data.payload;
//  if (id > 0) {
//    yield save(id, OrthographyData);
//  } else {
//    yield add(id, OrthographyData);
//  }
//}

export function* removeOrthography(data: any) {
  const id = data.payload;
  try {
    //yield put(
    //  actions.setRemoving({ id: IDBDatabase, removing: Loading.start })
    //);
    //const fetchData = yield call(
    //  async () => await fetchDelete(`/api/orthography/${id}`)
    //);
    //if (!fetchOk(fetchData.status)) {
    //  alert("error");
    //}
  } catch (err) {
    console.error(err);
  } finally {
    //yield put(actions.setRemoving({ id, removing: Loading.ok }));
  }
}
export function* recoverOrthography(data: any) {
  const id = data.payload;
  try {
    //yield put(actions.setRecovering({ id, recovering: Loading.start }));
    //const fetchData = yield call(
    //  async () => await fetchPut(`/api/orthography/recover/${id}`)
    //);
    //if (!fetchOk(fetchData.status)) {
    //  alert("error");
    //}
  } catch (err) {
    console.error(err);
  } finally {
    //yield put(actions.setRecovering({ id, recovering: Loading.ok }));
  }
}

export function* orthographyDetailSaga() {
  yield takeEvery(actions.setId, setOrthographyDetail);
  yield takeEvery(saveData, saveDataSaga);
  yield takeEvery(actions.removeOrthography, removeOrthography);
  yield takeEvery(actions.recoverOrthography, recoverOrthography);
}
