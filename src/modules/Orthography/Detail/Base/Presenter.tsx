import React, { FC, FormEvent } from "react";
import {
  Label,
  Input,
  Page,
  Button,
  RemoveButton,
  RecoverButton,
  AddLink,
  Footer,
} from "@components/Elements/elements";
import { Line } from "./styles";
import Editor from "@components/Editor";
import { Loading } from "@components/types";
import { OrthographyDetail } from "./types";
import { Checkbox } from "@components/Elements/elements";
import { boolean } from "@storybook/addon-knobs";

interface DetailBaseProps {
  formData: OrthographyDetail | undefined;
  onSubmit: (e: FormEvent) => void;
  removeOrthography: () => void;
  recoverOrthography: () => void;
  setFormValue: (name: string) => (e: FormEvent) => void;
  setTextValue: (value: string, name: string) => void;
  saving: Loading;
  loading: Loading;
}

export const Presenter: FC<DetailBaseProps> = ({
  saving,
  formData,
  loading,
  onSubmit,
  setFormValue,
  setTextValue,
  removeOrthography,
  recoverOrthography,
}: DetailBaseProps) => {
  return (
    <>
      <h1>
        Орфография. {formData?.name} ({formData?.id})
      </h1>
      <Page loading={loading}>
        <form action="" onSubmit={onSubmit}>
          <Line>
            <Label htmlFor="">Название</Label>
            <Input
              name="name"
              type="text"
              value={formData?.name || ""}
              onChange={setFormValue("name")}
              required
            />
          </Line>
          <Line>
            <Label htmlFor="">Код</Label>
            <Input
              name="code"
              type="text"
              value={formData?.code || ""}
              onChange={setFormValue("code")}
              required
            />
          </Line>
          <Line>
            <Label htmlFor="">Сортировка</Label>
            <Input
              type="number"
              min={0}
              name="sort"
              value={formData?.sort || ""}
              onChange={setFormValue("sort")}
            />
          </Line>
          <Line>
            <Label htmlFor="">Буква алфавита</Label>
            <Checkbox
              checked={Boolean(formData?.isAlphabet)}
              onChange={setFormValue("isAlphabet")}
            />
          </Line>
          <h2>Произношение</h2>
          <Editor
            name="textAbout"
            value={formData?.textAbout || ""}
            onChange={setTextValue}
          />
          <hr />
          <h2>Для чтения</h2>
          <Editor
            name="textForReading"
            value={formData?.textForReading || ""}
            onChange={setTextValue}
          />
          <h2>SEO</h2>
          <Line>
            <Label htmlFor="">title</Label>
            <Input
              name="title"
              type="text"
              value={formData?.title || ""}
              onChange={setFormValue("title")}
            />
          </Line>
          <Line>
            <Label htmlFor="">metaKeywords</Label>
            <Input
              name="metaKeywords"
              type="text"
              value={formData?.metaKeywords || ""}
              onChange={setFormValue("metaKeywords")}
            />
          </Line>
          <Line>
            <Label htmlFor="">metaDescription</Label>
            <Input
              name="metaDescription"
              type="text"
              value={formData?.metaDescription || ""}
              onChange={setFormValue("metaDescription")}
            />
          </Line>
          <Footer>
            <Button type="submit" saving={saving}>
              Сохранить
            </Button>
            {formData?.deletedAt ? (
              <RecoverButton
                saving={saving}
                onClick={recoverOrthography}
                type="button"
              >
                Восстановить
              </RecoverButton>
            ) : (
              <RemoveButton
                //saving={saving}
                onClick={removeOrthography}
                type="button"
              >
                Удалить
              </RemoveButton>
            )}
            <AddLink to={"../0/base"}>Добавить</AddLink>
          </Footer>
        </form>
      </Page>
    </>
  );
};

export default Presenter;
