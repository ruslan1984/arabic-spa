import React, { FC, FormEvent } from "react";
import { Page, Button, Footer } from "@components/Elements/elements";
import { Loading } from "@components/types";
import { Input } from "@components/Elements/elements";
import SoundLine from "./components/SoundLine";
import { SoundType } from "./types";
import { SoundSymbol } from "./styles";
import { Label } from "@components/Elements/elements";

interface ISound {
  dataSound: SoundType;
  symbol: string;
  saving?: Loading;
  loading?: Loading;
  saveData: (e: FormEvent) => void;
  removeSound?: (harf: string) => void;
  updateSymbol: (e: FormEvent<HTMLInputElement>) => void;
  update: (e: FormEvent<HTMLInputElement>) => void;
}

export const Presenter: FC<ISound> = ({
  dataSound,
  saving,
  symbol,
  loading,
  saveData,
  updateSymbol,
  update,
}: ISound) => {
  return (
    <>
      <h2>
        Орфография. Звучание.
        {dataSound.name}({dataSound.id})
      </h2>
      <Page loading={loading}>
        <form action="" onSubmit={saveData}>
          <SoundSymbol>
            <Label>Написание</Label>
            <Input
              onChange={updateSymbol}
              name="harfSoundSymbol"
              value={dataSound.harfSoundSymbol || ""}
            />
          </SoundSymbol>
          <SoundSymbol>
            <Label>Транскрипция</Label>
            <Input
              onChange={update}
              name="transcription"
              value={dataSound.transcription || ""}
            />
          </SoundSymbol>
          <SoundLine
            harf={dataSound.harfName}
            sound={dataSound["harfNameSound"]}
            label="Название буквы"
            name="harfNameSound"
          />
          <SoundLine
            harf={`${symbol}َ `}
            sound={dataSound["harfFathaSound"]}
            label="Произношение с фатхой"
            name="harfFathaSound"
          />
          <SoundLine
            harf={`${symbol}ِ `}
            sound={dataSound["harfKesraSound"]}
            label="Произношение c касрой"
            name="harfKesraSound"
          />
          <SoundLine
            harf={`${symbol}ُ `}
            sound={dataSound["harfDammaSound"]}
            label="Произношение c даммой"
            name="harfDammaSound"
          />
          <Footer>
            <Button type="submit" saving={saving}>
              Сохранить
            </Button>
          </Footer>
        </form>
      </Page>
    </>
  );
};

export default Presenter;
