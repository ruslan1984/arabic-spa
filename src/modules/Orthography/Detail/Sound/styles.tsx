import styled from "@emotion/styled";

export const SoundSymbol = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #e0dede;
  padding: 10px 0;
`;
