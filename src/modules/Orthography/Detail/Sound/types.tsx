import { Loading } from "@components/types";

export type SoundType = {
  id: number;
  name: string;
  harfName: string;
  harfFree: string;
  harfSoundSymbol?: string | null;
  harfNameSound: string | null;
  harfFathaSound: string | null;
  harfKesraSound: string | null;
  harfDammaSound: string | null;
  transcription: string | null;
  //harfSakenSound: string;
};

export type SoundNameType =
  | "harfNameSound"
  | "harfFathaSound"
  | "harfKesraSound"
  | "harfDammaSound"
  | "transcription"
  | "harfSoundSymbol";

export type UpdatedSoundDataType = {
  harfNameSound?: string | null;
  harfFathaSound?: string | null;
  harfKesraSound?: string | null;
  harfDammaSound?: string | null;
  harfSoundSymbol?: string | null;
  transcription?: string | null;
};

export type OrthographyDetailType = {
  dataSound: SoundType;
  loading: Loading;
  saving: Loading;
  removing: Loading;
  recovering: Loading;
  updatedSoundData?: UpdatedSoundDataType;
};

export type DefaultType = { id: number; name: string };
