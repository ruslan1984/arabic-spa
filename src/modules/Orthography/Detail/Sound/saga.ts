import { takeEvery, put, call, select } from "redux-saga/effects";
import { actions, saveData } from "./reducer";
import { Query } from "@helpers/graphql";
import { reducerType } from "@store/reducers";
import { objToString } from "@helpers/functions";
import { Loading } from "@components/types";

function* setData({ payload: id }: { payload: number }) {
  try {
    yield put(actions.setLoading(Loading.start));
    const query = ` { 
      orthography(id: ${id}) {
          id
          name
          harfFree
          harfName
          harfSoundSymbol
          harfNameSound
          harfFathaSound
          harfKesraSound
          harfDammaSound
          transcription
          isAlphabet
      }
    }`;
    const fetchData: {
      orthography: any;
    } = yield call(async () => await Query(query));
    yield put(actions.setDataSound(fetchData.orthography));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

function* saveDataSaga() {
  try {
    yield put(actions.setSaving(Loading.start));
    const state: reducerType = yield select();
    const { updatedSoundData, dataSound } = state.orthographyDetailSound;
    const res = objToString(updatedSoundData);
    if (res) {
      const query = `mutation {
          updateOrthography(id: ${dataSound.id}, ${res}
            ) { id }
        }`;
      yield call(async () => await Query(query));
    }
    yield put(actions.setDataSound(dataSound));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

export function* orthographyDetailSoundSaga() {
  yield takeEvery(actions.setId, setData);
  yield takeEvery(saveData, saveDataSaga);
}
