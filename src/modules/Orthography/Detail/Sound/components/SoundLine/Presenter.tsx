import React, { FC } from "react";
import { Line, SoundImg, HarfLine, RemoveImg, Harf } from "./styles";
import DragAndDrop from "@components/DragAndDrop";
import VolumeIcon from "@assets/icons/volume.svg";
import Remove from "@assets/icons/remove.svg";
import { Label } from "@components/Elements/elements";

interface ISoundLinePresenter {
  label: string;
  harf: string;
  sound?: string | null;
  onRemove: () => void;
  onPlay: () => void;
  onDrop: (value: string) => void;
}

const Presenter: FC<ISoundLinePresenter> = ({
  label,
  sound,
  harf,
  onPlay,
  onDrop,
  onRemove,
}: ISoundLinePresenter) => {
  return (
    <HarfLine>
      <Line>
        <Label htmlFor="">{label}</Label>
        {harf && <Harf>{harf}</Harf>}
        <DragAndDrop expansion={["mp3"]} readAsDataURL onDropResult={onDrop} />
        {sound && (
          <>
            <SoundImg
              onClick={onPlay}
              src={VolumeIcon.src}
              alt=""
              title="Прослушать"
            />
            <RemoveImg
              onClick={onRemove}
              src={Remove}
              alt="Удалить"
              title="Удалить"
            />
          </>
        )}
      </Line>
    </HarfLine>
  );
};

export default Presenter;
