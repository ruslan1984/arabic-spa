import styled from "@emotion/styled";

export const Line = styled.div`
  display: flex;
  align-items: center;
  margin: 5px 0;
`;

export const HarfLine = styled.div`
  border-bottom: 1px solid #e0dede;
  margin: 10px 0;
`;

export const SoundImg = styled.img`
  width: 50px;
  margin-left: 50px;
  cursor: pointer;
`;

export const RemoveImg = styled.img`
  width: 30px;
  cursor: pointer;
  margin-left: 10px;
`;

export const Harf = styled.div`
  font-size: 40px;
  min-width: 50px;
  text-align: center;
  margin: 10px 20px;
`;
