import React, { FC } from "react";
import { actions } from "@modules/Orthography/Detail/Sound/reducer";
import { useDispatch } from "react-redux";
import { SoundNameType } from "@modules/Orthography/Detail/Sound/types";
import Presenter from "./Presenter";

interface ISoundLine {
  label: string;
  harf: string;
  sound: string | null;
  name: SoundNameType;
}

const SoundLine: FC<ISoundLine> = ({
  label,
  harf,
  name,
  sound,
}: ISoundLine) => {
  const dispatch = useDispatch();
  const onDrop = (value: string) => {
    dispatch(actions.setUpdatedSoundData({ name, value }));
    dispatch(actions.updateValue({ name, value }));
  };

  const onPlay = async () => {
    try {
      if (sound) {
        const audio = new Audio(sound);
        audio.load();
        audio.play();
      }
    } catch (err: any) {
      console.error(err?.mesaage);
    }
  };
  const onRemove = async () => {
    dispatch(actions.removeSund(name));
    dispatch(actions.setUpdatedSoundData({ name, value: "" }));
  };

  return (
    <Presenter
      harf={harf}
      sound={sound}
      label={label}
      onRemove={onRemove}
      onPlay={onPlay}
      onDrop={onDrop}
    />
  );
};

export default SoundLine;
