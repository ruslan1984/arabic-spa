import React, { FC, FormEvent, useEffect, useState } from "react";
import { actions, saveData } from "./reducer";
import { reducerType } from "@store/reducers";

import Presenter from "./Presenter";
import { useSelector, useDispatch } from "react-redux";
import { SoundNameType } from "./types";

interface IOrphographySound {
  lessonId: number;
}

const OrphographySound: FC<IOrphographySound> = ({
  lessonId,
}: IOrphographySound) => {
  const dispatch = useDispatch();
  const { dataSound, saving, loading } = useSelector(
    (state: reducerType) => state.orthographyDetailSound
  );

  useEffect(() => {
    dispatch(actions.setId(lessonId));
  }, [lessonId, dispatch]);

  const [symbol, setSymbol] = useState("");

  useEffect(() => {
    dataSound.harfSoundSymbol
      ? setSymbol(dataSound.harfSoundSymbol)
      : setSymbol(dataSound.harfFree);
  }, [dataSound]);

  useEffect(
    () => () => {
      dispatch(actions.clear());
    },
    []
  );

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    dispatch(saveData());
  };

  const updateSymbol = (e: FormEvent<HTMLInputElement>) => {
    const value = e.currentTarget.value;
    dispatch(actions.setUpdatedSoundData({ name: "harfSoundSymbol", value }));
    dispatch(actions.updateValue({ name: "harfSoundSymbol", value }));
    dataSound.harfSoundSymbol
      ? setSymbol(dataSound.harfSoundSymbol)
      : setSymbol(dataSound.harfFree);
  };

  const update = (e: FormEvent<HTMLInputElement>) => {
    const { name, value } = e.currentTarget as {
      name: SoundNameType;
      value: string;
    };
    dispatch(actions.setUpdatedSoundData({ name, value }));
    dispatch(actions.updateValue({ name, value }));
  };

  return (
    <>
      <Presenter
        symbol={symbol}
        loading={loading}
        dataSound={dataSound}
        saving={saving}
        saveData={onSubmit}
        updateSymbol={updateSymbol}
        update={update}
      />
    </>
  );
};

export default OrphographySound;
