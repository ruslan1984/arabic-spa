import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import {
  OrthographyDetailType,
  SoundType,
  UpdatedSoundDataType,
  SoundNameType,
} from "./types";
import { Loading } from "@components/types";

export const defaultState: OrthographyDetailType = {
  dataSound: {
    id: 0,
    name: "",
    harfName: "",
    harfFree: "",
    harfNameSound: "",
    harfFathaSound: "",
    harfKesraSound: "",
    harfDammaSound: "",
    transcription: "",
  },
  updatedSoundData: {},
  loading: Loading.no,
  saving: Loading.no,
  removing: Loading.no,
  recovering: Loading.no,
};

export const saveData = createAction("saveData");

export const soundSlice = createSlice({
  name: "orthographyDetailSound",
  initialState: defaultState,
  reducers: {
    setId: (state, { payload }: PayloadAction<number>) => {
      const dataSound = { ...state.dataSound, id: payload };
      return { ...state, dataSound };
    },
    setDataSound: (state, { payload }: PayloadAction<SoundType>) => {
      const dataSound = { ...state.dataSound, ...payload };
      return { ...state, dataSound };
    },
    updateValue: (
      state,
      { payload }: PayloadAction<{ name: SoundNameType; value: string }>
    ) => {
      const dataSound = { ...state.dataSound };
      dataSound[payload.name] = payload.value;
      return { ...state, dataSound };
    },
    setUpdatedSoundData: (
      state,
      { payload }: PayloadAction<{ name: SoundNameType; value: string | null }>
    ) => {
      const updatedSoundData: UpdatedSoundDataType = {
        ...state.updatedSoundData,
      };
      const name: SoundNameType = payload.name;
      const value: string | null = payload.value;
      if (name) updatedSoundData[name] = value;
      return { ...state, updatedSoundData };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    setSaving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, saving: payload };
    },
    removeSund: (state, { payload }: PayloadAction<SoundNameType>) => {
      const dataSound = { ...state.dataSound };
      dataSound[payload] = null;
      return { ...state, dataSound };
    },
    clear: (state) => {
      const correntData = { ...state.dataSound };
      const obj: any = [];
      const dataSound = Object.entries(correntData).reduce(
        (res, [key, value]) => {
          if (value && key) {
            res[key] = "";
          }
          return res;
        },
        obj
      );
      return { ...state, dataSound };
    },
  },
});

export const { actions, reducer } = soundSlice;
