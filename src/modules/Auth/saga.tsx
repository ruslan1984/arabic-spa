import { put, call, takeEvery } from "redux-saga/effects";
import { setToken, clearToken } from "@helpers/tokens";
//import {
//  login as sessionLogin,
//  logout as sessionLogout,
//  isAuthorised,
//} from "./session";
//import { login as serverLogin } from "./data";
import { actions } from "./reducer";
import { fetchPost } from "@/helpers/fetch";
import { action } from "@storybook/addon-actions";

//export function* checkUserSession() {
//  const auth: boolean = yield call(isAuthorised);
//  if (auth) {
//    yield put(actions.login());
//  } else {
//    yield put(actions.logout());
//  }
//}

export function* auth({ payload }: any) {
  try {
    const data = {
      grant_type: "password",
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      username: payload.user,
      password: payload.password,
      scope: "*",
    };
    const fetchData: { access_token: string } = yield call(
      async () => await fetchPost(`/oauth/token`, data)
    );
    if (fetchData?.access_token) {
      yield call(setToken, fetchData.access_token);
      yield put(actions.login());
      location.href = "/admin";
    } else {
      yield call(clearToken);
    }
  } catch (err) {
    yield call(clearToken);
    console.error(err);
  }
}
export function* logout() {
  yield call(clearToken);
  location.href = "/auth";
}

export function* loginSaga() {
  yield takeEvery(actions.logout, logout);
  yield takeEvery(actions.auth, auth);
}
