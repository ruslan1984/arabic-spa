import React, { FormEvent } from "react";
import Presenter from "./Presenter";
import { useDispatch } from "react-redux";
import { actions } from "./reducer";
//import { CheckState, Login } from "./types";
//import { useSelector } from "react-redux";

export const AuthPage = () => {
  const dispatch = useDispatch();
  //const { status } = useSelector((state: any) => state.auth);
  const submit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const target = e.target as HTMLFormElement;
    const user = (target.querySelector("[name=name]") as HTMLInputElement)
      .value;
    const password = (
      target.querySelector("[name=password]") as HTMLInputElement
    ).value;
    dispatch(actions.auth({ user, password }));
  };

  //if (status === CheckState.succeed) {
  //  return <Navigate to="/admin" />;
  //}
  return <Presenter submit={submit} />;
};

export default AuthPage;
