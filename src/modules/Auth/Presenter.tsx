import {
  Input,
  Name,
  Label,
  Button,
  Page,
} from "@components/Elements/elements";
import React, { FC, FormEventHandler } from "react";
//import { CheckState } from "@modules/Auth/types";

interface AuthProps {
  submit: FormEventHandler;
}
const Presenter: FC<AuthProps> = (props: AuthProps) => {
  return (
    <Page>
      <h1>Авторизация</h1>
      <form action="" onSubmit={props.submit}>
        <Label>
          <Name>Логин</Name>
          <Input type="text" name="name" placeholder="Логин" />
        </Label>
        <Label>
          <Name>Пароль</Name>
          <Input type="password" name="password" placeholder="Пароль" />
        </Label>
        <Button>Вход</Button>
        {/*{(() => {
          if (props.status === CheckState.failed) {
            return "Ошибка авторизации";
          }
          if (props.status === CheckState.zeroData) {
            return "Пустые данные";
          }
        })()}*/}
      </form>
    </Page>
  );
};

export default Presenter;
