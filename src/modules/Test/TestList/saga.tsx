import { takeEvery, put, call, select } from "redux-saga/effects";
import { Status } from "@components/types";
import {
  actions,
  grammarTestListGet,
  grammarTestListBasketGet,
  grammarTestListSave,
} from "./reducer";
import { Loading } from "@components/types";
import { TTest } from "@modules/Test/types";
import { fetchGet, fetchPost } from "@helpers/fetch";
import { reducerType } from "@store/reducers";

function* getList(url: string) {
  try {
    yield put(actions.setLoading(Loading.start));
    const fetchData: TTest[] = yield call(async () => await fetchGet(url));
    yield put(actions.setTestListData(fetchData));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

function* grammarTestListGetSaga(data: { payload: number }) {
  yield getList(`/api/test_list/${data.payload}/grammar`);
}

function* grammarTestListBasketGetSaga(data: { payload: number }) {
  yield getList(`/api/test_list/${data.payload}/grammar_basket`);
}

function* grammarTestListSaveSaga() {
  try {
    yield put(actions.setLoading(Loading.start));
    const state: reducerType = yield select();
    const { data } = state.testList;
    const addTest = data
      .filter((item) => item.id === 0)
      .map(({ id, ...item }) => ({ ...item, status: Status.published }));
    const url = "/api/test/careate_by_list";
    yield call(async () => await fetchPost(url, addTest));
    yield put(actions.setTestListData(addTest));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* testListSaga() {
  yield takeEvery(grammarTestListGet, grammarTestListGetSaga);
  yield takeEvery(grammarTestListBasketGet, grammarTestListBasketGetSaga);
  yield takeEvery(grammarTestListSave, grammarTestListSaveSaga);
}
