import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import { actions } from "@storybook/addon-actions";

import Presenter from "./Presenter";

export default {
  title: "Test",
  decorators: [withKnobs],
};
const data = {
  testId: 1,
  test: [
    {
      questionId: 1,
      question:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem consectetur veritatis impedit numquam fugiat itaque vero nesciunt minima, quibusdam maxime quam quod, atque id consequuntur labore enim accusantium cum optio.",
      answers: [
        {
          text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis, doloribus.",
          trueAnswer: false,
        },
        {
          text: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias assumenda necessitatibus rem ut beatae aliquam.",
          trueAnswer: true,
        },
        {
          text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit ex, earum illum officia ducimus voluptatibus quod nihil quibusdam error voluptates.",
          trueAnswer: false,
        },
      ],
    },
    {
      questionId: 2,
      question:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod iusto iste assumenda itaque facilis eaque eum. Eaque voluptas quas minus.",
      answers: [
        {
          text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla delectus, blanditiis quae culpa vitae recusandae?",
          trueAnswer: false,
        },
        {
          text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam porro aut laudantium pariatur deserunt ut rem.",
          trueAnswer: true,
        },
        {
          text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores molestiae suscipit pariatur commodi autem cumque deleniti. Aut.",
          trueAnswer: false,
        },
      ],
    },
  ],
};
export const Test = () => {
  return <>Поправить</>; //<Presenter data={data} onSubmit={actions} />;
};
