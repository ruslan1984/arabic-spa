import { Loading } from "@components/types";
import { TTest } from "@modules/Test/types";

export type TestListReducerType = {
  loading: Loading;
  data: TTest[];
};
