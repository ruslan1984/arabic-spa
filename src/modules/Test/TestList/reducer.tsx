import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import { TTest } from "@modules/Test/types";
import { TestListReducerType } from "./types";
import { Loading } from "@components/types";

export const defaultState: TestListReducerType = {
  data: [],
  loading: Loading.no,
};

export const grammarTestListGet = createAction<number>("grammarTestListGet");
export const grammarTestListBasketGet = createAction<number>(
  "grammarTestListBasketGet"
);
export const grammarTestListSave = createAction<number>("grammarTestListSave");

export const testSlice = createSlice({
  name: "testList",
  initialState: defaultState,
  reducers: {
    setTestListData: (state, { payload }: PayloadAction<TTest[]>) => {
      return { ...state, data: payload };
    },
    addTestListData: (state, { payload }: PayloadAction<TTest[]>) => {
      const data = [...state.data, ...payload];
      return { ...state, data };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    clear: (state) => {
      return { ...state, data: [] };
    },
  },
});
export const { actions, reducer } = testSlice;
