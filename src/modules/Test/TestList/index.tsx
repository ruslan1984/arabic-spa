import React, { FC, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  actions,
  grammarTestListSave,
  grammarTestListGet,
  grammarTestListBasketGet,
} from "./reducer";
import { reducerType } from "@store/reducers";
import Presenter from "./Presenter";
import { Loading } from "@components/types";
import { Page } from "@components/Elements/elements";

interface TestListProps {
  lessonId: number;
  type: string;
}

export const TestList: FC<TestListProps> = ({
  lessonId,
  type,
}: TestListProps) => {
  const dispatch = useDispatch();
  const { data, loading } = useSelector((state: reducerType) => state.testList);

  useEffect(() => {
    switch (type) {
      case "grammar":
        dispatch(grammarTestListGet(lessonId));
        break;
      case "grammar_basket":
        dispatch(grammarTestListBasketGet(lessonId));
        break;
    }
  }, [lessonId]);

  useEffect(
    () => () => {
      dispatch(actions.clear());
    },
    []
  );

  const onSave = useCallback(() => {
    dispatch(grammarTestListSave(lessonId));
  }, [lessonId]);

  return loading == Loading.ok ? (
    <Presenter onSave={onSave} data={data} lessonId={lessonId} />
  ) : (
    <Page loading={loading}> Загрузка...</Page>
  );
};

export default TestList;
