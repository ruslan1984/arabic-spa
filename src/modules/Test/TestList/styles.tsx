import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import { Status } from "@components/types";
export const Answer = styled.div`
  display: flex;
  align-items: center;
  margin: 10px;
`;

export const AnswerText = styled.div`
  margin-left: 10px;
`;

export const EditQuestionLink = styled(Link)`
  color: black;
  text-decoration: none;
  display: block;
  padding: 5px 10px;
  transition: 0.2s;
  &:hover {
    background: #f9f9f9;
  }
  ${(props: { status?: Status }) => {
    if (props.status === Status.draft) {
      return "color: #696868; background: #e8e6e6;";
    }
  }};
`;

export const NewQuestionLink = styled(Link)`
  padding: 12px 20px;
  font-size: 15px;
  cursor: pointer;
  background: #87f187;
  color: #191818;
  border: none;
  text-transform: uppercase;
  display: inline-block;
  text-decoration: none;
  margin: 5px;
  &:hover {
    transition: 0.5s;
    background: #1be61b;
  }
  &:focus {
    outline: none;
  }
`;

export const QuestionTitle = styled.div`
  font-size: 20px;
`;
export const MixedIcon = styled.img`
  width: 30px;
`;
export const Head = styled.div`
  display: flex;
`;
export const H2 = styled.h2`
  margin-right: 15px;
`;
