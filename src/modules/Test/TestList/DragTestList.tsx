import React, { FC, useCallback } from "react";
import DragAndDrop, { DropType } from "@components/DragAndDrop";
import { Status } from "@components/types";
import { actions } from "./reducer";
import { useDispatch } from "react-redux";
import { TAnswer, TTest } from "@modules/Test/types";

interface IDragTestList {
  lessonId: number;
}

const DragTestList: FC<IDragTestList> = ({ lessonId }: IDragTestList) => {
  const dispatch = useDispatch();
  const click = useCallback((dropData: DropType) => {
    const data: string = (dropData as string).replaceAll("\n\n", "\n");

    const testArray = data.split("\n");
    const test: TTest[] = [];
    let questionText = "";
    let answerList: Array<TAnswer> = [];
    testArray.forEach((item: string) => {
      let str: string = item;
      if (str.trim() === "" && questionText.trim() !== "") {
        const question: TTest = {
          id: 0,
          questionText,
          answers: answerList,
          status: Status.draft,
          lessonId,
        };
        test.push(question);
        questionText = "";
        answerList = [];
      } else if (str.substring(0, 3).trim() === "") {
        const errorText = str.trim();
        answerList[answerList.length - 1].errorText = errorText;
      } else if (str.substring(0, 2).trim() === "") {
        const text = str.trim();
        answerList.push({
          text,
          trueAnswer: false,
          removed: false,
        });
      } else if (str.substring(0, 1).trim() === "") {
        const text = str.trim();
        answerList.push({
          text,
          trueAnswer: true,
          removed: false,
        });
      } else {
        str = str.trim();
        questionText = str;
      }
    });
    dispatch(actions.addTestListData(test));
  }, []);
  return <DragAndDrop onDropResult={click} />;
};

export default DragTestList;
