import { actions, reducer, defaultState } from "./reducer";
import { Loading } from "@components/types";
import { random, lorem } from "faker";

describe("Grammar test lessen", () => {
  const state = {
    loading: Loading.no,
    data: [
      {
        questionId: 1,
        questionText: lorem.text,
        status: 1,
        answers: [
          {
            answerNumber: 1,
            text: lorem.text,
            errorText: lorem.text,
            trueAnswer: true,
          },
          {
            answerNumber: 2,
            text: "string",
            errorText: "string1",
            trueAnswer: false,
          },
        ],
      },
    ],
  };
  it("setQuestionList", () => {
    const id = random.number();
    expect(reducer({ lessonId: 0 }, actions.setQuestionList(id))).toEqual({
      lessonId: id,
    });
  });
  it("setQuestionListData", () => {
    expect(
      reducer({ lessonId: 0 }, actions.setQuestionListData(state.data))
    ).toEqual({
      data: state.data,
      lessonId: 0,
    });
  });
  it("setTrueAnswer", () => {
    const payload = { questionId: 1, answerId: 2 };
    const data1 = state.data.map((questionItem) => {
      if (questionItem.questionId === payload.questionId) {
        const answers = questionItem.answers.map((answerItem) => {
          if (answerItem.answerNumber === payload.answerId) {
            return { ...answerItem, trueAnswer: !answerItem.trueAnswer };
          }
          return answerItem;
        });
        return { ...questionItem, answers };
      }
      return questionItem;
    });
    expect(reducer(state, actions.setTrueAnswer(payload))).toEqual({
      ...state,
      data: data1,
    });
  });

  it("setLoading", () => {
    expect(reducer(state, actions.setLoading(Loading.start))).toEqual({
      ...state,
      loading: Loading.start,
    });
  });
});
