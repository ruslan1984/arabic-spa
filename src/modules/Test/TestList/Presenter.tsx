import React, { FC } from "react";
import { Checkbox } from "@components/Elements/elements";
import {
  Answer,
  AnswerText,
  EditQuestionLink,
  NewQuestionLink,
  QuestionTitle,
  MixedIcon,
  Head,
  H2,
} from "./styles";
import { TAnswer, TTest } from "@modules/Test/types";
import routes from "@routes/routes";
import { Button } from "@components/Elements/elements";
import DragTestList from "./DragTestList";
import mixed from "@assets/icons/mixed/mixed.svg";
import nomixed from "@assets/icons/mixed/nomixed.svg";

interface TestProps {
  lessonId: number;
  data: TTest[];
  onSave: () => void;
}

export const Presenter: FC<TestProps> = ({
  data,
  lessonId,
  onSave,
}: TestProps) => {
  return (
    <>
      <NewQuestionLink to={routes.grammar.TestCreate(lessonId)}>
        Добавить вопрос
      </NewQuestionLink>
      {data && (
        <>
          <NewQuestionLink to={routes.grammar.Testing(lessonId)}>
            Пройти тест
          </NewQuestionLink>
          <DragTestList lessonId={lessonId} />
        </>
      )}
      {data &&
        data.map((testItem: TTest, index: number) => {
          return (
            <div key={String(index)}>
              <EditQuestionLink
                status={testItem.status}
                to={`test/${testItem.id}`}
              >
                <Head>
                  <H2>Вопрос {index + 1}</H2>
                  {testItem.mixAnswers ? (
                    <MixedIcon src={mixed.src} alt="Ответы перемешиваются" />
                  ) : (
                    <MixedIcon
                      src={nomixed.src}
                      alt="Ответы не перемешиваются"
                    />
                  )}
                </Head>
                <QuestionTitle
                  dangerouslySetInnerHTML={{
                    __html: testItem.questionText,
                  }}
                />
                {testItem.answers &&
                  testItem.answers.map(
                    (answerItem: TAnswer, index1: number) => {
                      return (
                        <Answer key={String(index + "_" + index1)}>
                          <Checkbox checked={answerItem.trueAnswer} />
                          <AnswerText
                            dangerouslySetInnerHTML={{
                              __html: answerItem.text,
                            }}
                          />
                        </Answer>
                      );
                    }
                  )}
              </EditQuestionLink>
            </div>
          );
        })}
      <Button onClick={onSave}>Сохранить</Button>
    </>
  );
};

export default Presenter;
