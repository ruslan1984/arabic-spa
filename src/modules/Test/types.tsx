import { Status } from "@components/types";

export type TAnswer = {
  text: string;
  trueAnswer: boolean;
  removed: boolean;
  errorText?: string;
  errorBlockShow?: boolean;
  answerNumber?: number;
};

export type TTest = {
  id?: number;
  questionText: string;
  answers: TAnswer[];
  status: Status;
  deletedAt?: string;
  lessonId?: number;
  mixAnswers?: boolean;
};

export type ChecboxType = {
  testId: number;
  answerId: number;
};
