import React, { FC, FormEvent } from "react";
import Editor from "@components/Editor";
import Answer from "./Answer";
import {
  AnswersBlock,
  QuestionTitle,
  QuestionFooter,
  ErrorText,
  SettingsBlock,
  Label,
} from "./styles";
import { TAnswer } from "@modules/Test/types";
import {
  Button,
  RemoveButton,
  RecoverButton,
} from "@components/Elements/elements";
import { Checkbox } from "@components/Elements/elements";
import { Loading } from "@components/types";

interface ITest {
  questionText: string;
  testId: number;
  errorText: string[];
  answers: TAnswer[];
  saving: Loading;
  deletedAt: string;
  mixAnswers?: boolean;
  setQuestionText: (text: string) => void;
  questionSave: (e: FormEvent) => void;
  addAnswer: () => void;
  del: () => void;
  recover: () => void;
  checkMixAnswers: () => void;
}

export const Presenter: FC<ITest> = ({
  questionText,
  testId,
  errorText,
  answers,
  mixAnswers,
  saving,
  deletedAt,
  setQuestionText,
  questionSave,
  addAnswer,
  del,
  recover,
  checkMixAnswers,
}: ITest) => {
  return (
    <>
      <form onSubmit={questionSave}>
        <QuestionTitle>Вопрос</QuestionTitle>
        <SettingsBlock>
          <Label htmlFor="mixAnswers">
            <Checkbox
              id="mixAnswers"
              checked={mixAnswers}
              onChange={checkMixAnswers}
            />
            Перемешивать варианты ответов
          </Label>
        </SettingsBlock>
        <Editor
          name={`question${testId}`}
          value={questionText}
          onChange={setQuestionText}
        />
        <AnswersBlock>
          <QuestionTitle>Варианты ответов</QuestionTitle>
          {answers &&
            answers.length > 0 &&
            answers.map((item: TAnswer, index: number) => {
              return <Answer {...item} key={index} answerNumber={index} />;
            })}
        </AnswersBlock>
        <Button type="submit" saving={saving}>
          Сохранить
        </Button>
        <ErrorText>
          {errorText.length > 0 &&
            errorText.map((text, index) => (
              <div key={index}>
                {index + 1}) {text}
              </div>
            ))}
        </ErrorText>
      </form>
      <QuestionFooter>
        <Button onClick={addAnswer}>Добавить вариант ответа</Button>
        {deletedAt ? (
          <RecoverButton onClick={recover}>Восстановить</RecoverButton>
        ) : (
          <RemoveButton onClick={del}>Удалить</RemoveButton>
        )}
      </QuestionFooter>
    </>
  );
};
export default Presenter;
