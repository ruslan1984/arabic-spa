import React, { FC } from "react";
import {
  AnswerBlock,
  EditorBlock,
  ErrorTextBlock,
  RecoverBlock,
  RecoverText,
  AnswerWindow,
} from "./elements";
import Editor from "@components/Editor";
import AnswerHeader from "./AnswerHeader";
import { removeTags } from "@/helpers/functions";

interface AnswerProps {
  answerNumber: number;
  text: string;
  errorText: string;
  errorBlockShow: boolean;
  trueAnswer: boolean;
  removed: boolean;
  setAnswerText: (text: string) => void;
  setAnswerErrorText: (text: string) => void;
}

export const Presenter: FC<AnswerProps> = ({
  answerNumber,
  text,
  errorText,
  errorBlockShow,
  trueAnswer,
  removed,
  setAnswerText,
  setAnswerErrorText,
}: AnswerProps) => {
  if (removed) {
    return (
      <>
        <RecoverBlock>
          <AnswerHeader
            removed={removed}
            trueAnswer={trueAnswer}
            answerNumber={answerNumber}
          />
          <RecoverText dangerouslySetInnerHTML={{ __html: text }}></RecoverText>
        </RecoverBlock>
      </>
    );
  } else {
    return (
      <AnswerWindow>
        <AnswerHeader
          removed={removed}
          trueAnswer={trueAnswer}
          answerNumber={answerNumber}
        />
        <AnswerBlock>
          <EditorBlock>
            <Editor
              name={`answer_${answerNumber}`}
              value={text ?? ""}
              onChange={(text) => {
                setAnswerText(text);
              }}
            />

            <ErrorTextBlock
              visible={errorBlockShow || removeTags(errorText) != ""}
            >
              <div>Текст при ошибке</div>
              <Editor
                name={`errorText_${answerNumber}`}
                value={errorText ?? ""}
                onChange={(text) => {
                  setAnswerErrorText(text);
                }}
              />
            </ErrorTextBlock>
          </EditorBlock>
        </AnswerBlock>
      </AnswerWindow>
    );
  }
};
export default Presenter;
