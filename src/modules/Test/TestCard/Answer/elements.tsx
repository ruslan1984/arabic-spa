import styled from "@emotion/styled";

export const AnswerBlock = styled.div`
  display: flex;
`;
export const AnswerWindow = styled.div`
  margin-bottom: 30px;
`;

export const EditorBlock = styled.div`
  flex: 1;
`;
export const ErrorTextBlock = styled.div`
  margin-top: 20px;
  ${(props: { visible?: boolean }) => {
    return props.visible ? "display: block" : "display: none";
  }};
`;

export const RecoverBlock = styled.div`
  display: block;
  margin-bottom: 30px;
`;
export const RecoverText = styled.p`
  color: red;
  margin: 10px;
`;
export const RecoverBlockFooter = styled.div`
  text-align: right;
`;
