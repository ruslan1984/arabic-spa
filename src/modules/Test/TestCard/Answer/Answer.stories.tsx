import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import { action, actions } from "@storybook/addon-actions";

import Presenter from "./Presenter";

export default {
  title: "Test",
  decorators: [withKnobs],
};
const props = {
  questionId: 1,
  answerNumber: 1,
  text: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis quam aliquam vel voluptates quidem. Laboriosam unde distinctio ad deleniti? Vero, odit dolores tempore commodi error iste? Explicabo vitae eaque repudiandae minima ad quos, in soluta ipsum laboriosam id maxime, asperiores reprehenderit voluptatem alias a, tempore quae minus labore omnis cum ipsam nemo delectus neque? Modi sequi minus provident, eum, esse dolores obcaecati consequatur itaque necessitatibus beatae mollitia laboriosam officia consequuntur aut, natus magni! Ducimus dolore magni eum quas maxime provident corrupti architecto! Ipsa nesciunt sint animi libero voluptates quisquam voluptatibus non odio veritatis natus doloremque exercitationem esse dicta, numquam quod?",
  result: true,
  removed: false,
  errorText:
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo, quam!",
  errorBlockShow: false,
  trueAnswer: true,
  setAnswerText: (text: string) => action(text),
  setAnswerErrorText: (text: string) => action(text),
  setUpdateData: (data: { answer: boolean }) => action("text"),
};

export const Answer = () => {
  return <Presenter {...props} />;
};
