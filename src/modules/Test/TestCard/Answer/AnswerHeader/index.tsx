import React, { FC } from "react";
import { actions } from "@modules/Test/TestCard/reducer";
import { useDispatch } from "react-redux";
import { Presenter } from "./Presenter";

interface AnswerHeaderProps {
  answerNumber: number;
  trueAnswer: boolean;
  removed: boolean;
}
export const AnswerHeader: FC<AnswerHeaderProps> = ({
  answerNumber,
  trueAnswer,
  removed,
}: AnswerHeaderProps) => {
  const dispatch = useDispatch();
  return (
    <Presenter
      answerNumber={answerNumber}
      trueAnswer={trueAnswer}
      removed={removed}
      setTrueAnswer={() => {
        dispatch(actions.setTrueAnswer(answerNumber));
      }}
      setRemoveAnswer={() => {
        dispatch(actions.setRemoveAnswer(answerNumber));
      }}
      setShowErrorText={() => {
        dispatch(actions.setShowErrorText(answerNumber));
      }}
      setRecoverAnswer={() => {
        dispatch(actions.setRecoverAnswer(answerNumber));
      }}
    />
  );
};

export default AnswerHeader;
