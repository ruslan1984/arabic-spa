import React, { FC } from "react";

import {
  AnswerMenu,
  AnswerTitle,
  AnswerCheckbox,
  ErrorTextBtn,
  AnswerHeaderBlock,
  RemoveBtn,
  RecoverBtn,
} from "./elements";

interface AnswerHeaderProps {
  answerNumber: number;
  trueAnswer: boolean;
  removed: boolean;
  setTrueAnswer: () => void;
  setRemoveAnswer: () => void;
  setShowErrorText: () => void;
  setRecoverAnswer: () => void;
}
export const Presenter: FC<AnswerHeaderProps> = (props: AnswerHeaderProps) => {
  return (
    <AnswerHeaderBlock>
      <AnswerTitle>Вариант ответа {props.answerNumber + 1}</AnswerTitle>
      <AnswerMenu>
        {(() => {
          if (props.removed) {
            return <RecoverBtn onClick={props.setRecoverAnswer} />;
          } else {
            return (
              <>
                <AnswerCheckbox
                  checked={props.trueAnswer ?? false}
                  onChange={props.setTrueAnswer}
                />
                <ErrorTextBtn onClick={props.setShowErrorText} />
                <RemoveBtn onClick={props.setRemoveAnswer} />
              </>
            );
          }
        })()}
      </AnswerMenu>
    </AnswerHeaderBlock>
  );
};
export default Presenter;
