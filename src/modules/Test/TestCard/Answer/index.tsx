import React, { FC } from "react";
import Presenter from "./Presenter";
import { useDispatch } from "react-redux";
import { actions } from "@modules/Test/TestCard/reducer";

interface IAnswer {
  answerNumber: number;
  text: string;
  trueAnswer: boolean;
  removed: boolean;
  errorText?: string;
  errorBlockShow?: boolean;
}

export const Answer: FC<IAnswer> = ({
  answerNumber,
  text,
  trueAnswer,
  removed,
  errorText,
  errorBlockShow,
}: IAnswer) => {
  const dispatch = useDispatch();

  const setText = (text: string) => {
    dispatch(
      actions.setAnswerText({
        answerNumber: answerNumber,
        text,
      })
    );
  };

  const setErrorText = (text: string) => {
    dispatch(
      actions.setAnswerErrorText({
        answerNumber: answerNumber,
        text,
      })
    );
  };

  return (
    <Presenter
      answerNumber={answerNumber}
      text={text}
      trueAnswer={trueAnswer}
      removed={removed}
      errorText={errorText || ""}
      errorBlockShow={errorBlockShow || false}
      setAnswerText={setText}
      setAnswerErrorText={setErrorText}
    />
  );
};

export default Answer;
