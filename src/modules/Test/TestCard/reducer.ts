import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import * as R from "ramda";
import { TAnswer } from "@modules/Test/types";
import { Loading } from "@components/types";
import { TestCardReducerType, TestCardType } from "./types";

const emptyData = {
  questionText: "",
  answers: [],
  deletedAt: "",
  mixAnswers: false,
};

export const defaultState: TestCardReducerType = {
  saving: Loading.no,
  loading: Loading.no,
  errorText: [],
  data: emptyData,
};
export const testSave = createAction<{ lessonId: number; testId: number }>(
  "testSave"
);
export const testCardGet = createAction<number>("testCardGet");
export const testCardNew = createAction<number>("testCardNew");

export const testCardSlice = createSlice({
  name: "testCard",
  initialState: defaultState,
  reducers: {
    setTestData: (state, { payload }: PayloadAction<any>) => {
      const data = { ...state.data, ...payload };
      return { ...state, data };
    },
    setTrueAnswer: (state, { payload }: PayloadAction<number>) => {
      const answersState: TAnswer[] = [...state.data.answers];
      const newAnswer: TAnswer = {
        ...answersState[payload],
        trueAnswer: !answersState[payload].trueAnswer,
      };
      const answers: TAnswer[] = R.update(payload, newAnswer, answersState);
      const data = { ...state.data, answers };
      return { ...state, data };
    },
    checkMixAnswers: (state) => {
      const data: TestCardType = {
        ...state.data,
        mixAnswers: !state.data.mixAnswers,
      };
      return { ...state, data };
    },
    setRemoveAnswer: (state, { payload }: PayloadAction<number>) => {
      const answersState: TAnswer[] = [...state.data.answers];
      const newAnswer: TAnswer = {
        ...answersState[payload],
        removed: true,
      };
      const answers: TAnswer[] = R.update(payload, newAnswer, answersState);
      const data: TestCardType = { ...state.data, answers };
      return { ...state, data };
    },
    setRecoverAnswer: (state, { payload }: PayloadAction<number>) => {
      const answersState: TAnswer[] = [...state.data.answers];
      const newAnswer: TAnswer = {
        ...answersState[payload],
        removed: false,
      };
      const answers: TAnswer[] = R.update(payload, newAnswer, answersState);
      const data: TestCardType = { ...state.data, answers };
      return { ...state, data };
    },
    setShowErrorText: (state, { payload }: PayloadAction<number>) => {
      const answersState: TAnswer[] = [...state.data.answers];
      const newAnswer: TAnswer = {
        ...answersState[payload],
        errorBlockShow: !answersState[payload].errorBlockShow,
      };
      const answers: TAnswer[] = R.update(payload, newAnswer, answersState);
      const data: TestCardType = { ...state.data, answers };
      return { ...state, data };
    },

    testDelete: (state, { payload }: PayloadAction<number>) => {
      const data: TestCardType = { ...state.data, deletedAt: "1" };
      return { ...state, data };
    },
    testRecover: (state, { payload }: PayloadAction<number>) => {
      const data: TestCardType = { ...state.data, deletedAt: "" };
      return { ...state, data };
    },
    setQuestionText: (state, { payload }: PayloadAction<string>) => {
      const data = { ...state.data, questionText: payload };
      return { ...state, data };
    },
    setAnswerText: (state, { payload }: PayloadAction<any>) => {
      const answersState: TAnswer[] = [...state.data.answers];
      const newAnswer: TAnswer = {
        ...answersState[payload.answerNumber],
        text: payload.text,
      };
      const answers: TAnswer[] = R.update(
        payload.answerNumber,
        newAnswer,
        answersState
      );
      const data = { ...state.data, answers };
      return { ...state, data };
    },
    setAnswerErrorText: (state, { payload }: PayloadAction<any>) => {
      const answersState: TAnswer[] = [...state.data.answers];
      const newAnswer: TAnswer = {
        ...answersState[payload.answerNumber],
        errorText: payload.text,
      };
      const answers: TAnswer[] = R.update(
        payload.answerNumber,
        newAnswer,
        answersState
      );
      const data = { ...state.data, answers };
      return { ...state, data };
    },

    addAnswer: (state, { payload }: PayloadAction<number>) => {
      const newAnswer: TAnswer = {
        text: "",
        trueAnswer: false,
        removed: false,
        errorText: "",
        errorBlockShow: false,
      };
      const answers: TAnswer[] = (state?.data?.answers && [
        ...state.data.answers,
        newAnswer,
      ]) || [newAnswer];
      const data = { ...state.data, answers };
      return { ...state, data };
    },

    addError: (state, { payload }: PayloadAction<string>) => {
      const errorText = [...state.errorText];
      errorText.push(payload);
      return { ...state, errorText };
    },
    clearErrors: (state) => {
      return { ...state, errorText: [] };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    setSaving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, saving: payload };
    },
    clear: (state) => {
      return { ...state, data: emptyData };
    },
  },
});
export const { actions, reducer } = testCardSlice;
