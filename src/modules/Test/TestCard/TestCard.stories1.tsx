import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import { actions } from "@storybook/addon-actions";

import Presenter from "./Presenter";

export default {
  title: "Test",
  decorators: [withKnobs],
};
const data = {
  questionId: 1,
  question:
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores numquam molestias est provident exercitationem necessitatibus, itaque ducimus modi, saepe quos nulla aut obcaecati laudantium vero vitae quo iste velit ipsum. Iure quod praesentium eligendi, itaque facere commodi officia deserunt cumque?",
  answers: [
    {
      id: 1,
      text: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis quam aliquam vel voluptates quidem. Laboriosam unde distinctio ad deleniti? Vero, odit dolores tempore commodi error iste? Explicabo vitae eaque repudiandae minima ad quos, in soluta ipsum laboriosam id maxime, asperiores reprehenderit voluptatem alias a, tempore quae minus labore omnis cum ipsam nemo delectus neque? Modi sequi minus provident, eum, esse dolores obcaecati consequatur itaque necessitatibus beatae mollitia laboriosam officia consequuntur aut, natus magni! Ducimus dolore magni eum quas maxime provident corrupti architecto! Ipsa nesciunt sint animi libero voluptates quisquam voluptatibus non odio veritatis natus doloremque exercitationem esse dicta, numquam quod?",
      trueAnswer: true,
      removed: false,
      errorText: "",
      errorBlockShow: false,
    },
    {
      id: 2,
      text: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis quam aliquam vel voluptates quidem. Laboriosam unde distinctio ad deleniti? Vero, odit dolores tempore commodi error iste? Explicabo vitae eaque repudiandae minima ad quos, in soluta ipsum laboriosam id maxime, asperiores reprehenderit voluptatem alias a, tempore quae minus labore omnis cum ipsam nemo delectus neque? Modi sequi minus provident, eum, esse dolores obcaecati consequatur itaque necessitatibus beatae mollitia laboriosam officia consequuntur aut, natus magni! Ducimus dolore magni eum quas maxime provident corrupti architecto! Ipsa nesciunt sint animi libero voluptates quisquam voluptatibus non odio veritatis natus doloremque exercitationem esse dicta, numquam quod?",
      trueAnswer: true,
      removed: true,
      errorText:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo, quam!",
      errorBlockShow: false,
    },
    {
      id: 3,
      text: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis quam aliquam vel voluptates quidem. Laboriosam unde distinctio ad deleniti? Vero, odit dolores tempore commodi error iste? Explicabo vitae eaque repudiandae minima ad quos, in soluta ipsum laboriosam id maxime, asperiores reprehenderit voluptatem alias a, tempore quae minus labore omnis cum ipsam nemo delectus neque? Modi sequi minus provident, eum, esse dolores obcaecati consequatur itaque necessitatibus beatae mollitia laboriosam officia consequuntur aut, natus magni! Ducimus dolore magni eum quas maxime provident corrupti architecto! Ipsa nesciunt sint animi libero voluptates quisquam voluptatibus non odio veritatis natus doloremque exercitationem esse dicta, numquam quod?",
      trueAnswer: false,
      removed: false,
      errorText:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo, quam!",
      errorBlockShow: false,
    },
  ],
};
export const Question = () => {
  return <>Поправить</>; //<Presenter {...data} />;
};
