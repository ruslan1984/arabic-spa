import { Loading } from "@components/types";
import { TAnswer } from "@modules/Test/types";

export type TestCardType = {
  questionText: string;
  answers: TAnswer[];
  deletedAt: string;
  lessonId?: number;
  mixAnswers: boolean;
};

export type TestCardReducerType = {
  saving: Loading;
  loading: Loading;
  errorText: string[];
  data: TestCardType;
};
