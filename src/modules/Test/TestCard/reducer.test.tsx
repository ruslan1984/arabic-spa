import { actions, reducer, defaultState } from "./reducer";
import { Loading, Status } from "@components/types";
import { random, name, lorem } from "faker";
import { TAnswer } from "@modules/Test/types";

describe("Grammar test question", () => {
  const state = {
    updatedData: {
      questionText: false,
      answers: false,
      lessonId: false,
      status: false,
    },
    saving: Loading.no,
    loading: Loading.no,
    errorText: "",
    data: {
      testId: 1,
      questionId: 1,
      lessonId: 1,
      questionText: lorem.text(),
      status: Status.draft,
      answers: [
        {
          answerNumber: 1,
          text: lorem.text,
          errorText: lorem.text(),
          trueAnswer: true,
        },
        {
          answerNumber: 2,
          text: "string",
          errorText: "string1",
          trueAnswer: false,
        },
      ],
    },
  };

  it("setQuestion", () => {
    const payload = { lessonId: 1, testId: 1 };
    const data = {
      ...state.data,
      lessonId: Number(payload.lessonId),
      testId: Number(payload.testId),
    };
    expect(reducer(state, actions.setQuestion(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setQuestionData", () => {
    const payload = { lessonId: 1, testId: 1 };
    const data = {
      ...state.data,
      lessonId: Number(payload.lessonId),
      testId: Number(payload.testId),
    };
    expect(reducer(state, actions.setQuestionData(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setTestId", () => {
    const payload = 1;
    const data = { ...state.data, testId: payload };
    expect(reducer(state, actions.setTestId(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setTrueAnswer", () => {
    const payload = 1;
    const answers = state.data.answers.map((answerItem: TAnswer) => {
      if (answerItem.answerNumber === payload) {
        return { ...answerItem, trueAnswer: !answerItem.trueAnswer };
      }
      return answerItem;
    });
    const data = { ...state.data, answers };
    expect(reducer(state, actions.setTrueAnswer(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setRemoveAnswer", () => {
    const payload = 1;
    const answers = state.data.answers.map((answerItem: TAnswer) => {
      if (answerItem.answerNumber === payload) {
        return { ...answerItem, removed: true };
      }
      return answerItem;
    });
    const data = { ...state.data, answers };
    const updatedData = { ...state.updatedData, answers: true };
    expect(reducer(state, actions.setRemoveAnswer(payload))).toEqual({
      ...state,
      data,
      updatedData,
    });
  });

  it("setRecoverAnswer", () => {
    const payload = 1;
    const answers = state.data.answers.map((answerItem: TAnswer) => {
      if (answerItem.answerNumber === payload) {
        return { ...answerItem, removed: false };
      }
      return answerItem;
    });
    const data = { ...state.data, answers };
    expect(reducer(state, actions.setRecoverAnswer(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setShowErrorText", () => {
    const payload = 1;
    const answers = state.data.answers.map((answerItem: TAnswer) => {
      if (answerItem.answerNumber === payload) {
        return { ...answerItem, errorBlockShow: !answerItem.errorBlockShow };
      }
      return answerItem;
    });
    const data = { ...state.data, answers };
    expect(reducer(state, actions.setShowErrorText(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setQuestionText", () => {
    const payload = lorem.text();
    const data = { ...state.data, questionText: payload };
    expect(reducer(state, actions.setQuestionText(payload))).toEqual({
      ...state,
      data,
    });
  });
  it("setAnswerErrorText", () => {
    const payload = {
      answerNumber: 1,
      text: lorem.text(),
    };
    const answers = state.data.answers.map((answerItem: TAnswer) => {
      if (answerItem.answerNumber === payload.answerNumber) {
        return { ...answerItem, errorText: payload.text };
      }
      return answerItem;
    });
    const data = { ...state.data, answers };
    expect(reducer(state, actions.setAnswerErrorText(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setAnswerText", () => {
    const payload = {
      answerNumber: 1,
      text: lorem.text(),
    };
    const answers = state.data.answers.map((answerItem: TAnswer) => {
      if (answerItem.answerNumber === payload.answerNumber) {
        return { ...answerItem, text: payload.text };
      }
      return answerItem;
    });
    const data = { ...state.data, answers };
    expect(reducer(state, actions.setAnswerText(payload))).toEqual({
      ...state,
      data,
    });
  });

  it("setUpdateData", () => {
    const payload = { questionText: true };
    const updatedData = { ...state.updatedData, ...payload };
    expect(reducer(state, actions.setUpdateData(payload))).toEqual({
      ...state,
      updatedData,
    });
  });

  it("addAnswer", () => {
    const answers = [...state.data.answers];
    const answersSort = answers.sort((x: TAnswer, y: TAnswer) => {
      return x.answerNumber < y.answerNumber;
    });
    const reverse = answersSort.reverse();
    const maxAnswerNumber = reverse[0].answerNumber ?? 0;
    const data = {
      ...state.data,
      answers: [
        ...state.data.answers,
        {
          answerNumber: maxAnswerNumber + 1,
          text: "",
          trueAnswer: false,
          removed: false,
          errorText: "",
          errorBlockShow: false,
        },
      ],
    };
    expect(reducer(state, actions.addAnswer())).toEqual({ ...state, data });
  });

  it("setErrorText", () => {
    const payload = lorem.text();
    expect(reducer(state, actions.setErrorText(payload))).toEqual({
      ...state,
      errorText: payload,
    });
  });

  it("setLoading", () => {
    const payload = Loading.start;
    expect(reducer(state, actions.setLoading(payload))).toEqual({
      ...state,
      loading: payload,
    });
  });
  it("setSaving", () => {
    const payload = Loading.start;
    expect(reducer(state, actions.setSaving(payload))).toEqual({
      ...state,
      saving: payload,
    });
  });
  it("setPublished", () => {
    const data = { ...state.data, status: Status.published };
    expect(reducer(state, actions.setPublished())).toEqual({ ...state, data });
  });

  it("setDraft", () => {
    const data = { ...state.data, status: Status.draft };
    expect(reducer(state, actions.setDraft())).toEqual({ ...state, data });
  });
});
