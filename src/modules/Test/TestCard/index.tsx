import React, { FC, FormEvent, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions, testSave, testCardGet, testCardNew } from "./reducer";
import { reducerType } from "@store/reducers";
import { Loading } from "@components/types";
import Presenter from "./Presenter";
import { Page } from "@components/Elements/elements";

interface TestCardProps {
  lessonId: number;
  testId: number;
}

export const TestCard: FC<TestCardProps> = ({
  testId,
  lessonId,
}: TestCardProps) => {
  const dispatch = useDispatch();
  const { saving, errorText, loading } = useSelector(
    (state: reducerType) => state.testCard
  );

  const { questionText, answers, deletedAt, mixAnswers } = useSelector(
    (state: reducerType) => state.testCard.data
  );

  useEffect(
    () => () => {
      dispatch(actions.clear());
    },
    []
  );

  useEffect(() => {
    testId == 0
      ? dispatch(testCardNew(lessonId))
      : dispatch(testCardGet(testId));
  }, []);

  const save = (e: FormEvent) => {
    e.preventDefault();
    dispatch(testSave({ lessonId, testId }));
  };
  const del = () => {
    dispatch(actions.testDelete(testId));
  };
  const recover = () => {
    dispatch(actions.testRecover(testId));
  };
  const addAnswer = () => {
    dispatch(actions.addAnswer(testId));
  };

  const setQuestionText = (text: string) => {
    dispatch(actions.setQuestionText(text));
  };
  const checkMixAnswers = () => {
    dispatch(actions.checkMixAnswers());
  };

  return loading == Loading.ok ? (
    <Presenter
      testId={testId}
      questionText={questionText}
      mixAnswers={mixAnswers}
      answers={answers}
      saving={saving}
      errorText={errorText}
      deletedAt={deletedAt}
      questionSave={save}
      addAnswer={addAnswer}
      del={del}
      recover={recover}
      setQuestionText={setQuestionText}
      checkMixAnswers={checkMixAnswers}
    />
  ) : (
    <Page loading={loading}> Загрузка...</Page>
  );
};

export default TestCard;
