import styled from "@emotion/styled";

export const AnswersBlock = styled.div`
  padding-left: 40px;
`;
export const QuestionTitle = styled.div`
  margin: 30px 0;
  text-align: center;
  font-size: 40px;
`;
export const QuestionFooter = styled.div``;

export const ErrorText = styled.div`
  color: red;
`;

export const SettingsBlock = styled.div`
  margin: 10px 0;
`;
export const Label = styled.label`
  margin: 10px 0;
  cursor: pointer;
`;
