import { takeEvery, put, call, select, delay } from "redux-saga/effects";
import { actions, testSave, testCardGet, testCardNew } from "./reducer";
import { Loading } from "@components/types";
import { TestCardType } from "./types";
import { fetchGet, fetchPut, fetchDelete, fetchPost } from "@helpers/fetch";
import { reducerType } from "@store/reducers";
import { TAnswer } from "../types";
//import history from "@routes/history";
import routes from "@routes/routes";

export function* newTest() {
  yield put(actions.setLoading(Loading.start));
  const answer = {
    text: "",
    trueAnswer: false,
  };
  const test = {
    questionText: "",
    deletedAt: "",
    answers: [answer, answer],
  };
  yield delay(10);
  yield put(actions.setTestData(test));
  yield put(actions.setLoading(Loading.ok));
}

export function* testCardGetSaga(data: { payload: number }) {
  try {
    yield put(actions.setLoading(Loading.start));
    const fetchData: TestCardType = yield call(
      async () => await fetchGet(`/api/test/${data.payload}`)
    );
    yield put(actions.setTestData(fetchData));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* saveTestSaga(testId: number, cardData: TestCardType) {
  try {
    yield put(actions.setSaving(Loading.start));
    yield fetchPut(`/api/test/${testId}`, cardData);
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

export function* createTest(lessonId: number, cardData: TestCardType) {
  try {
    yield put(actions.setSaving(Loading.start));
    const fetchData: { id: number } = yield call(
      async () => await fetchPost(`/api/test`, { ...cardData, lessonId })
    );
    //yield history.push(routes.grammar.TestCard(lessonId, fetchData.id));
    //yield put(testCardGet(fetchData.id));

    location.href = routes.grammar.TestCard(lessonId, fetchData.id);
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

export function* saveTestClickSaga(data: {
  payload: { lessonId: number; testId: number };
}) {
  const { lessonId, testId } = data.payload;
  const state: reducerType = yield select();
  const { data: cardData } = state.testCard;

  const answers = cardData.answers.filter(
    (item: TAnswer) =>
      !item.removed && item.text.replace(/<\/?[^>]+>/g, "").trim() != ""
  );

  const newData = { ...cardData, answers };
  let err = false;
  yield put(actions.clearErrors());
  if (newData.questionText.replace(/<\/?[^>]+>/g, "").trim() == "") {
    yield put(actions.addError("Не указан вопрос"));
    err = true;
  }
  if (answers.length === 0) {
    yield put(actions.addError("Не указан вариант ответа"));
    err = true;
  }
  if (err) return;
  if (testId == 0) {
    yield createTest(lessonId, newData);
  } else {
    yield saveTestSaga(testId, newData);
  }
}

export function* setTestDelete(data: { payload: number }) {
  try {
    yield fetchDelete(`/api/test/${data.payload}`);
  } catch (err) {
    console.error(err);
  }
}
export function* setTestRecover(data: { payload: number }) {
  try {
    yield fetchPut(`/api/test/recover/${data.payload}`);
  } catch (err) {
    console.error(err);
  }
}

export function* testCardSaga() {
  yield takeEvery(testCardGet, testCardGetSaga);
  yield takeEvery(testSave, saveTestClickSaga);
  yield takeEvery(testCardNew, newTest);
  yield takeEvery(actions.testDelete, setTestDelete);
  yield takeEvery(actions.testRecover, setTestRecover);
}
