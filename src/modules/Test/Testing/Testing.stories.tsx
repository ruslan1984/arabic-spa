import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import Presenter from "./index";

export default {
  title: "Test",
  decorators: [withKnobs],
};

const data = [
  {
    questionText: "<p>1+1</p>",
    answers: [
      {
        text: "<p>2</p>",
        trueAnswer: true,
        errorText: "11",
      },
      {
        text: "<p>3</p>",
        trueAnswer: false,
        errorText: "err",
      },
    ],
    status: 1,
    sort: 1,
    deletedAt: null,
  },
  {
    questionText: "<p>11+11</p>",
    answers: [
      {
        text: "<p>22</p>",
        trueAnswer: true,
        errorText: "21",
      },
      {
        text: "<p>33</p>",
        trueAnswer: false,
        removed: false,
        errorText: "22",
      },
    ],
  },
  {
    questionText: "<p>1+2</p>",
    answers: [
      {
        text: "<p>1</p>",
        trueAnswer: false,
        errorText: "31",
      },
      {
        text: "<p>2</p>",
        trueAnswer: false,
      },
      {
        text: "<p>3</p>",
        trueAnswer: true,
        errorText: "33",
      },
    ],
  },
  {
    questionText: "<p>>2</p>",
    answers: [
      {
        text: "<p>1</p>",
        trueAnswer: false,
      },
      {
        text: "<p>3</p>",
        trueAnswer: true,
        errorText: "42",
      },
      {
        text: "<p>4</p>",
        trueAnswer: true,
        errorText: "43",
      },
    ],
  },
];

export const Testing = () => {
  return <Presenter data={data} />;
};
