import React, { ChangeEvent, memo, FC, useEffect, useState } from "react";
import { TAnswer, QuestionType, QuestionStatus, TestStatus } from "./types";

import Question from "./Question";
import Result from "./Result";
import { TestBlock } from "./styles";
import { shuffle } from "@helpers/functions";

interface TestProps {
  data: QuestionType[];
  isRadio?: boolean; // Только один вариант ответа
  isOpen?: boolean;
  hideHeader?: boolean;
  QuestionTitleCustom?: React.FunctionComponent<{
    children: string | JSX.Element;
  }>; // Задаем свой стиль для вопроса
  AnswerTextCustom?: React.FunctionComponent<{
    children: string | JSX.Element;
  }>; // Задаем свой стиль для вариантов ответов
}

export const Testing: FC<TestProps> = ({
  data,
  isRadio,
  QuestionTitleCustom,
  AnswerTextCustom,
  hideHeader,
  isOpen,
}: TestProps) => {
  const [currentQuestionId, setCurrentQuestionId] = useState<number>(0);

  const [questionText, setQuestionText] = useState<string>("");
  const [answers, setAnswers] = useState<TAnswer[]>([]);
  const [checkedAnswers, setCheckedAnswers] = useState<boolean[]>([]);
  const [questionType, setQuestionType] = useState<QuestionStatus>(
    QuestionStatus.ShowQuestion
  );
  const [testType, setTestType] = useState<TestStatus>(TestStatus.ShowTest);
  const [errorList, setErrorList] = useState<string[]>([]);
  const [errorQuestion, setErrorQuestion] = useState<boolean>(false);
  const [questionCount, setQuestionCount] = useState<number>(0);
  const [correctAnswers, setCorrectAnswers] = useState<TAnswer[]>([]);
  const [trueAnswerCount, setTrueAnswerCount] = useState<number>(0);
  const [location, setLocation] = useState<string>("");
  if (typeof document !== "undefined") {
    history.state.as !== location && setLocation(history.state.as);
  }
  useEffect(() => {
    setCurrentQuestionId(0);
    setQuestionText("");
    setAnswers([]);
    setCheckedAnswers([]);
    setQuestionType(QuestionStatus.ShowQuestion);
    setTestType(TestStatus.ShowTest);
    setErrorList([]);
    setErrorQuestion(false);
    setQuestionCount(0);
    setTrueAnswerCount(0);
    setQuestionCount(data.length || 0);
    setCorrectAnswers([]);
  }, [location, isOpen]);

  //  Формируем спиок вопросов
  useEffect(() => {
    if (!data || data.length <= 1) return;
    setQuestionText(data[currentQuestionId]?.questionText);
    let answers: TAnswer[] = data[currentQuestionId].answers;
    if (data[currentQuestionId].mixAnswers) {
      answers = shuffle<TAnswer>(data[currentQuestionId].answers);
    }

    setCorrectAnswers(answers);
    setAnswers(answers);
  }, [currentQuestionId, location, data]);

  useEffect(() => {
    setQuestionCount(data.length || 0);
  }, [data]);

  const onCheck = (e: ChangeEvent) => {
    const target: HTMLInputElement = e.target as HTMLInputElement;
    const cheked: boolean = target.checked;
    const answerId: number | null = Number(target.id);
    let answer: boolean[] = [];
    if (!isRadio) {
      answer = [...checkedAnswers];
    }
    answer[answerId] = cheked;

    setCheckedAnswers(answer);
  };
  const nextQuestion = () => {
    setCheckedAnswers([]);
    setQuestionType(QuestionStatus.ShowQuestion);
    !errorQuestion && setTrueAnswerCount(trueAnswerCount + 1);
    setErrorQuestion(false);

    if (currentQuestionId === questionCount - 1) {
      setTestType(TestStatus.ShowTotalResult);
    } else {
      setCurrentQuestionId(currentQuestionId + 1);
    }
  };

  const verify = () => {
    correctAnswers &&
      correctAnswers.forEach((item: TAnswer, index: number) => {
        if (item.trueAnswer !== (checkedAnswers[index] || false)) {
          setErrorQuestion(true);
          if (item?.errorText && errorList.indexOf(item.errorText) === -1) {
            item.errorText && setErrorList([...errorList, item.errorText]);
          }
        }
      });
    setQuestionType(QuestionStatus.ShowResult);
  };

  const startTest = () => {
    setCurrentQuestionId(0);
    setErrorList([]);
    setTestType(TestStatus.ShowTest);
    setTrueAnswerCount(0);
  };

  return (
    <TestBlock>
      {testType === TestStatus.ShowTest ? (
        <Question
          isRadio={isRadio}
          currentQuestionId={currentQuestionId}
          questionCount={questionCount}
          questionText={questionText}
          answers={answers}
          questionType={questionType}
          checkedAnswers={checkedAnswers}
          errorQuestion={errorQuestion}
          QuestionTitleCustom={QuestionTitleCustom}
          AnswerTextCustom={AnswerTextCustom}
          hideHeader={hideHeader}
          onCheck={onCheck}
          verify={verify}
          nextQuestion={nextQuestion}
        />
      ) : (
        <Result
          trueAnswerCount={trueAnswerCount}
          questionCount={questionCount}
          errorList={errorList}
          startTest={startTest}
        />
      )}
    </TestBlock>
  );
};

export default memo(Testing, (prev, next) => prev.data === next.data);
