import styled from "@emotion/styled";
import { Button } from "@components/Elements/elements";
import { tabletXs } from "@site/media";

export const Answer = styled.div`
  display: flex;
  align-items: center;
  margin: 10px;
`;

export const AnswerText = styled.div`
  margin-left: 10px;
  span,
  p {
    text-indent: 0;
    font-size: 1.2rem;
  }
`;

export const QuestionTitle = styled.div`
  font-size: 1.3rem;
  span,
  p {
    font-size: 1.25rem;
  }
`;
export const QuestionTitleResult = styled.div`
  font-size: 20px;
  color: black;
  font-weight: 700;
  text-transform: uppercase;
`;

export const Label = styled.label`
  cursor: pointer;
`;

export const ResultImg = styled.img`
  width: 30px;
  height: 30px;
`;

export const ResultImgBlock = styled.div`
  display: flex;
  justify-content: center;
  height: 30px;
  margin: 10px;
`;

export const QuestionHeader = styled.div`
  font-size: 25px;
  text-align: center;
  margin-bottom: 10px;
`;

export const TestButton = styled(Button)`
  color: white;
  background: var(--bthBg);
  border-radius: 3px;
  padding: 15px 30px;
  font-size: 20px;
  font-weight: 100;
  text-transform: none;
  min-width: 230px;
  &:hover {
    background: var(--bthBg-hover);
  }
  &[disabled] {
    color: #888;
    background: #eee;
  }
  &[disabled]:hover {
    color: #888;
    background: #eee;
  }
`;

export const TestBlock = styled.div`
  border: 1px solid var(--bthBg);
  border-radius: 25px;
  padding: 20px;
  background: var(--main-bg);
  z-index: 100;
  @media (max-width: ${tabletXs()}) {
    padding: 20px 5px;
  }
`;

export const ButtonsBlock = styled.div`
  text-align: center;
`;

export const ResultBlock = styled.div`
  text-align: center;
`;

export const Scope = styled.div`
  font-size: 40px;
`;
export const Recomendation = styled.div`
  text-align: left;
  margin: 20px 0 30px;
`;
