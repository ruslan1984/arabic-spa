import React, { FC, ChangeEvent } from "react";
import { Checkbox, Radio } from "@components/Elements/elements";
import {
  Answer,
  AnswerText,
  QuestionTitle,
  Label,
  ResultImg,
  ResultImgBlock,
  QuestionHeader,
  TestButton,
  ButtonsBlock,
} from "./styles";
import { TAnswer, QuestionStatus } from "./types";

import ok from "@assets/icons/tick.svg";
import error from "@assets/icons/close.svg";

interface QuestionProps {
  currentQuestionId: number;
  questionCount: number;
  questionText: string;
  answers: TAnswer[];
  questionType: QuestionStatus;
  checkedAnswers: boolean[];
  errorQuestion: boolean;
  isRadio?: boolean;
  hideHeader?: boolean;
  QuestionTitleCustom?: React.FunctionComponent<{
    children: string | JSX.Element;
  }>;
  AnswerTextCustom?: React.FunctionComponent<{
    children: string | JSX.Element;
  }>;
  onCheck: (e: ChangeEvent) => void;
  verify: () => void;
  nextQuestion: () => void;
}

const Question: FC<QuestionProps> = ({
  currentQuestionId,
  questionCount,
  questionText,
  answers,
  questionType,
  checkedAnswers,
  errorQuestion,
  isRadio,
  QuestionTitleCustom,
  AnswerTextCustom,
  hideHeader,
  onCheck,
  verify,
  nextQuestion,
}: QuestionProps) => (
  <>
    {!hideHeader && (
      <QuestionHeader>
        Вопрос {currentQuestionId + 1} / {questionCount}
      </QuestionHeader>
    )}
    {QuestionTitleCustom ? (
      <QuestionTitleCustom>{questionText}</QuestionTitleCustom>
    ) : (
      <QuestionTitle dangerouslySetInnerHTML={{ __html: questionText }} />
    )}
    {answers?.map((item: TAnswer, index: number) => {
      return (
        <Label key={index}>
          <Answer>
            {isRadio ? (
              <Radio
                id={String(index)}
                checked={checkedAnswers[index] || false}
                onChange={onCheck}
                disabled={questionType !== QuestionStatus.ShowQuestion}
              />
            ) : (
              <Checkbox
                id={String(index)}
                checked={checkedAnswers[index] || false}
                onChange={onCheck}
                disabled={questionType !== QuestionStatus.ShowQuestion}
              />
            )}
            {AnswerTextCustom ? (
              <AnswerTextCustom>{item.text}</AnswerTextCustom>
            ) : (
              <AnswerText dangerouslySetInnerHTML={{ __html: item.text }} />
            )}
          </Answer>
        </Label>
      );
    })}

    <ResultImgBlock>
      {questionType === QuestionStatus.ShowResult &&
        (errorQuestion ? (
          <div>
            <ResultImg src={error.src} alt="" />
          </div>
        ) : (
          <div>
            <ResultImg src={ok.src} alt="" />
          </div>
        ))}
    </ResultImgBlock>
    <ButtonsBlock>
      {questionType === QuestionStatus.ShowQuestion && (
        <TestButton onClick={verify}>Ответить</TestButton>
      )}
      {questionType === QuestionStatus.ShowResult && (
        <TestButton onClick={nextQuestion}>Следущий вопрос</TestButton>
      )}
    </ButtonsBlock>
  </>
);

export default Question;
