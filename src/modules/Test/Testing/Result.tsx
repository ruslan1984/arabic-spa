import React, { FC } from "react";
import { TestButton, ResultBlock, Scope, Recomendation } from "./styles";

interface ResultProps {
  trueAnswerCount: number;
  questionCount: number;
  errorList: string[];
  startTest: () => void;
}

const Result: FC<ResultProps> = ({
  trueAnswerCount,
  questionCount,
  errorList,
  startTest,
}: ResultProps) => (
  <ResultBlock>
    <Scope>
      Результат: {trueAnswerCount} из {questionCount}
    </Scope>
    <Recomendation>
      {errorList.map((item: string, index: number) => (
        <div key={index} dangerouslySetInnerHTML={{ __html: item }} />
      ))}
    </Recomendation>
    <TestButton onClick={startTest}>Повторить</TestButton>
  </ResultBlock>
);

export default Result;
