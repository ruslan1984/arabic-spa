export type TAnswer = {
  text: string;
  errorText?: string;
  trueAnswer: boolean;
};
export type QuestionType = {
  questionText: string;
  answers: TAnswer[];
  mixAnswers?: boolean;
};
export enum QuestionStatus {
  ShowQuestion,
  ShowResult,
}

export enum TestStatus {
  ShowTest,
  ShowTotalResult,
}
