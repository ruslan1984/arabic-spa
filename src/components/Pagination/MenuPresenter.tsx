import { FC, useState, useCallback, useEffect, memo } from "react";
import {
  PaginationItem,
  PaginationMenu,
  Pagination,
  PaginationBlock,
  PaginationMenuBlock,
  Dots,
  Close,
  ActiveDots,
} from "./styles";
import { closeIcon } from "@/components/Elements/icons";

interface IPagination {
  pagesCount: number;
  currentPage: number;
  pageClick: (page: number) => () => void;
}

const MenuPresenter: FC<IPagination> = ({
  pagesCount,
  currentPage,
  pageClick,
}: IPagination) => {
  const pages = Array.from(Array(pagesCount).keys());
  const [showMenu, setShowMenu] = useState(false);
  const onMenuClick = useCallback(() => {
    setShowMenu(!showMenu);
  }, [showMenu]);
  const onMenuCloseClick = useCallback(() => {
    setShowMenu(false);
  }, []);

  const hideMenu = (e: MouseEvent) => {
    if (!(e.target as HTMLElement).closest(".paginationMenu")) {
      setShowMenu(false);
    }
  };

  useEffect(() => {
    if (typeof document !== "undefined") {
      document.addEventListener("click", hideMenu);
    }
  }, []);

  useEffect(() => {
    return () => document.removeEventListener("click", hideMenu);
  }, []);

  return (
    <Pagination className="paginationMenu">
      <PaginationItem
        disabled={currentPage === 1}
        onClick={pageClick(currentPage - 1)}
      >
        &#8592;
      </PaginationItem>
      {[1, 2, 3].map((item) => (
        <PaginationItem
          key={item}
          onClick={pageClick(item)}
          active={currentPage === item}
        >
          {item}
        </PaginationItem>
      ))}
      <PaginationMenuBlock onClick={onMenuClick}>
        {[1, 2, 3, pagesCount - 2, pagesCount - 1, pagesCount].includes(
          currentPage
        ) ? (
          <ActiveDots>&bull; &bull; &bull; </ActiveDots>
        ) : (
          <ActiveDots>
            <Dots>&bull; &bull; &bull;</Dots>
            <PaginationItem active>{currentPage}</PaginationItem>
            <Dots>&bull; &bull; &bull;</Dots>
          </ActiveDots>
        )}
      </PaginationMenuBlock>
      {[pagesCount - 2, pagesCount - 1, pagesCount].map((item) => (
        <PaginationItem
          key={item}
          onClick={pageClick(item)}
          active={currentPage === item}
        >
          {item}
        </PaginationItem>
      ))}
      <PaginationItem
        disabled={currentPage === pages.length}
        onClick={pageClick(currentPage + 1)}
      >
        &#8594;
      </PaginationItem>
      <PaginationBlock active={showMenu}>
        <div onClick={onMenuCloseClick}>
          <Close src={closeIcon} />
        </div>
        <PaginationMenu>
          {pages.map((item) => (
            <PaginationItem
              key={item}
              onClick={pageClick(item + 1)}
              active={currentPage === item + 1}
            >
              {item + 1}
            </PaginationItem>
          ))}
        </PaginationMenu>
      </PaginationBlock>
    </Pagination>
  );
};

export default memo(MenuPresenter);
