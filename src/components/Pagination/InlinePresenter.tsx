import { FC } from "react";
import { Pagination, PaginationItem } from "./styles";

interface IPagination {
  pagesCount: number;
  currentPage: number;
  pageClick: (page: number) => () => void;
}

const InlinePresenter: FC<IPagination> = ({
  pagesCount,
  currentPage,
  pageClick,
}: IPagination) => {
  const pages = Array.from(Array(pagesCount).keys());
  return (
    <Pagination>
      <PaginationItem
        disabled={currentPage === 1}
        onClick={pageClick(currentPage - 1)}
      >
        &#8592;
      </PaginationItem>
      {pages.map((item) => (
        <PaginationItem
          key={item}
          active={currentPage === item + 1}
          onClick={pageClick(item + 1)}
        >
          {item + 1}
        </PaginationItem>
      ))}
      <PaginationItem
        disabled={currentPage === pages.length}
        onClick={pageClick(currentPage + 1)}
      >
        &#8594;
      </PaginationItem>
    </Pagination>
  );
};

export default InlinePresenter;
