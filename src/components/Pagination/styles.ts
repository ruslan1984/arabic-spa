import styled from "@emotion/styled";

export const Pagination = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 3px;
  padding: 10px 5px;
  border-radius: 5px;
  background: #eee;
  margin: 30px 0;
  position: relative;
`;

export const PaginationItem = styled.div`
  padding: 10px;
  cursor: pointer;
  text-align: center;
  border-radius: 20px;
  &:hover {
    outline: 1px solid var(--bthBg);
  }
  ${({ active, disabled }: { active?: boolean; disabled?: boolean }) =>
    (active && "outline: 1px solid var(--bthBg);") ||
    (disabled && "color: #888;pointer-events: none;")}
`;

export const Dots = styled.div`
  display: flex;
  align-items: center;
  margin-right: 15px;
  margin-left: 15px;
  cursor: pointer;
`;

export const PaginationMenuBlock = styled.div`
  display: flex;
  border-radius: 5px;
  transition: 0.3s;
  &:hover {
    outline: 1px solid var(--bthBg);
  }
}`;

export const PaginationMenu = styled.div`
  flex-wrap: wrap;
  display: flex;
  background: var(--main-bg);
  box-shadow: 2px 1px 8px #908d8d;
  border-radius: 10px;
  padding: 20px 10px;
  transition: 0.3s;
`;

export const PaginationBlock = styled.div`
  position: absolute;
  opacity: 0;
  max-width: 70vw;
  bottom: 40px;
  text-align: right;
  transition: 0.3s;
  pointer-events: none;
  ${({ active }: { active?: boolean }) =>
    active && "opacity: 1; pointer-events: all"}
`;
export const Close = styled.img`
  background: var(--main-bg);
  display: inline-block;
  padding: 7px;
  border-radius: 21px;
  cursor: pointer;
  margin-bottom: 10px;
  box-shadow: 2px 1px 8px #908d8d;
`;
export const ActiveDots = styled.div`
  background: var(--bthMenu-hover);
  padding: 0 10px;
  border-radius: 6px;
  cursor: pointer;
  display: flex;
  align-items: center;
`;
// export const CenterDots = styled.div`
//   background: var(--bthMenu-hover);
// `;
