import React from "react";
import { FaHeadphonesAlt, FaMosque } from "react-icons/fa";
import { IoIosMoon, IoIosSchool } from "react-icons/io";
import { BsQuestionCircle, BsEyeglasses, BsCheck2All } from "react-icons/bs";
import { ImQuill } from "react-icons/im";
import { GoBook } from "react-icons/go";

let Icons: any = [];
Icons["pen"] = <ImQuill />;
Icons["sound"] = <FaHeadphonesAlt />;
Icons["glasses"] = <BsEyeglasses />;
Icons["book"] = <GoBook />;
Icons["test"] = <BsCheck2All />;
Icons["question"] = <BsQuestionCircle />;
Icons["arabic"] = <IoIosMoon />;
Icons["word"] = <IoIosSchool />;
Icons["alphabet"] = <FaMosque />;

export default Icons;

export const closeIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAnElEQVRIie2VSw6DMAwFB+7QCC5Y7guoSPQw6caRgFapP7DjbcmbIUg2cMeRJ5AcvSTdagYgA5NRkoBZukPt4AMY5eAC9Er4tOl0/woWiRlukbjhGkkYXpOcBv8lWQV6GnwreQk4A28tvA1Im0B3l+MnKjfRzokaXoCeYVTDNc/C8LDEUjRLPG9l6pR1PWNf12W6q+saLv7h3PnKB/1VUtVNViCxAAAAAElFTkSuQmCC";
