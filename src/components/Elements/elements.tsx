import React from "react";
import { ChangeEvent } from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import { Loading } from "@components/types";
//import { Field } from "formik";
export const H1 = styled.h1`
  margin: 0;
`;

const InputStyle = `
  padding: 10px;
  font-size: 16px;
  border: 1px solid #ececec;
  flex: 1;
  width: 100%;
  box-sizing: border-box;
  border-radius: 5px;
  transition: 0.5s;
  &:focus {
    outline: none;
    border: 1px solid #bfffd3;
  }
`;
export const Input = styled.input`
  ${InputStyle}
`;
//export const FormikField = styled(Field)`
//  ${InputStyle}
//`;

const InputCheckbox = (props: {
  checked?: boolean;
  onChange?: (e: ChangeEvent) => void;
  question_id?: number;
  answerId?: number;
  disabled?: boolean;
  id?: string;
  name?: string;
}) => <input readOnly={true} type="checkbox" {...props} />;

export const Checkbox = styled(InputCheckbox)`
  margin: 10px;
  position: relative;
  cursor: pointer;

  &:before {
    content: "";
    position: absolute;
    width: 22px;
    height: 22px;
    background: white;
    border: 1px solid black;
    left: -5px;
    top: -5px;
  }
  &:checked&:after {
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 1px;
    left: 1px;
    background: #bd853a;
  }
`;

export const Radio = styled(Checkbox)`
  &:checked&:after {
    border-radius: 50%;
  }
  &:before {
    border-radius: 50%;
  }
`;

export const Select = styled.select`
  padding: 12px 20px;
  font-size: 16px;
  border: 1px solid grey;
  border-radius: 5px;
  flex: 1;
`;

export const Textarea = styled.textarea`
  padding: 12px 20px;
  font-size: 16px;
  border: 1px solid grey;
  border-radius: 5px;
  font-weight: 900;
`;

export const Button = styled.button`
  padding: 12px 20px;
  font-size: 15px;
  cursor: pointer;
  background: #87f187;
  color: #191818;
  border: none;
  text-transform: uppercase;
  margin: 5px;
  transition: 0.5s;
  border-radius: 5px;
  box-shadow: 1px 2px 5px 0px #8d8c8c;
  &:hover {
    background: #beffbe;
  }
  &:focus {
    outline: none;
  }
  @keyframes error {
    to {
      background: red;
      color: white;
    }
  }

  ${(props: { saving?: Loading }) => {
    if (props.saving === Loading.start) {
      return " background: #eeeeee !important; color: #dddddd !important;";
    }
  }};
`;

export const RemoveButton = styled(Button)`
  background: #f77c8d;
  &:hover {
    background: #ff0023;
  }
`;
export const RecoverButton = styled(Button)`
  background: #ffe895;
  &:hover {
    background: #ffc800;
  }
`;
export const AddLink = styled(Link)`
  padding: 12px 20px;
  font-size: 15px;
  cursor: pointer;
  background: #87f187;
  color: #191818;
  text-transform: uppercase;
  text-decoration: none;
  margin: 5px;
  transition: 0.5s;
  display: inline-block;
  &:hover {
    background: #1be61b;
  }
`;
export const Name = styled.span`
  min-width: 100px;
  min-width: 115px;
  display: inline-flex;
  font-size: 20px;
`;

export const Label = styled.label`
  display: flex;
  margin: 5px 0;
  min-width: 200px;
`;

export const Line = styled.div`
  display: flex;
  margin: 5px 0;
`;

export const Ul = styled.ul`
  list-style: none;
  padding-left: 20px;
`;

const loading = `
  &:after{
    content:'Loading';
    animation: uploading 1s infinite;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    padding: 10px;
    z-index: 1000;
  }
  @keyframes uploading {
    0% {
        background: #fff;
    }
    50% {
      background: #ddd;
    }
    100% {
      background: #fff;
    }
  }
`;
export const Page = styled.div`
  padding: 30px 40px;
  min-height: 200px;
  flex: 1;
  position: relative;
  padding-bottom: 30px;
  ${(props: { loading?: Loading }) => {
    if (props.loading === Loading.start) {
      return loading;
    }
  }}
`;

export const Menu = styled.div`
  width: 300px;
  min-width: 300px;
  background: #edfbed;
`;

export const MainPage = styled.div`
  display: flex;
`;
export const ListLink = styled(Link)`
  color: black;
  text-decoration: none;
  font-size: 20px;
  padding: 2px 5px;
  display: inline-flex;
  border-bottom: 1px solid #eee;
  justify-content: center;
  transition: 0.5s;
  border-radius: 5px;
  color: #333;
  &:nth-child(3n + 1) {
    justify-content: start;
  }
  &:hover {
    background: #edfbed;
    color: #000;
  }
  ${(props: { active?: boolean | number }) => {
    if (props.active) {
      return "background: #edfbed";
    }
  }}
`;

export const TopMenu = styled.div`
  display: flex;
  border-bottom: 1px solid #cecece;
  position: sticky;
  top: 0;
  z-index: 500;
  background: #fff;
  ul {
    margin: 0;
    padding: 0;
    display: flex;
    list-style: none;
  }
  li {
    cursor: pointer;
    &:hover {
      background: #edfbed;
    }
    a {
      padding: 10px;
      display: inline-block;
    }
  }
`;
export const ButtonsBlock = styled.div`
  position: fixed;
  bottom: 0;
  right: 0;
  display: flex;
  z-index: 100;
`;

export const Footer = styled.footer`
  display: flex;
  z-index: 1000;
  aligin-items: center;
  justify-content: end;
  position: fixed;
  background: white;
  bottom: 0;
  width: 100%;
  left: 0;
  box-shadow: 0px 1px 5px black;
`;
