import styled from "@emotion/styled";

export const ClearNbspButton = styled.button`
  background: #f7b4b4;
  border: none;
  margin-top: 5px;
  border-radius: 5px;
  text-decoration: line-through;
  cursor: pointer;
  transition: 0.5s;
  &:hover {
    background: #ef9595;
  }
`;
