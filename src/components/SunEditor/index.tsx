import React, { FC, memo, useCallback } from "react";
//import SunEditor from "suneditor-react";
import { ClearNbspButton } from "./elements";

interface EditorProps {
  name: string;
  text?: string;
  onChange?: (text: string, name: string) => void;
  onBlur?: (text: string, name: string) => void;
}

const Editor: FC<EditorProps> = ({
  text,
  name,
  onChange,
  onBlur,
}: EditorProps) => {
  const textChange = useCallback((newText: string) => {
    text !== newText && onChange && onChange(newText, name);
  }, []);
  const onEditorBlur = useCallback((event: any, newText: string) => {
    text !== newText && onBlur && onBlur(newText, name);
  }, []);

  const clearNbsp = useCallback(() => {
    if (text) {
      const newText = text.replace(/&nbsp;/g, " ");
      onChange && onChange(newText, name);
    }
  }, [text]);

  return (
    <>
      Sun Editor
      {/*<SunEditor
        autoFocus={false}
        name={name}
        setContents={text}
        onChange={textChange}
        onBlur={onEditorBlur}
        setOptions={{
          buttonList: [
            ["undo", "redo", "font", "fontSize", "formatBlock"],
            [
              "bold",
              "underline",
              "italic",
              "strike",
              "subscript",
              "superscript",
              "removeFormat",
            ],
            [
              "fontColor",
              "hiliteColor",
              "outdent",
              "indent",
              "align",
              "horizontalRule",
              "list",
              "table",
            ],
            [
              "link",
              "image",
              "video",
              "fullScreen",
              "showBlocks",
              "codeView",
              "preview",
            ],
          ],
        }}
      />*/}
      <ClearNbspButton onClick={clearNbsp}>nbsp;</ClearNbspButton>
    </>
  );
};

export default memo(Editor);
