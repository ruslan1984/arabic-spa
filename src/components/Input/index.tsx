import {
  FC,
  useState,
  useRef,
  RefObject,
  useEffect,
  InputHTMLAttributes,
  FocusEvent,
} from "react";
import { InputStyled, Label, InputBlock, ErrorText } from "./styles";

interface IInput extends InputHTMLAttributes<HTMLInputElement> {
  error?: string;
  inputRef?: RefObject<HTMLInputElement>;
  label: string;
}

const Input: FC<IInput> = ({
  label,
  name,
  type = "text",
  inputRef,
  error,
  onChange,
  onFocus,
  onBlur,
  ...props
}: IInput) => {
  const [focused, setFocused] = useState(false);
  const textInput = inputRef || useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (props?.value || props?.defaultValue) {
      setFocused(true);
    }
  }, []);

  const onFocusClick = (e: FocusEvent<HTMLInputElement, Element>) => {
    setFocused(true);
    onFocus && onFocus(e);
  };
  const onBlurClick = (e: FocusEvent<HTMLInputElement, Element>) => {
    !textInput.current?.value && setFocused(false);
    onBlur && onBlur(e);
  };
  return (
    <InputBlock>
      <Label active={focused}>{label}</Label>
      <InputStyled
        ref={textInput}
        onFocus={onFocusClick}
        onBlur={onBlurClick}
        name={name}
        type={type}
        error={Boolean(error)}
        onChange={onChange}
        // defaultValue={value}
        {...props}
      />
      <ErrorText active={Boolean(error)}>{error}</ErrorText>
    </InputBlock>
  );
};
export default Input;
