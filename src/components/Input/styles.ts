import styled from "@emotion/styled";

export const InputStyled = styled.input`
  padding: 12px 10px;
  border: 1px solid var(--bthBg-hover);
  width: 100%;
  transition: 0.2s;
  border-radius: 5px;
  box-sizing: border-box;
  margin-top: 15px;
  &:focus {
    outline: none;
    border: 1px solid var(--bthMenu-hover);
  }
  ${({ error }: { error?: boolean }) => error && "border-color: red"}};

`;

export const InputBlock = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 8px;
`;
export const Label = styled.div`
  position: absolute;
  top: 26px;
  left: 15px;
  background: var(--main-bg);
  transition: 0.2s;
  color: #777;
  pointer-events: none;
  font-size: 1rem;
  font-weight: 200;
  ${({ active }: { active?: boolean }) =>
    active && "top:7px; left:18px; font-size: 0.8rem;"}};
`;

export const ErrorText = styled.div`
  color: red;
  font-size: 0.8rem;
  height: 10px;
  visibility: hidden;
  ${({ active }: { active?: boolean }) => active && "visibility: visible;"}};
`;
