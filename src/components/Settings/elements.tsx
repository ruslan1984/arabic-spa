import styled from "@emotion/styled";
export const Name = styled.div`
  min-width: 150px;
`;
export const Label = styled.label`
  display: flex;
  align-items: center;
  margin: 5px 0;
`;
