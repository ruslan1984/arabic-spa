import React, { FC } from "react";
//import { useFormik } from "formik";
import { SettingDataType } from "./types";
import {
  Input,
  Button,
  Page,
  ButtonsBlock,
} from "@components/Elements/elements";
import { Loading } from "@components/types";
import { Label, Name } from "./elements";

interface SettingsProps {
  data: SettingDataType;
  loading: Loading;
  saving: Loading;
  title: string;
  saveData: (data: SettingDataType) => void;
}

export const Presenter: FC<SettingsProps> = (props: SettingsProps) => {
  //const formik = useFormik({
  //  initialValues: { ...props?.data },
  //  enableReinitialize: true,
  //  onSubmit: (values) => {
  //    props.saveData(values);
  //  },
  //});
  return (
    <Page loading={props.loading}>
      <h1>Настройки {props.title}</h1>
      {/*<form onSubmit={formik.handleSubmit}>
        <h2>SEO детально</h2>
        <Label htmlFor="detail.name">
          <Name>name</Name>
          <Input
            type="text"
            value={formik.values?.detail?.name}
            name="detail.name"
            onChange={formik.handleChange}
          />
        </Label>
        <Label htmlFor="">
          <Name>title</Name>
          <Input
            type="text"
            value={formik.values?.detail?.title}
            name="detail.title"
            onChange={formik.handleChange}
          />
        </Label>
        <Label htmlFor="">
          <Name>metaKeywords</Name>
          <Input
            type="text"
            value={formik.values?.detail?.metaKeywords}
            name="detail.metaKeywords"
            onChange={formik.handleChange}
          />
        </Label>
        <Label htmlFor="">
          <Name>metaDescription</Name>
          <Input
            type="text"
            value={formik.values?.detail?.metaDescription}
            name="detail.metaDescription"
            onChange={formik.handleChange}
          />
        </Label>
        <h2>SEO список</h2>
        <Label htmlFor="">
          <Name>name</Name>
          <Input
            type="text"
            value={formik.values?.list?.name}
            name="list.name"
            onChange={formik.handleChange}
          />
        </Label>
        <Label htmlFor="">
          <Name>title</Name>
          <Input
            type="text"
            value={formik.values?.list?.title}
            name="list.title"
            onChange={formik.handleChange}
          />
        </Label>
        <Label htmlFor="">
          <Name>metaKeywords</Name>
          <Input
            type="text"
            value={formik.values?.list?.metaKeywords}
            name="list.metaKeywords"
            onChange={formik.handleChange}
          />
        </Label>
        <Label htmlFor="">
          <Name>metaDescription</Name>
          <Input
            type="text"
            value={formik.values?.list?.metaDescription}
            name="list.metaDescription"
            onChange={formik.handleChange}
          />
        </Label>
        <ButtonsBlock>
          <Button saving={props.saving} type="submit">
            Сохранить
          </Button>
        </ButtonsBlock>
      </form>*/}
    </Page>
  );
};

export default Presenter;
