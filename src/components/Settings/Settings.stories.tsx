import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import Presenter from "./Presenter";
import { Loading } from "@components/types";
import { action } from "@storybook/addon-actions";
import { SettingDataType } from "./types";

export default {
  title: "Settings",
  decorators: [withKnobs],
};
const props = {
  saving: Loading.ok,
  loading: Loading.ok,
  title: "Грамматика",
  saveData: (data: SettingDataType) => action("click"),
  data: {
    title: "Грамматика",
    detail: {
      id: 3,
      name: "Арабский язык",
      title: "Арабская1 {name}",
      metaKeywords: "Арабская2 {name}",
      metaDescription: "Арабская3 {name}",
      code: "orthography_detail",
    },
    list: {
      id: 4,
      name: "Арабский язык",
      title: "Арабские буквы",
      metaKeywords: "Арабские буквы",
      metaDescription: "Арабские буквы",
      code: "orthography_list",
    },
  },
};

export const Settings = () => {
  return <Presenter {...props} />;
};
