import { takeEvery, put, call } from "redux-saga/effects";
import { actions, initSettings } from "./reducer";
import { Loading } from "@components/types";

export function* getData() {
  try {
    //const code = data.payload;
    //yield put(actions.setLoading(Loading.start));
    //const gl = yield call(() => {
    //  return fetch(process.env.REACT_APP_HOST + `/api/${code}/settings`, {
    //    method: "GET",
    //    headers: {
    //      "Content-Type": "application/json;charset=utf-8",
    //      Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
    //    },
    //  }).then((res) => res.json());
    //});
    //yield put(actions.setData({ ...gl, code }));
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* saveData(data: any) {
  const updateData = {
    title: data.payload.title,
    detail: data.payload.detail,
    list: data.payload.list,
  };
  const code = data.payload.code;

  try {
    yield put(actions.setSaving(Loading.start));
    yield call(() => {
      return fetch(process.env.HOST + `/api/${code}/settings`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
        },
        body: JSON.stringify(updateData),
      }).then((res) => res.json());
    });
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}

export function* pageSettingsSaga() {
  yield takeEvery(initSettings, getData);
  yield takeEvery(actions.saveData, saveData);
}
