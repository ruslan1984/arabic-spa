import { Loading } from "@components/types";

export type SettingType = {
  id: number;
  name: string;
  title: string;
  metaKeywords: string;
  metaDescription: string;
  code: string;
};
export type SettingDataType = {
  list: SettingType;
  detail: SettingType;
  title: string;
};

export type SettingTypeReducer = {
  data: SettingDataType;
  loading: Loading;
  saving: Loading;
  title: string;
  code: string;
};
