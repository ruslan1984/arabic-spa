import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import { Loading } from "@components/types";
import { SettingTypeReducer, SettingDataType } from "./types";

export const defaultState: SettingTypeReducer = {
  data: {
    title: "",
    detail: {
      id: 0,
      name: "",
      title: "",
      metaKeywords: "",
      metaDescription: "",
      code: "",
    },
    list: {
      id: 4,
      name: "",
      title: "",
      metaKeywords: "",
      metaDescription: "",
      code: "",
    },
  },
  title: "",
  code: "",
  loading: Loading.no,
  saving: Loading.no,
};

export const initSettings = createAction("initSettings");
export const slice = createSlice({
  name: "settingsPages",
  initialState: defaultState,
  reducers: {
    setData: (state, { payload }: PayloadAction<SettingDataType>) => {
      return { ...state, data: payload };
    },
    saveData: (state, { payload }: PayloadAction<SettingDataType>) => {
      return { ...state, data: payload };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    setSaving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, saving: payload };
    },
  },
});
export const { actions, reducer } = slice;
