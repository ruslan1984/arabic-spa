import React, { Component } from "react";
import { connect } from "react-redux";
import { reducerType } from "@store/reducers";
import { actions, initSettings } from "./reducer";
import { SettingDataType } from "./types";
import Presenter from "./Presenter";
import { Loading } from "@components/types";

interface SettingsProps {
  data: SettingDataType;
  loading: Loading;
  saving: Loading;
  title: string;
  code: string;
  initSettings: (code: string) => void;
  saveData: (data: SettingDataType) => void;
}
export class Settings extends Component<SettingsProps> {
  _isMounted = false;
  componentDidMount() {
    this._isMounted = true;
    this.props.initSettings(this.props.code);
  }
  componentDidUpdate() {
    return false;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    return (
      this._isMounted && (
        <Presenter
          {...this.props}
          title={this.props.title}
          loading={this.props.loading}
          saving={this.props.saving}
          saveData={this.props.saveData}
        />
      )
    );
  }
}

const mapStateToProps = (state: reducerType) => {
  const { data, loading, saving } = state.pageSettingsReducer;
  return {
    data,
    loading,
    saving,
  };
};

const mapDispatchToProps = {
  initSettings,
  saveData: actions.saveData,
};

export default Settings;
//export default connect(mapStateToProps, mapDispatchToProps)(Settings);
