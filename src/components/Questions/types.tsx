import { Loading } from "@components/types";
export type QuestionType = {
  id: number;
  name: string;
  text: string;
  lessonId: number;
  deletedAt: string | null;
};

export type UpdatingData = {
  id: number;
  name: string;
  value: string;
};

export type UpdatedData = {
  id?: number;
  name?: string;
  text?: string;
  saving?: Loading;
  lessonId?: number;
  deletedAt?: string;
};
export type GrammarItem = {
  id: number;
  name: string;
};
export type ReducerType = {
  data: QuestionType[];
  grammarList: Array<GrammarItem>;
  updatedData: UpdatedData[];
  lessonId: number;
  loading: Loading;
  removing: Loading;
  recovering: Loading;
  saving: Loading;
};
export type SaveQuestionType = {
  id: number;
  data: UpdatedData;
};
export type SaveType = {
  id: number;
  saving: Loading;
};
export type RemoveType = {
  id: number;
  removing: Loading;
};
