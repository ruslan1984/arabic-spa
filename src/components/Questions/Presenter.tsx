import React, { FC, ReactNode } from "react";
import {
  Input,
  Select,
  Button,
  RemoveButton,
  RecoverButton,
} from "@components/Elements/elements";
import { Form } from "./elements";
import { Loading } from "@components/types";
import { QuestionType, UpdatedData } from "./types";
import Editor from "@components/Editor";

interface QuestionProps {
  data: QuestionType;
  removing: Loading;
  recovering: Loading;
  saving: Loading;
  grammarList: () => ReactNode;
  onSave: (id: number, data: UpdatedData) => void;
  removeQuestion: (id: number) => void;
  recoverQuestion: (id: number) => void;
}

export const Presenter: FC<QuestionProps> = (props: QuestionProps) => {
  const f: any = {
    initialValues: { ...props.data },
    onSubmit: (values: UpdatedData) => {
      props.onSave(props.data.id, values);
    },
  };

  const removeQuestion = () => {
    props.removeQuestion(props.data.id);
  };
  const recoverQuestion = () => {
    props.recoverQuestion(props.data.id);
  };
  const updateQuestionText = (text: string) => {
    //formik.values.text = text;
  };
  return (
    <Form>
      <div>id {props.data.id}</div>
      <label htmlFor="">
        Вопрос
        <Input
          id={String(props.data.id | 0)}
          type="text"
          //value={formik.values.name || ""}
          name="name"
          //onChange={formik.handleChange}
        />
      </label>
      <label htmlFor="">
        Ответы
        {/*<Editor
          name={`questions_${props.data.id}`}
          text={formik.values.text || ""}
          onChange={updateQuestionText}
        />*/}
      </label>
      <div>
        <Select
          //defaultValue={formik.values.lessonId}
          name="lessonId"
          id={String(props.data.id | 0)}
          //onChange={formik.handleChange}
        >
          {props.grammarList()}
        </Select>
      </div>
      <Button saving={props.saving} type="submit">
        Сохранить
      </Button>
      {(() => {
        if (props.data.deletedAt) {
          return (
            <RecoverButton
              saving={props.removing}
              onClick={recoverQuestion}
              type="button"
            >
              Восстановить
            </RecoverButton>
          );
        } else {
          return (
            <RemoveButton
              saving={props.removing}
              onClick={removeQuestion}
              type="button"
            >
              Удалить
            </RemoveButton>
          );
        }
      })()}
    </Form>
  );
};

export default Presenter;
