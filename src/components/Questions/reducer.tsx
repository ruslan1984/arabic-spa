import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  QuestionType,
  ReducerType,
  SaveType,
  SaveQuestionType,
  GrammarItem,
  RemoveType,
} from "./types";
import { Loading } from "@components/types";

export const defaultState: ReducerType = {
  data: [],
  grammarList: [],
  lessonId: 0,
  updatedData: [],
  loading: Loading.no,
  removing: Loading.no,
  recovering: Loading.no,
  saving: Loading.no,
};

export const grammarQuestionSlice = createSlice({
  name: "grammarQuestion",
  initialState: defaultState,
  reducers: {
    setLessonId: (state, { payload }: PayloadAction<number>) => {
      return { ...state, lessonId: payload };
    },
    setBasketLessonId: (state, { payload }: PayloadAction<number>) => {
      return { ...state, lessonId: payload };
    },
    setData: (state, { payload }: PayloadAction<QuestionType[]>) => {
      return { ...state, data: payload };
    },
    setGrammarList: (state, { payload }: PayloadAction<GrammarItem[]>) => {
      return { ...state, grammarList: payload };
    },
    saveQuestion: (state, { payload }: PayloadAction<any>) => {
      const id = payload.id;
      const getData = payload.data;
      const stateData = [...state.data];
      const data = stateData.map((item) => {
        if (item.id == id) {
          return getData;
        }
        return item;
      });
      return { ...state, data };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    setSaving: (state, { payload }: PayloadAction<SaveType>) => {
      const id = payload.id;
      const saving = payload.saving;
      const stateData = [...state.data];
      const data = stateData.map((item) => {
        if (item.id == id) {
          return { ...item, saving };
        }
        return item;
      });
      return { ...state, data };
    },
    setRemoving: (state, { payload }: PayloadAction<RemoveType>) => {
      const id = payload.id;
      const removing = payload.removing;
      const stateData = [...state.data];
      const data = stateData.map((item) => {
        if (item.id == id) {
          return { ...item, removing };
        }
        return item;
      });
      return { ...state, data };
    },
    removeQuestion: (state, { payload }: PayloadAction<number>) => {
      const id = payload;
      const stateData = [...state.data];
      const data = stateData.map((item) => {
        if (item.id == id) {
          return { ...item, deletedAt: "now" };
        }
        return item;
      });
      return { ...state, data };
    },
    recoverQuestion: (state, { payload }: PayloadAction<number>) => {
      const id = payload;
      const stateData = [...state.data];
      const data = stateData.map((item) => {
        if (item.id == id) {
          return { ...item, deletedAt: null };
        }
        return item;
      });
      return { ...state, data };
    },
    addQuestion: (state) => {
      const data = [...state.data];
      const item: any = {
        id: 0,
        name: "",
        text: "",
        lessonId: state.lessonId,
      };
      data.push(item);
      return { ...state, data };
    },
  },
});
export const { actions, reducer } = grammarQuestionSlice;
