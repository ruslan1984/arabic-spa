import { takeEvery, put, call } from "redux-saga/effects";
import { actions } from "./reducer";
import { Loading } from "@components/types";
import { fetchGet } from "@helpers/fetch";
import { SaveQuestionType } from "./types";

export function* setLessonId(data: any) {
  const lessonId = data.payload;
  if (lessonId == 0) return;
  try {
    yield put(actions.setLoading(Loading.start));

    const url = `/api/grammar/${lessonId}/questions_with_grammar`;

    const fetchData: { data: any[]; grammarList: any[] } = yield call(
      async () => await fetchGet(url)
    );

    //const fetchData:any = yield call(() => {
    //  return fetch(
    //    process.env.REACT_APP_HOST + `/api/grammar/${lessonId}/questions_with_grammar`,
    //    {
    //      method: "GET",
    //      headers: {
    //        "Content-Type": "application/json;charset=utf-8",
    //        Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
    //      },
    //    }
    //  ).then((res) => res.json());
    //});

    yield put(actions.setData(fetchData.data));
    yield put(actions.setGrammarList(fetchData.grammarList));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* setBasketLessonId(data: any) {
  try {
    yield put(actions.setLoading(Loading.start));
    //const lessonId = data.payload;
    //const fetchData: TTest[] = yield call(
    //  async () => await fetchGet(`/api/question/basket/${lessonId}`)
    //);
    //const fetchData = yield call(() => {
    //  return fetch(process.env.REACT_APP_HOST + `/api/question/basket/${lessonId}`, {
    //    method: "GET",
    //    headers: {
    //      "Content-Type": "application/json;charset=utf-8",
    //      Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
    //    },
    //  }).then((res) => res.json());
    //});
    //yield put(actions.setData(fetchData.data));
    //yield put(actions.setGrammarList(fetchData.grammarList));
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* save(questionId: number, question: any) {
  try {
    yield put(actions.setSaving({ id: questionId, saving: Loading.start }));
    yield call(async () => {
      await fetch(process.env.REACT_APP_HOST + `/api/question/${questionId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
        },
        body: JSON.stringify(question),
      });
    });
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving({ id: questionId, saving: Loading.ok }));
  }
}

export function* add(questionId: number, question: any) {
  try {
    yield put(actions.setSaving({ id: questionId, saving: Loading.start }));
    yield call(async () => {
      await fetch(process.env.REACT_APP_HOST + `/api/question/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
        },
        body: JSON.stringify(question),
      });
    });
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving({ id: questionId, saving: Loading.ok }));
  }
}

export function* saveQuestion(data: any) {
  const questionId = (data as any).payload.id;
  const question = (data as any).payload.data;
  if (questionId == 0) {
    yield add(questionId, question);
  } else {
    yield save(questionId, question);
  }
}

export function* removeQuestion(data: any) {
  const questionId = (data as any).payload;
  try {
    yield put(actions.setRemoving({ id: questionId, removing: Loading.start }));
    yield call(async () => {
      await fetch(process.env.REACT_APP_HOST + `/api/question/${questionId}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
        },
      });
    });
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setRemoving({ id: questionId, removing: Loading.ok }));
  }
}
export function* recoverQuestion(data: any) {
  const questionId = (data as any).payload;
  try {
    yield put(actions.setRemoving({ id: questionId, removing: Loading.start }));
    yield call(async () => {
      await fetch(
        process.env.REACT_APP_HOST + `/api/question/recover/${questionId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json;charset=utf-8",
            Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
          },
        }
      );
    });
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setRemoving({ id: questionId, removing: Loading.ok }));
  }
}

export function* questionsGrammarSaga() {
  yield takeEvery(actions.setLessonId, setLessonId);
  yield takeEvery(actions.setBasketLessonId, setBasketLessonId);
  yield takeEvery(actions.saveQuestion, saveQuestion);
  yield takeEvery(actions.removeQuestion, removeQuestion);
  yield takeEvery(actions.recoverQuestion, recoverQuestion);
}
