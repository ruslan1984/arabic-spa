import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import Presenter from "./Presenter";
import { Loading } from "@components/types";
import { UpdatedData } from "./types";
import { action } from "@storybook/addon-actions";

export default {
  title: "Grammar",
  decorators: [withKnobs],
};
const props = {
  data: {
    id: 1,
    name: "Определите род следующих слов единственного числа:",
    text: "<p>1.&nbsp;مَدْرَسَةٌ&nbsp;&nbsp;&nbsp;</p><p>2.&nbsp;دَفْتَرٌ&nbsp;&nbsp;&nbsp;</p><p>3.&nbsp;بَيْتٌ&nbsp;&nbsp;&nbsp;</p><p>4.&nbsp;مِسْطَرَةٌ&nbsp;&nbsp;</p><p>5.&nbsp;لَوْحٌ&nbsp;&nbsp;</p><p>6.&nbsp;سَيَّارَةٌ&nbsp;&nbsp;&nbsp;</p><p>7.&nbsp;قَلَمٌ&nbsp;&nbsp;&nbsp;</p><p>8.&nbsp;رِسَالَةٌ&nbsp;</p><p>9.&nbsp;مِفْتَاحٌ&nbsp;&nbsp;&nbsp;</p><p>10.&nbsp;مَلْعَبٌ&nbsp;&nbsp;&nbsp;</p><p>11.&nbsp;بَابٌ&nbsp;&nbsp;</p><p>12.&nbsp;مِلْعَقَةٌ&nbsp;&nbsp;</p><p>13.&nbsp;كُرَةٌ&nbsp;&nbsp;</p><p>14.&nbsp;دِينٌ&nbsp;&nbsp;</p><p>15.&nbsp;رِيشَةٌ&nbsp;&nbsp;</p><p>16.&nbsp;مِقَصٌّ</p>",
    lessonId: 3,
    deletedAt: null,
  },
  removing: Loading.ok,
  recovering: Loading.ok,
  saving: Loading.ok,
  onSave: (id: number, data: UpdatedData) => {
    action(String(id));
  },
  removeQuestion: (id: number) => {
    action(String(id));
  },
  recoverQuestion: (id: number) => {
    action(String(id));
  },
};

const grammarList = () => [
  <option key={1} value={1}>
    {"name1"}
  </option>,
  <option key={2} value={2}>
    {"name2"}
  </option>,
];

export const GrammarQuestions = () => {
  return <Presenter grammarList={grammarList} {...props} />;
};
