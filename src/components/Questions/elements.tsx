import styled from "@emotion/styled";

export const WordLine = styled.div`
  display: flex;
  margin: 5px 0;
`;

export const WordBlock = styled.div`
  display: flex;
  flex-direction: column;
  flex: 3;
  margin: 0 5px;
`;

export const WordTypeBlock = styled(WordBlock)`
  flex: 1;
`;

export const WordsHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const Form = styled.form`
  padding: 10px 0 5px;
  border-bottom: 1px solid #dedede;
`;
