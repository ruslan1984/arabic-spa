import React, { Component } from "react";
import Presenter from "./Presenter";
import {
  QuestionType,
  UpdatedData,
  GrammarItem,
  SaveQuestionType,
} from "./types";
import { Loading } from "@components/types";

interface QuestionProps {
  data: QuestionType[];
  grammarList: GrammarItem[];
  loading: Loading;
  id: number;
  removing: Loading;
  recovering: Loading;
  saving: Loading;
  saveQuestion: (data: SaveQuestionType) => void;
  removeQuestion: (id: number) => void;
  recoverQuestion: (id: number) => void;
  setUpdateData: (data: UpdatedData) => void;
  setLessonId: (lessonId: number) => void;
  addQuestion: () => void;
}

export class QuestionsBase<T> extends Component<QuestionProps> {
  onSave = (id: number, data: UpdatedData) => {
    this.props.saveQuestion({ id, data });
  };
  grammarList = () => {
    if (this.props.grammarList) {
      return this.props.grammarList.map((item, id) => (
        <option key={id} value={item.id}>
          {item.name}
        </option>
      ));
    }
  };

  questionList() {
    if (this.props.data.length > 0) {
      return this.props.data.map((item) => (
        <Presenter
          key={item.id.toString()}
          onSave={this.onSave}
          removeQuestion={this.props.removeQuestion}
          recoverQuestion={this.props.recoverQuestion}
          grammarList={this.grammarList}
          data={item}
          removing={this.props.removing}
          recovering={this.props.recovering}
          saving={this.props.saving}
        />
      ));
    } else {
      return <div>Пусто</div>;
    }
  }
}

export default QuestionsBase;
