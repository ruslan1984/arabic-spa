import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { Page, Button } from "@components/Elements/elements";
import { actions } from "./reducer";
import { reducerType } from "@store/reducers";
import {
  QuestionType,
  UpdatedData,
  GrammarItem,
  SaveQuestionType,
} from "./types";
import { Loading } from "@components/types";
//import { RouteComponentProps } from '@types/react-router-dom';
//import { QuestionsBase } from "./QuestionsBase";
import { WordsHeader } from "./elements";
import Presenter from "./Presenter";

interface QuestionsProps {
  lessonId: number;
  data: QuestionType[];
  grammarList: GrammarItem[];
  loading: Loading;
  removing: Loading;
  recovering: Loading;
  saving: Loading;
  saveQuestion: (data: SaveQuestionType) => void;
  removeQuestion: (id: number) => void;
  recoverQuestion: (id: number) => void;
  //setUpdateData: ( data: UpdatedData ) => void;
  setLessonId: (lessonId: number) => void;
  addQuestion: () => void;
}

export class Questions extends Component<QuestionsProps> {
  componentDidMount() {
    const lessonId = this.props.lessonId;
    this.props.setLessonId(lessonId);
  }

  onSave = (id: number, data: UpdatedData) => {
    this.props.saveQuestion({ id, data });
  };
  grammarList = () => {
    if (this.props.grammarList) {
      return this.props.grammarList.map((item, id) => (
        <option key={id} value={item.id}>
          {item.name}
        </option>
      ));
    }
  };

  questionList() {
    if (this.props.data.length > 0) {
      return this.props.data.map((item: QuestionType) => (
        <span key={item.id.toString()}>
          <Presenter
            data={item}
            onSave={this.onSave}
            removeQuestion={this.props.removeQuestion}
            recoverQuestion={this.props.recoverQuestion}
            grammarList={this.grammarList}
            removing={this.props.removing}
            recovering={this.props.recovering}
            saving={this.props.saving}
          />
        </span>
      ));
    } else {
      return <div>Пусто</div>;
    }
  }

  render() {
    return (
      <Page loading={this.props.loading}>
        <WordsHeader>
          <h1>Вопросы</h1>
          <Link to={"questions/basket"}>Корзина</Link>
        </WordsHeader>
        {this.questionList()}
        <Button onClick={this.props.addQuestion}>Добавить вопрос</Button>
      </Page>
    );
  }
}

export const mapStateToProps = (state: reducerType) => {
  const { data, loading, grammarList, removing, recovering, saving } =
    state.questionsGrammarReducer;
  return {
    grammarList,
    data,
    loading,
    removing,
    recovering,
    saving,
  };
};

const mapDispatchToProps = {
  //addQuestion: actions.addQuestion,
  //setUpdateData: actions.setUpdateData,
  setLessonId: actions.setLessonId,
  saveQuestion: actions.saveQuestion,
  removeQuestion: actions.removeQuestion,
  recoverQuestion: actions.recoverQuestion,
};

export default Questions;
//export default connect(mapStateToProps, mapDispatchToProps)(Questions);
