import React from "react";
import { connect } from "react-redux";
//import { RouteComponentProps } from "react-router-dom";
//import { RouteComponentProps } from '@types/react-router-dom';
import { actions } from "./reducer";
import { reducerType } from "@store/reducers";
import { Page } from "@components/Elements/elements";
import { QuestionsBase } from "./QuestionsBase";

import {
  QuestionType,
  UpdatedData,
  GrammarItem,
  SaveQuestionType,
} from "./types";
import { Loading } from "@components/types";

interface QuestionProps {
  data: QuestionType[];
  grammarList: GrammarItem[];
  loading: Loading;
  saveWord: (data: SaveQuestionType) => void;
  removeWord: (id: number) => void;
  recoverWord: (id: number) => void;
  setLessonId: (lessonId: number) => void;
}

export class Basket extends QuestionsBase<QuestionProps> {
  componentDidMount() {
    const lessonId: number = 1; //(this.props.match.params as any).id;
    this.props.setLessonId(lessonId);
  }
  render(): any {
    return (
      <Page loading={this.props.loading}>
        {/* <WordsHeader> */}
        <h1>Корзина</h1>
        {/* <Link to=".">{"<<"}</Link> */}
        {/* </WordsHeader> */}
        {this.questionList()}
      </Page>
    );
  }
}

const mapStateToProps = (state: reducerType) => {
  const { data, loading, grammarList } = state.questionsGrammarReducer;
  return {
    grammarList,
    data,
    loading,
  };
};

const mapDispatchToProps = {
  setLessonId: actions.setBasketLessonId,
  saveQuestion: actions.saveQuestion,
  removeQuestion: actions.removeQuestion,
  recoverQuestion: actions.recoverQuestion,
};

export default Basket;
//export default connect(mapStateToProps, mapDispatchToProps)(Basket);
