import React, {
  DragEvent,
  FC,
  FormEvent,
  useState,
  useEffect,
  ReactNode,
} from "react";
import Presenter from "./Presenter";

interface IDragAndDrop {
  children?: ReactNode;
  name?: string;
  readAsDataURL?: boolean;
  expansion?: string[];
  onDropResult: (str: string, name?: string) => void;
}

export const DragAndDrop: FC<IDragAndDrop> = ({
  name,
  children,
  readAsDataURL = false,
  onDropResult,
  expansion = [],
}: IDragAndDrop) => {
  const [drag, setDrag] = useState<boolean>(false);
  const [errorText, setErrorText] = useState("");
  const [fileName, setFileName] = useState("");
  const [input, setInput] = useState<HTMLInputElement>();

  useEffect(() => {
    const newInput: HTMLInputElement = document.createElement("input");
    if (newInput !== null) {
      newInput.type = "file";
      newInput.addEventListener("change", () => {
        const reader = new FileReader();
        if (newInput?.files && newInput.files.length > 0) {
          const file = newInput?.files[0];
          readAsDataURL ? reader.readAsDataURL(file) : reader.readAsText(file);
          const fileName = file.name;
          setFileName(fileName);
          const ex: string | undefined = fileName.split(".").pop();
          if (!ex) {
            throw "Файл без расширения";
          }
          if (ex && expansion.length > 0 && expansion.indexOf(ex) === -1) {
            throw `Не верное расширение файла. Допустимые расширения ${expansion.join()}`;
          }
          setErrorText("");
          reader.onload = () => {
            onDropResult(reader.result as string, name);
          };
        }
      });
      setInput(newInput);
    }
  }, []);
  const onDragHandler = (e: DragEvent) => {
    e.preventDefault();
    setDrag(true);
  };

  const onClick = () => {
    input?.click();
  };

  const onDropHandler = (e: DragEvent) => {
    e.preventDefault();
    try {
      const reader = new FileReader();
      const { files } = e.dataTransfer;

      const fileName = files[0].name;
      const ex: string | undefined = fileName.split(".").pop();
      if (!ex) {
        throw "Файл без расширения";
      }
      if (ex && expansion.length > 0 && expansion.indexOf(ex) === -1) {
        throw `Не верное расширение файла. Допустимые расширения ${expansion.join()}`;
      }
      setErrorText("");
      readAsDataURL
        ? reader.readAsDataURL(files[0])
        : reader.readAsText(files[0]);

      setFileName(files[0].name);
      reader.onload = () => {
        onDropResult(reader.result as string, name);
      };
      setDrag(false);
    } catch (err: any) {
      setErrorText(err?.message);
    }
  };

  return (
    <Presenter
      drag={drag}
      fileName={fileName}
      onDragStart={onDragHandler}
      onDragOver={onDragHandler}
      onDrop={onDropHandler}
      onClick={onClick}
      errorText={errorText}
    >
      {children}
    </Presenter>
  );
};

export default DragAndDrop;
export type DropType = string | ArrayBuffer | null;
