import styled from "@emotion/styled";

export const DragAndDropAraea = styled.div`
  padding: 40px;
  background: ${({
    error,
    drag,
    active,
  }: {
    error?: boolean;
    drag?: boolean;
    active?: boolean;
  }) => (error ? "#f9b7b7" : active ? "#edfbed" : drag ? "#ddd" : "#eee")};
  border-radius: 10px;
  display: flex;
  justify-content: center;
  transition: 0.5s;
  cursor: pointer;
  text-align: center;
  width: 100%;
  box-sizing: border-box;
`;
