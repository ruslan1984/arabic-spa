import React, { DragEventHandler, FC, ReactNode, useEffect } from "react";
import { DragAndDropAraea } from "./styles";

interface IDragAndDrop {
  drag: boolean;
  onDragStart: DragEventHandler;
  onDragOver: DragEventHandler;
  onDrop: DragEventHandler;
  children?: ReactNode;
  errorText?: string;
  fileName?: string;
  onClick: () => void;
}

const Presenter: FC<IDragAndDrop> = ({
  drag,
  onDragStart,
  onDragOver,
  onDrop,
  children,
  errorText,
  fileName,
  onClick,
}: IDragAndDrop) => {
  return (
    <>
      <DragAndDropAraea
        drag={drag}
        active={Boolean(fileName)}
        onDragStart={onDragStart}
        onDragOver={onDragOver}
        onDrop={onDrop}
        error={errorText != ""}
        onClick={onClick}
      >
        {errorText || fileName || children || "Перетащите сюда файл"}
      </DragAndDropAraea>
    </>
  );
};

export default Presenter;
