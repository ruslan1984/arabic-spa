import React from "react";
//import { withKnobs } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import Presenter from "./index";

export default {
  title: "Elements",
  //decorators: [withKnobs],
};

export const DragAndDrop = () => <Presenter onDropResult={action("drop")} />;
