import { Quill } from "react-quill-with-table";

const Size = Quill.import("attributors/style/size");

Size.whitelist = [
  "8px",
  "9px",
  "10px",
  "12px",
  "14px",
  "16px",
  "18px",
  "20px",
  "22px",
  "24px",
  "26px",
  "28px",
  "30px",
  "36px",
  "48px",
];

Quill.register(Size, true);

const colors = [
  "#000000",
  "#e60000",
  "#ff9900",
  "#ffff00",
  "#008a00",
  "#0066cc",
  "#9933ff",
  "#ffffff",
  "#facccc",
  "#ffebcc",
  "#ffffcc",
  "#cce8cc",
  "#cce0f5",
  "#ebd6ff",
  "#bbbbbb",
  "#f06666",
  "#ffc266",
  "#ffff66",
  "#66b966",
  "#66a3e0",
  "#c285ff",
  "#888888",
  "#a10000",
  "#b26b00",
  "#b2b200",
  "#006100",
  "#0047b2",
  "#6b24b2",
  "#444444",
  "#5c0000",
  "#663d00",
  "#666600",
  "#003700",
  "#002966",
  "#3d1466",
  "custom-color",
];

export const modules = {
  table: false,
  toolbar: [
    [{ header: "1" }, { header: "2" }, { font: [] }],
    [{ size: Size.whitelist }],
    ["bold", "italic", "underline", "strike", "blockquote"],
    [
      {
        color: colors,
      },
      {
        background: colors,
      },
    ],
    [
      { list: "ordered" },
      { list: "bullet" },
      { align: [] },
      { indent: "-1" },
      { indent: "+1" },
    ],
    //['link', 'image', 'video'],
    ["table"],
    ["clean"],
  ],
  clipboard: {
    matchVisual: true,
  },
};
