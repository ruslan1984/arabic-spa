import React, { FC, memo, useCallback, useState, useRef } from "react";
import ReactQuill from "react-quill-with-table";

import cn from "classnames";
import {
  ButtonsBlock,
  ModeBtn,
  TextHTML,
  ClearNbspButton,
  ModeButtonsBlock,
} from "./elements";
import { modules } from "./modules";
interface EditorProps {
  value: string;
  name: string;
  onChange?: (data: string, name: string) => void;
}

export const Editor: FC<EditorProps> = ({
  value,
  name,
  onChange,
}: EditorProps) => {
  const textRef = useRef<HTMLTextAreaElement>(null);
  const [mode, setMode] = useState(0);
  const handleChange = useCallback((data: string) => {
    onChange && onChange(String(data), name);
  }, []);
  const handleTextChange = useCallback(() => {
    onChange && onChange(String(textRef?.current?.value || ""), name);
  }, []);

  const setTextMode = useCallback(() => {
    setMode(0);
  }, [mode]);
  const setHTMLMode = useCallback(() => {
    setMode(1);
  }, [mode]);

  const clearNbsp = useCallback(() => {
    if (value) {
      const newText = value.replace(/&nbsp;/g, " ");
      onChange && onChange(newText, name);
    }
  }, [value]);

  return (
    <div>
      <ButtonsBlock>
        <ModeButtonsBlock>
          <ModeBtn className={cn({ active: mode === 0 })} onClick={setTextMode}>
            Text
          </ModeBtn>
          <ModeBtn className={cn({ active: mode === 1 })} onClick={setHTMLMode}>
            HTML
          </ModeBtn>
        </ModeButtonsBlock>
        <ClearNbspButton onClick={clearNbsp}>nbsp;</ClearNbspButton>
      </ButtonsBlock>
      {mode === 0 ? (
        <ReactQuill
          value={value}
          onChange={handleChange}
          modules={modules}
          theme="snow"
        />
      ) : (
        <TextHTML ref={textRef} value={value} onChange={handleTextChange} />
      )}
    </div>
  );
};

export default memo(Editor, () => false);
