import styled from "@emotion/styled";

export const ButtonsBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ModeButtonsBlock = styled.div`
  display: flex;
  margin-bottom: 0.5rem;
`;

export const ModeBtn = styled.div`
  padding: 0.5rem;
  margin-right: 0.5rem;
  background: #edfbed;
  cursor: pointer;
  transition: 1s;
  border-radius: 5px;
  &:hover {
    background: #cef6ce;
  }
  &.active {
    background: #cef6ce;
  }
`;

export const TextHTML = styled.textarea`
  width: 100%;
  min-height: 300px;
  border: 1px solid #ccc;
  padding: 0.5rem;
  font-size: 20px;
`;

export const ClearNbspButton = styled.div`
  background: #f7b4b4;
  border: none;
  margin-top: 5px;
  margin-bottom: 5px;
  border-radius: 5px;
  text-decoration: line-through;
  cursor: pointer;
  transition: 0.5s;
  display: inline-block;
  padding: 5px;
  font-size: 10px;
  &:hover {
    background: #ef9595;
  }
`;
