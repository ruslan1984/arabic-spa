export type EditorType = {
  setData: (text: string) => void;
  editor: { ui: { view: { toolbar: { element: object } } } };
};
