export enum Loading {
  start,
  ok,
  error,
  no,
}
// export enum Saving {
//   none,
//   start,
//   ok,
// }

export enum Status {
  draft,
  published,
}

export enum TestResultRating {
  bad,
  good,
  excellent,
}
