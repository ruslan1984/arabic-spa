export type grammar = {
  id: number;
  name: string;
  text: string;
};
export type GrammarList = {
  id: number;
  name: string;
};
