import React from "react";
import GrammarList from "@/modules/Grammar/List";
import AdminLayout from "@adminScreen/AdminLayout";

export const GrammarListScreen = (): JSX.Element => {
  return (
    <AdminLayout>
      <GrammarList />
    </AdminLayout>
  );
};

export default GrammarListScreen;
