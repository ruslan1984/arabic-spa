import React from "react";
import { useParams } from "react-router-dom";
import Words from "@modules/Words";
import GrammarDetailLayout from "@grammarScreen/Detail/";

const GrammarWords = (): JSX.Element => {
  const { id } = useParams();
  return (
    <GrammarDetailLayout id={Number(id)} active={2}>
      <Words lessonId={Number(id)} />
    </GrammarDetailLayout>
  );
};

export default GrammarWords;
