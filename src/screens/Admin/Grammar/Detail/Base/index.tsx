import React from "react";
import { useParams } from "react-router-dom";
import Base from "@modules/Grammar/Detail/Base";
import GrammarDetailLayout from "@grammarScreen/Detail/";

const GrammarBase = (): JSX.Element => {
  const { id } = useParams();
  return (
    <GrammarDetailLayout id={Number(id)} active={0}>
      <Base lessonId={Number(id)} />
    </GrammarDetailLayout>
  );
};

export default GrammarBase;
