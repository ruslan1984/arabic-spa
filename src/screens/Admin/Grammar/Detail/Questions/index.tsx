import React, { FC } from "react";
import { useParams } from "react-router-dom";
import Questions from "@components/Questions";
import GrammarDetailLayout from "@grammarScreen/Detail/";
import { reducerType } from "@store/reducers";

export const GrammarQuestions: FC = () => {
  const { id } = useParams();
  return (
    <GrammarDetailLayout id={Number(id)} active={3}>
      {/*<Questions lessonId={id} />*/}
    </GrammarDetailLayout>
  );
};

export default GrammarQuestions;
