import React from "react";
import { useParams } from "react-router-dom";
import ArabicSentence from "@modules/Grammar/Detail/ArabicSentence";
import GrammarDetailLayout from "@grammarScreen/Detail/";

const GrammarArabicSentence = (): JSX.Element => {
  const { id } = useParams();
  return (
    <GrammarDetailLayout id={Number(id)} active={5}>
      <ArabicSentence lessonId={Number(id)} />
    </GrammarDetailLayout>
  );
};

export default GrammarArabicSentence;
