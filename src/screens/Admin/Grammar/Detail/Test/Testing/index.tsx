import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { grammarTestListGet } from "@modules/Test/TestList/reducer";
import { reducerType } from "@store/reducers";
import GrammarDetailLayout from "@grammarScreen/Detail/";
import { Testing } from "@modules/Test/Testing";
//import { Loading } from "@components/types";

interface IParams {
  id: string;
}

const GrammarTesting = (): any => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const { data, loading } = useSelector((state: reducerType) => state.testList);
  useEffect(() => {
    dispatch(grammarTestListGet(Number(id)));
  }, []);

  return (
    <GrammarDetailLayout id={Number(id)} active={1}>
      {/*{loading == Loading.ok ? */}
      <Testing data={data} />
      {/*: <>Загрузка...</>}*/}
    </GrammarDetailLayout>
  );
};

export default GrammarTesting;
