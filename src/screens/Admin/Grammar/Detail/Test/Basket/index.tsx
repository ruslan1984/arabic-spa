import React from "react";
import { useParams } from "react-router-dom";
import TestList from "@modules/Test/TestList";
import GrammarDetailLayout from "@grammarScreen/Detail/";

export const GrammarTest = () => {
  const { id } = useParams();
  return (
    <GrammarDetailLayout id={Number(id)} active={1}>
      <TestList type="grammar_basket" lessonId={Number(id)} />
    </GrammarDetailLayout>
  );
};

export default GrammarTest;
