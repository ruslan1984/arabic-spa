import React from "react";
import { Link, useParams } from "react-router-dom";
import TestList from "@modules/Test/TestList";
import GrammarDetailLayout from "@grammarScreen/Detail/";
import routes from "@routes/routes";

export const GrammarTest = () => {
  const { id } = useParams();
  return (
    <GrammarDetailLayout id={Number(id)} active={1}>
      <TestList type="grammar" lessonId={Number(id)} />
      <br />
      <Link to={routes.grammar.TestBasket(id)}>Корзина</Link>
    </GrammarDetailLayout>
  );
};

export default GrammarTest;
