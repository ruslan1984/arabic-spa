import React from "react";
import { useParams } from "react-router-dom";
import { TestCard } from "@modules/Test/TestCard";
import GrammarDetailLayout from "@grammarScreen/Detail/";

export const GrammarQuestion = () => {
  const { id, testId } = useParams();
  return (
    <>
      <GrammarDetailLayout id={Number(id)} active={1}>
        <TestCard lessonId={Number(id)} testId={Number(testId)} />
      </GrammarDetailLayout>
    </>
  );
};

export default GrammarQuestion;
