import React, { FC, ReactNode } from "react";
import { ListLink, TopMenu, Page } from "@components/Elements/elements";
import AdminLayout from "@adminScreen/AdminLayout";
import routes from "@routes/routes";

interface IGrammarDetailLayout {
  children: ReactNode;
  id: number;
  active: number;
}

export const GrammarDetailLayout: FC<IGrammarDetailLayout> = ({
  children,
  id,
  active,
}: IGrammarDetailLayout) => {
  return (
    <AdminLayout>
      <Page>
        <TopMenu>
          <ul>
            <li>
              <ListLink
                to={routes.grammar.Base(id)}
                active={active === 0 ? 1 : 0}
              >
                Основное
              </ListLink>
            </li>
            {id > 0 && (
              <>
                <li>
                  <ListLink
                    to={routes.grammar.TestList(id)}
                    active={active === 1 ? 1 : 0}
                  >
                    Тесты
                  </ListLink>
                </li>
                <li>
                  <ListLink
                    to={routes.grammar.Words(id)}
                    active={active === 2 ? 1 : 0}
                  >
                    Слова
                  </ListLink>
                </li>
                <li>
                  <ListLink
                    to={routes.grammar.ArabicText(id)}
                    active={active === 5 ? 1 : 0}
                  >
                    Арабский Текст
                  </ListLink>
                </li>
                <li>
                  <ListLink
                    to={routes.grammar.Questions(id)}
                    active={active === 3 ? 1 : 0}
                  >
                    Вопросы
                  </ListLink>
                </li>
                <li>
                  <ListLink
                    to={`/admin/grammar/basket`}
                    active={active === 4 ? 1 : 0}
                  >
                    Корзина
                  </ListLink>
                </li>
              </>
            )}
          </ul>
        </TopMenu>
        {children}
      </Page>
    </AdminLayout>
  );
};

export default GrammarDetailLayout;
