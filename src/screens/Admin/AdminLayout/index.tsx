import React, { FC, ReactNode } from "react";
import Header from "@/screens/Admin/Header";
import LeftMenuPresenter from "@adminScreen/LeftMenu/LeftMenuPresenter";
import { MainPage } from "@components/Elements/elements";

interface AdminLayoutProps {
  children: ReactNode;
}

export const AdminLayout: FC<AdminLayoutProps> = ({
  children,
}: AdminLayoutProps) => {
  return (
    <>
      <Header />
      <MainPage>
        <LeftMenuPresenter />
        {children}
      </MainPage>
    </>
  );
};
export default AdminLayout;
