import React, { FC, ReactNode } from "react";
import { ListLink, TopMenu, Page } from "@components/Elements/elements";
import AdminLayout from "@adminScreen/AdminLayout";
import routes from "@routes/routes";

interface IOrthographyDetailLayout {
  children: ReactNode;
  id: number;
  active: number;
}

export const OrthographyDetailLayout: FC<IOrthographyDetailLayout> = ({
  children,
  id,
  active,
}: IOrthographyDetailLayout) => {
  return (
    <AdminLayout>
      <Page>
        <TopMenu>
          <ul>
            <li>
              <ListLink
                to={routes.orthography.Id(id)}
                active={active === 0 ? 1 : 0}
              >
                Основное
              </ListLink>
            </li>
            {id > 0 && (
              <>
                <li>
                  <ListLink
                    to={routes.orthography.Harf(id)}
                    active={active === 1 ? 1 : 0}
                  >
                    Написание
                  </ListLink>
                </li>
                <li>
                  <ListLink
                    to={routes.orthography.Sound(id)}
                    active={active === 2 ? 1 : 0}
                  >
                    Звучание
                  </ListLink>
                </li>
                {/*<li>
                  <ListLink
                    to={`/admin/grammar/basket`}
                    active={active === 4 ? 1 : 0}
                  >
                    Корзина
                  </ListLink>
                </li>*/}
              </>
            )}
          </ul>
        </TopMenu>
        {children}
      </Page>
    </AdminLayout>
  );
};

export default OrthographyDetailLayout;
