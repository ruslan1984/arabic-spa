import React from "react";
import { useParams } from "react-router-dom";
import Sound from "@modules/Orthography/Detail/Sound";
import OrthographyDetailLayout from "@orthographyScreen/Detail/";

const OrthographyBase = (): JSX.Element => {
  const { id } = useParams();
  return (
    <OrthographyDetailLayout id={Number(id)} active={2}>
      <Sound lessonId={Number(id)} />
    </OrthographyDetailLayout>
  );
};

export default OrthographyBase;
