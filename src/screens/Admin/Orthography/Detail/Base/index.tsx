import React from "react";
import { useParams } from "react-router-dom";
import Base from "@modules/Orthography/Detail/Base";
import OrthographyDetailLayout from "@orthographyScreen/Detail/";

const OrthographyBase = (): JSX.Element => {
  const { id } = useParams();
  return (
    <OrthographyDetailLayout id={Number(id)} active={0}>
      <Base lessonId={Number(id)} />
    </OrthographyDetailLayout>
  );
};

export default OrthographyBase;
