import React from "react";
import { useParams } from "react-router-dom";
import Harf from "@modules/Orthography/Detail/Harf";
import OrthographyDetailLayout from "@orthographyScreen/Detail/";

const OrthographyBase = (): JSX.Element => {
  const { id } = useParams();
  return (
    <OrthographyDetailLayout id={Number(id)} active={1}>
      <Harf lessonId={Number(id)} />
    </OrthographyDetailLayout>
  );
};

export default OrthographyBase;
