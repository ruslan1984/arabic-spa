import React from "react";
import OrthographyList from "@/modules/Orthography/List";
import AdminLayout from "@adminScreen/AdminLayout";

export const OrthographyListScreen = (): JSX.Element => {
  return (
    <AdminLayout>
      <OrthographyList />
    </AdminLayout>
  );
};

export default OrthographyListScreen;
