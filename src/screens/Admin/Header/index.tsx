import React, { FC } from "react";
import { actions } from "@modules/Auth/reducer";
import { HeaderPresenter } from "./HeaderPresenter";
import { useDispatch } from "react-redux";

const Header: FC = () => {
  const dispatch = useDispatch();
  const logout = () => {
    dispatch(actions.logout());
  };

  return <HeaderPresenter logout={logout} />;
};

export default Header;
