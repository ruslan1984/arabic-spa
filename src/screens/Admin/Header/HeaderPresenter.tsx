import React, { FC } from "react";
import { Header, H2 } from "./styles";

interface IHeader {
  logout: () => void;
}

export const HeaderPresenter: FC<IHeader> = ({ logout }: IHeader) => {
  return (
    <Header>
      <H2>Arabic online</H2>
      <button onClick={logout}> Выйти</button>
    </Header>
  );
};
export default HeaderPresenter;
