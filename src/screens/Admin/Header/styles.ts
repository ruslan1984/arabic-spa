import styled from "@emotion/styled";

export const Header = styled.div`
  padding: 10px 20px;
  border-bottom: 1px solid #eee;
  display: flex;
  justify-content: space-between;
  box-shadow: 1px 0 3px lightgrey;
  z-index: 1;
  position: relative;
`;
export const H2 = styled.h2`
  margin: 0;
  text-transform: uppercase;
  font-weight: 400;
`;
