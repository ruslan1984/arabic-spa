import React, { FC } from "react";
import { Ul, Menu, ListLink, TitleLi } from "./styles";

const LeftMenuPresenter: FC = () => {
  return (
    <Menu>
      <TitleLi>Грамматика</TitleLi>
      <Ul>
        <li>
          <ListLink to="/admin/grammar">Страницы</ListLink>
        </li>
        <li>
          <ListLink to="/admin/grammar/basket">Корзина</ListLink>
        </li>
        <li>
          <ListLink to="/admin/grammar/settings">Настройки</ListLink>
        </li>
      </Ul>
      <TitleLi>Орфография</TitleLi>
      <Ul>
        <li>
          <ListLink to="/admin/orthography">Страницы</ListLink>
        </li>
        <li>
          <ListLink to="/admin/orthography/basket">Корзина</ListLink>
        </li>
        <li>
          <ListLink to="/admin/orthography/settings">Настройки</ListLink>
        </li>
      </Ul>
      <ListLink to="/admin/settings">Настройки</ListLink>
    </Menu>
  );
};

export default LeftMenuPresenter;
