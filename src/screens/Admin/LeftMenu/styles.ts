import styled from "@emotion/styled";
import { Link } from "react-router-dom";

export const Menu = styled.div`
  min-width: 200px;
  background: #edfbed;
  padding: 20px 0;
`;

export const ListLink = styled(Link)`
  color: #333;
  text-decoration: none;
  font-size: 18px;
  padding: 2px 5px;
  display: block;
  transition: 0.4s;
  color: #333;
  padding: 3px 3px 3px 30px;
  justify-content: start;

  &:hover {
    // background: #edfbed;
    color: #555;
    box-shadow: 0px 0px 2px #ddd;
  }
  ${(props: { active?: boolean | number }) => {
    if (props.active) {
      return "background: #edfbed";
    }
  }}
`;

export const Ul = styled.ul`
  margin: 0;
  padding: 10px 0;
  background: #f2f9f2;
`;

export const TitleLi = styled.div`
  padding: 0;
  font-size: 20px;
  padding-left: 20px;
`;
