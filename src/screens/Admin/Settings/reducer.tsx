import { createSlice, PayloadAction, createAction } from "@reduxjs/toolkit";
import { SettingsTypeSlice, ContactsType } from "./types";
import { Loading } from "@components/types";

export const defaultState: SettingsTypeSlice = {
  data: {
    phone: "",
    email: "",
    whatsapp: "",
    sendto: "",
  },
  saving: Loading.no,
  loading: Loading.no,
  sitemapLoading: Loading.no,
};

export const init = createAction("init");
export const save = createAction("save");
export const generateSitemap = createAction("generateSitemap");

export const grammarDetailSlice = createSlice({
  name: "grammarDetail",
  initialState: defaultState,
  reducers: {
    setData: (state, { payload }: PayloadAction<ContactsType>) => {
      const data: ContactsType = payload;
      //for (const key in data) {
      //if (data[key] === null) {
      //  data[key] = "";
      //}
      //}
      return { ...state, data };
    },
    setLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, loading: payload };
    },
    setSitemapLoading: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, sitemapLoading: payload };
    },
    setSaving: (state, { payload }: PayloadAction<Loading>) => {
      return { ...state, saving: payload };
    },
  },
});
export const { actions, reducer } = grammarDetailSlice;
