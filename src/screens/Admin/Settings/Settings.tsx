import React, { Component } from "react";
import { connect } from "react-redux";
import { reducerType } from "@store/reducers";
import Presenter from "./Presenter";
import { init, save, generateSitemap } from "./reducer";
import { ContactsType } from "./types";
import { Loading } from "@components/types";
import { Page } from "@components/Elements/elements";

interface SettingsProps {
  data: ContactsType;
  loading: Loading;
  saving: Loading;
  sitemapLoading: Loading;
  generateSitemap: () => void;
  init: () => void;
  save: () => void;
}

export class Settings extends Component<SettingsProps> {
  _isMounted = false;
  async componentDidMount() {
    this.props.init();
    this._isMounted = true;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    if (this._isMounted && this.props.loading === Loading.ok) {
      return (
        <Presenter
          data={this.props.data}
          //save={this.props.save}
          generateSitemap={this.props.generateSitemap}
          loading={this.props.loading}
          saving={this.props.saving}
          sitemapLoading={this.props.sitemapLoading}
        />
      );
    }
    return <Page loading={this.props.loading} />;
  }
}
const mapStateToProps = (state: reducerType) => {
  const { data, loading, saving, sitemapLoading } = state.settingsReducer;
  return {
    data,
    loading,
    saving,
    sitemapLoading,
  };
};

const mapDispatchToProps = {
  init,
  save,
  generateSitemap,
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
