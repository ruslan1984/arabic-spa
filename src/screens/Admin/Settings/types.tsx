import { Loading } from "@components/types";

export type ContactsType = {
  phone?: string;
  email?: string;
  whatsapp?: string;
  sendto?: string;
};

export type SettingsTypeSlice = {
  data: ContactsType;
  loading: Loading;
  saving: Loading;
  sitemapLoading: Loading;
};
