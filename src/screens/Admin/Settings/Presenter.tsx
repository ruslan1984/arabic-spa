import React, { FC } from "react";
import { ContactsType } from "./types";
import { Loading } from "@components/types";
import { Button, Page } from "@components/Elements/elements";

interface ContactsProps {
  data: ContactsType;
  //save: (data: any) => void;
  loading: Loading;
  saving: Loading;
  sitemapLoading: Loading;
  generateSitemap: () => void;
}

export const Presenter: FC<ContactsProps> = (props: ContactsProps) => {
  return (
    <Page loading={props.loading}>
      <h1>Контакты</h1>
      <hr />
      {/*<Formik
        enableReinitialize={true}
        initialValues={{ ...props.data }}
        onSubmit={(values: any) => {
          props.save(values);
        }}
      >
        <Form>
          <Line>
            <Label>Телефон</Label>
            <FormikField name="phone" placeholder="Телефон" />
          </Line>
          <Line>
            <Label>Email</Label>
            <FormikField name="email" placeholder="Email" type="text" />
          </Line>
          <Line>
            <Label>WhatsApp</Label>
            <FormikField name="whatsapp" placeholder="WhatsApp" />
          </Line>
          <Line>
            <Label>Отправлять на Email</Label>
            <FormikField name="sendto" placeholder="Отправлять на Email" />
          </Line>
          <Button type="submit" saving={props.saving}>
            Сохранить
          </Button>
        </Form>
      </Formik>*/}
      <Button saving={props.sitemapLoading} onClick={props.generateSitemap}>
        Sitemap
      </Button>
    </Page>
  );
};
export default Presenter;
