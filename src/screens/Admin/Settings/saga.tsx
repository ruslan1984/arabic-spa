import { takeEvery, put } from "redux-saga/effects";
//import { fetchGet, fetchPut, fetchOk } from "@helpers/fetch";
import { init, save, generateSitemap, actions } from "./reducer";
import { Loading } from "@components/types";

export function* initData() {
  try {
    yield put(actions.setLoading(Loading.start));
    //const fetchData = yield call(async () => await fetchGet(`/api/contacts`));
    //if (fetchOk(fetchData.status)) {
    //  yield put(actions.setData(fetchData.data));
    //} else {
    //  alert("error");
    //}
  } finally {
    yield put(actions.setLoading(Loading.ok));
  }
}

export function* saveData() {
  try {
    yield put(actions.setSaving(Loading.start));
    //const updateData = data.payload;
    //const fetchData = yield call(
    //  async () => await fetchPut("/api/contacts/1", updateData)
    //);
    //if (!fetchOk(fetchData.status)) {
    //  alert("error");
    //}
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSaving(Loading.ok));
  }
}
export function* sitemap() {
  yield put(actions.setSitemapLoading(Loading.start));
  try {
    //const fetchData = yield call(async () => await fetchGet(`/api/sitemap`));
    //!fetchOk(fetchData.status) && alert("error");
  } catch (err) {
    console.error(err);
  } finally {
    yield put(actions.setSitemapLoading(Loading.ok));
  }
}

export function* settingsSaga() {
  yield takeEvery(init, initData);
  yield takeEvery(save, saveData);
  yield takeEvery(generateSitemap, sitemap);
}
