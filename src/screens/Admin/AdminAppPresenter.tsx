import React, { FC } from "react";
import Header from "@/screens/Admin/Header";
import LeftMenuPresenter from "./LeftMenu/LeftMenuPresenter";
import { MainPage } from "@components/Elements/elements";

export const AdminAppPresenter: FC = () => {
  return (
    <>
      <Header />
      <MainPage>
        <LeftMenuPresenter />
      </MainPage>
    </>
  );
};
export default AdminAppPresenter;
