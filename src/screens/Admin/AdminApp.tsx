import React from "react";
import { AdminAppPresenter } from "./AdminAppPresenter";
import { ErrorBoundary } from "@components/ErrorBoundary";

export const AdminApp = () => {
  return (
    <ErrorBoundary>
      <AdminAppPresenter />
    </ErrorBoundary>
  );
};

export default AdminApp;
