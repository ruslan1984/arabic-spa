import styled from "@emotion/styled";

export const BtnBlock = styled.div`
  position: relative;
  width: 26px;
`;

export const Line = styled.div`
  height: 4px;
  background: #333;
  margin: 3px 0;
  transition: 0.4s;
  border-radius: 5px;
  &.openLine1 {
    transform: rotate(45deg) translate(5px, 5px);
  }
  &.openLine2 {
    opacity: 0;
  }
  &.openLine3 {
    transform: rotate(-45deg) translate(5px, -5px);
  }
`;
