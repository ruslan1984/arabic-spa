import React, { FC, memo } from "react";
import cn from "classnames";
import { Line, BtnBlock } from "./elements";

interface IMenuBtn {
  open: Boolean;
  onClick: () => void;
}

const MenuBtn: FC<IMenuBtn> = ({ open, onClick }) => {
  return (
    <BtnBlock className={cn({ open })} onClick={onClick}>
      <Line className={cn({ openLine1: open })} />
      <Line className={cn({ openLine2: open })} />
      <Line className={cn({ openLine3: open })} />
    </BtnBlock>
  );
};

export default memo(MenuBtn);
