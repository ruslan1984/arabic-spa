import React, {
  FC,
  useMemo,
  useState,
  useCallback,
  memo,
  useEffect,
  MouseEvent,
} from "react";
import Link from "next/link";
import { MobMenu, Menu, Li, MenuTitle, Ul } from "./styles";
import MenuBtn from "./MenuBtn";
import cn from "classnames";
import { useRouter } from "next/router";
import { TMobileMenu } from "@site/types";

interface IMobileMenu {
  menu: TMobileMenu[];
}

const MobileMenu: FC<IMobileMenu> = ({ menu }: IMobileMenu) => {
  const [open, setOpen] = useState(false);
  const [codes, setCodes] = useState(["/"]);
  const menuBtnClick = useCallback(() => {
    setOpen(!open);
  }, [open]);

  const router = useRouter();
  const myMenu = useMemo(
    () => ({
      id: 0,
      sort: 0,
      name: "",
      code: "",
      list: menu,
    }),
    [menu]
  );

  useEffect(() => {
    const paths = router.asPath.split("/");
    setCodes(paths);
  }, [router]);

  const menuItemClick = useCallback(() => {
    setOpen(false);
  }, [open]);

  const menuTitleClick = useCallback((e: MouseEvent<HTMLElement>) => {
    (e.target as HTMLElement).classList.toggle("open");
  }, []);

  const muiltiMenu = (
    menuItem: TMobileMenu,
    currentCode: string,
    level = 0
  ): JSX.Element => (
    <>
      {menuItem?.list ? (
        <>
          <MenuTitle
            className={cn({
              open: codes.includes(menuItem.code) || menuItem.code === "/",
            })}
            onClick={menuTitleClick}
          >
            {menuItem.name}
          </MenuTitle>
          <Ul>
            {menuItem.list?.map((subItem) => (
              <React.Fragment key={`${currentCode}/${subItem.code}`}>
                {muiltiMenu(
                  subItem,
                  `${currentCode}/${subItem.code}`,
                  level + 1
                )}
              </React.Fragment>
            ))}
          </Ul>
        </>
      ) : (
        <>
          {level > 1 ? (
            <Link href={currentCode}>
              <a>
                <Li
                  className={cn({ active: currentCode === router.asPath })}
                  onClick={menuItemClick}
                >
                  {menuItem.name}
                </Li>
              </a>
            </Link>
          ) : (
            <MenuTitle
              onClick={menuItemClick}
              className={cn({
                active: codes.includes(menuItem.code) || menuItem.code === "/",
              })}
            >
              <Link href={currentCode}>
                <a>{menuItem.name}</a>
              </Link>
            </MenuTitle>
          )}
        </>
      )}
    </>
  );

  return (
    <MobMenu>
      <MenuBtn open={open} onClick={menuBtnClick} />
      <Menu className={cn({ active: open })}>{muiltiMenu(myMenu, "", 0)}</Menu>
    </MobMenu>
  );
};

export default memo(MobileMenu);
