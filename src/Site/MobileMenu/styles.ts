import styled from "@emotion/styled";
import { tabletLg } from "@site/media";

export const MobMenu = styled.div`
  @media (min-width: ${tabletLg(+1)}) {
    display: none;
  }
`;

export const Menu = styled.div`
  background: var(--main-bg);
  position: fixed;
  left: 0;
  top: 35px;
  width: 100%;
  height: 0;
  z-index: 2500;
  transition: 0.5s;
  max-height: 0;
  overflow: hidden;
  &.active {
    height: calc(100% - 35px);
    max-height: max-content;
    overflow-y: scroll;
    padding-bottom: 50px;
  }
`;

export const Ul = styled.ul`
  max-height: 0;
  overflow: hidden;
  transition: 1s;
  box-shadow: inset 1px 3px 10px 0px #cfcfcf;
  // &:first-child {
  //   padding-top: 1rem;
  // }
`;

export const Li = styled.li`
  padding: 4px 2px 4px 20px;
  font-size: 1.2rem;
  background: rgb(255 177 51 / 7%);
  &.active {
    background: rgb(255 181 62 / 21%);
  }
  &:nth-child(1) {
    padding-top: 1rem;
  }
`;

export const MenuTitle = styled.div`
  padding: 5px 2px 2px 10px;
  font-size: 1.4rem;
  cursor: pointer;
  &.open + ul {
    max-height: max-content;
  }
  &.active {
    background: #fff7ea;
  }
`;

export const MenuHeader = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Title = styled.div`
  a {
    font-family: "Poiret One", cursive;
    font-size: 1.5rem;
    padding: 5px;
  }
`;
