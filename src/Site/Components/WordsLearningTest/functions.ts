import { QuestionType, TAnswer } from "@modules/Test/Testing/types";
import { TTestWord } from "./types";

export const generateTest = (words: TTestWord[]): QuestionType[] => {
  if (!words || words.length === 0) return [];

  const questions: QuestionType[] = words.map((item, index) => {
    let answer: TAnswer = {
      text: item.translate,
      trueAnswer: true,
    };
    const falseAnswersIndexes = getRand(words.length, index);
    const falseAnswers = falseAnswersIndexes.map((itemIndex) => ({
      text: words[itemIndex].translate,
      trueAnswer: false,
    }));
    return {
      questionText: item.word,
      answers: [answer, ...falseAnswers],
      mixAnswers: true,
    };
  });
  const randQuestions = questions.sort(() => Math.random() - 0.5);
  return randQuestions;
};

const getRand = (
  max: number,
  currentIndex: number,
  countNumbers = 3
): number[] => {
  if (max < countNumbers) return [];
  const arr: number[] = [];
  let i = 100;
  while (arr.length < countNumbers && i > 0) {
    const newNumber = Number(Math.floor(Math.random() * max));
    if (newNumber !== currentIndex && !arr.includes(newNumber)) {
      arr.push(newNumber);
    }
    i--;
  }
  return arr;
};
