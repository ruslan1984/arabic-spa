import React, { FC, useMemo } from "react";
import { useState, MouseEvent } from "react";
import { TTestWord } from "./types";
import { generateTest } from "./functions";
import Presenter from "./Presenter";

interface IWordsLearningTest {
  words: TTestWord[];
}

const WordsLearningTest: FC<IWordsLearningTest> = ({
  words,
}: IWordsLearningTest) => {
  const [isOpen, setIsOpen] = useState(false);
  const questions = generateTest(words);
  const openClick = () => {
    setIsOpen(true);
  };

  const modalClick = (e: MouseEvent<HTMLElement>) => {
    const className = (e.target as HTMLElement).className;
    if (!className) return;
    if (className.indexOf("closeWordsTestModal") >= 0) {
      setIsOpen(false);
    }
  };

  const disabled = useMemo(() => questions.length < 2, [questions]);

  return (
    <Presenter
      isOpen={isOpen}
      disabled={disabled}
      modalClick={modalClick}
      openClick={openClick}
      questions={questions}
    />
  );
};

export default WordsLearningTest;
export type { TTestWord };
