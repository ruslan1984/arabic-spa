import React, { FC, MouseEvent, memo } from "react";
import Testing from "@modules/Test/Testing";
import { Modal, CloseBtn, BtnBlock } from "./styles";
import { TestButton } from "@modules/Test/Testing/styles";
import { QuestionType } from "@modules/Test/Testing/types";
import QuestionTitleCustom from "./Components/QuestionTitleCustom";
import AnswerTextCustom from "./Components/AnswerTextCustom";

interface IPresenter {
  isOpen: boolean;
  questions: QuestionType[];
  disabled?: boolean;
  modalClick: (e: MouseEvent<HTMLElement>) => void;
  openClick: () => void;
}

const Presenter: FC<IPresenter> = ({
  isOpen,
  questions,
  disabled,
  modalClick,
  openClick,
}: IPresenter) => {
  return (
    <>
      <Modal
        className="closeWordsTestModal"
        isOpen={isOpen}
        onClick={modalClick}
      >
        <CloseBtn className="closeWordsTestModal" />
        <Testing
          isOpen={isOpen}
          hideHeader
          isRadio={true}
          data={questions}
          QuestionTitleCustom={QuestionTitleCustom}
          AnswerTextCustom={AnswerTextCustom}
        />
      </Modal>

      <BtnBlock>
        <TestButton disabled={disabled} onClick={openClick}>
          Заучивать слова
        </TestButton>
      </BtnBlock>
    </>
  );
};

export default memo(
  Presenter,
  (prevProps, nextProps) =>
    prevProps.isOpen === nextProps.isOpen &&
    prevProps.questions === nextProps.questions
);
