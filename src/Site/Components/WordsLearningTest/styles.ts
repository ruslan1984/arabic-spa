import styled from "@emotion/styled";
import { closeIcon } from "@/components/Elements/icons";

export const Modal = styled.div`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1001;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  transition: .5s;
  opacity: 0;
  pointer-events: none;
  flex-direction: column;
  &:after {
    content: "";
    width: 100%;
    height: 100%;
    background: #111;
    opacity: 0.9;
    position: fixed;
  }
  ${({ isOpen }: { isOpen?: boolean }) =>
    isOpen && "pointer-events: auto; opacity: 1;"}
};
`;

export const CloseBtn = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  position: relative;
  z-index: 1000;
  cursor: pointer;
  background: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 5px;
  background-image: url(${closeIcon});
  background-size: 12px;
  background-repeat: no-repeat;
  background-position: center;
`;

export const BtnBlock = styled.div`
  text-align: center;
`;
