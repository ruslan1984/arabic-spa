import styled from "@emotion/styled";

export const AnswerText = styled.div`
  font-size: 1.5rem;
  margin-left: 15px;
`;
