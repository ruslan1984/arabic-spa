import React, { FC } from "react";
import { AnswerText } from "./styles";

const AnswerTextCustom: FC<{ children: string | JSX.Element }> = ({
  children,
}: {
  children: string | JSX.Element;
}) => <AnswerText>{children}</AnswerText>;

export default AnswerTextCustom;
