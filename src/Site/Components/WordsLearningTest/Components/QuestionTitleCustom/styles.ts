import styled from "@emotion/styled";

export const QuestionText = styled.div`
  text-align: center;
  font-size: 1.5rem;
  margin-top: 15px;
  padding-bottom: 5px;
  margin-bottom: 15px;
  border-bottom: 1px solid #ddd;
`;
