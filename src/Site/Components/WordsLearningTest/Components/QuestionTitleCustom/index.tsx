import React, { FC } from "react";
import { QuestionText } from "./styles";

const QuestionTitleCustom: FC<{ children: string | JSX.Element }> = ({
  children,
}: {
  children: string | JSX.Element;
}) => <QuestionText>{children}</QuestionText>;

export default QuestionTitleCustom;
