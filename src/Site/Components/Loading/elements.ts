import styled from "@emotion/styled";
import { keyframes } from "@emotion/react";

const loadingAnimation = keyframes`
	to { 
		transform: rotate(360deg) 
	}
`;

const loadingStyle = `
	position: fixed;
	width: 100%;
	height: 100%;
	left: 0;
	top: 0;
	z-index:100;
	background-color: var(--main-bg);
	width: 100%;
	height: 100%;
	left: 0;
	top: 0;
	z-index:100;
	//&:before{
	//	content: "";
	//	background: var(--main-bg);
	//	width: 100%;
	//	height: 100%;
	//	left: 0;
	//	top: 0;
	//	z-index:100;
	//	position: fixed;
	//}
//	&:after {
//		//animation: ${loadingAnimation} 3s infinite linear;
//		width: 100%;
//		height: 100%;
//		left: 0;
//		top: 0;
//		content: "";
//		position: absolute;
//		z-index:101;
//		background-position: center;
//		background-repeat: no-repeat;
//		background-size: 100px;
//		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABUCAYAAAAcaxDBAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAB2AAAAdgB+lymcgAACvhJREFUeJztnHlwU8cdx2kmoXebNmnS9Mi000kmbZpOMnTaJEw5WoLvA+uyDcYGjC9dNnhIoaQIMI6ZYOuWD9k6nEIa0iYp7TCdDMQFC2Osd+h4DwIxDQk0pYHQNGAOW3rbXUnv+ZmiARKhlen7zOwflp/ffvenPX6/3d96xgwJCQkJCQkJCQkJCQkJCQkJCQkJCQmJ6U4N0f3AyiM9DxuA4U70M2ANMyeIzW0c2fI+R2z8Z9SnpsFOw5fQ7+Azd6Bnq4d/9x28qjOQqgHD57SM5xUd6wWoFFCmC2V01zMTxKbNHLUFCGVfDZgY1A3XHnY/BJ8Lo2drmT5QTJtPFg8Y7sbdjoxBw3o38MZEJZ8ygQLC+DFHbzkuGNP/W2jQlbDUT+gZj49/tp5xg4VEO5BR1gHc7cBCXcD7bWiInTrGfboy0HO0hLKs17Lu/byBUI9DBkLlHLFlNGZMwgC4wYaEQRsu61hPlH9+SbAr9mw+YfqPju3N1jH9I+jdioAtVH7EMQt3e28pBoPhTi3rpXhjrAg5QRY0xopQ75CO8YLqUC/IJ00xA2WTHVxkuLmfG1QnDJkoQ02v6hjPhxrGA5YGu2N/j55fRJnfh9PGOP9uecAOjWy8qGB3zsTd7ltGI9vzuHholwc6E8awjGZTRo7vmagoKNsrAC48YKjxZW6/+gIs58FQ03b0mYy0/DGL6BCezYZlebD3T8K7mfi0gX6n9NvLcLc7pShO7vy89rBrQW2wN7uS6Z3NN7o67ES9MDH/2Q7IaXMRNGwALjBvKSn7c9d7r5KybYa98tiigJWU0eZcaMTn0Xu1rAeUBhyCsZWUsayOdf2yPuTNKQ85vpaONt8yYqsx038cNVQDG5pPdVyBPe9sIWUWGox6mpJ2FH/aupbQ1nkltCWaRxqFd+eT5jHtYe9R3tC5hGkceQ+paBsW9KxnN98jkUGR8Yop8weFpPndHMIYQSu5nLKvTlV9Csr+6wLKfD7Xb4oUUeZ3GpjePXz9yKBZcEQUkOYPUlVf2oEL0Fm+QWgBQb0m12+MpK1+xnuSr78y1BMfEXCxS1f9Kac+7Bqpg37iYrj4ZCXmyyLK9GG66lez7n3IT0WuVXZiESugjR+nq/6U0DhgvBsuQut1YY+rnLa/mU1Ortyod5TRtsp0aVHRtuqF5KT3gIy6NNizA2mrDHV1Fxy1PpIuLZ+INW/1fVnDeN/mh5kK+oJozpRRlt1wJX+9gjD9PN2aSvzW2XLa8hdFwL6rNuwa4LVVwSkAGniidF/7d9Ot6YbRhTxqsZ9ZQlljPUNOOxS4tenD3p9M8YGDcR9YDv1d3NqSomFdLbzgleFesDAx3JW0ZR12bUx/ljj+z024V9D/HcStLSnLQs4itJor4VDnI5ksOIcVE5Yf4NZWc3THvVDbJTQN8QFF7MsmbWrc2pJSEey8TxawDWcJi5CRUwVsG3Hr4pHT9g25VEc0HlC0g1LSsbeKtX8Tt67/4Vr7mUh8Ju5RzoYL5xKmu1rDukNIax3aT6XMJ/MyKSzVM66N4gk/jzRBf898DreuZMAv/01ea0NiPxUuoPtw6xLQsN6ha+1nKoi2r+LWdjU5b1s+CyO4CK+3IhTfT0UhK25tAnD4vBbfQZrcz8wh26O4dSUDaj2jRb4oXED5CKqItPwDty4BFJHkiFZOfj8Tt65kIN8zS6QVeSSqEccy3LqmYKXbm7eSHe9qyI4TioAlY1b2ZCgpa+ty0nxiG93xTj/Ztga3HgEAwGciRIubP0yL+vQRMKTvxK3regCyZTFHbrkY0z2yFnA+7SgwxI+v8QobaVFNOeo90Bg77gXDtQ/h1pYMsGftPRzZMiZoPrQudl4FDjSacGubMUG3GgVh5CbA7a+NizvU1IRbWzKA3zBvshO0wE6gSxwA6knc2qC4zU0cuRkOm/Wio15o0EF12neWbhRAtD3IkRujHLEBGrFxUvOBppdwa5sBdmu/Ed1fd2nKUa9PdxC3ruvBDemIqZrrx8BrjZkR2YE/r773ysHVL509uPoofWjjq6V0z9O4NV2P5qDz++ERw64zQ82j532rfg92G76CW5OAnu1/UMN43ptMpTFGS2kb9i27ZKCtPBh+jsVDTxc6DY1Av/kp3LoE9IzbLY7lcwgjFGkcx60rGeJThbqwi888OYZblwD8tg8JRwvBnoyO5WM7Y6yXE45p6HhSRB5lvIBbm0BN0LUdxfEotYYP6XIp02XcupJRE3aOorwqlP/Ef/nFlJnFrUtA5rc8kUOYxkWxMZfJO+Eq2tYoPg3NozquqPydP8atawoKtvfrStq+tSrg3KWGc5SW9VwqDdiPKwhHHm5tPI1k/w/RXijStjjYdbqEsvpVtL0Facet7Zo0BXoe1THeK+L5KYfomMgEwbGkNdb7d/FcnxXbFbMW4taWFE3I/Zx4tS8gzYmtPMcq3Noaaec8sTZlYv5cRFr34taWFHXI+2w8Mcs7JaUQ7Zfi1qajX3xanOTAH3GjJAjc2pKyONT9WDFpnkB+qJBSSJnGGhjvfPWx3jk1RPcX0q3JwO6c2Xik76nqcNczMsp6Nl+U7ohSg3Bks9wUZSOOJShlMI/suALdkuOaxLyFUrhzSdOl8oDlZ+nSgq7c8LdF0KgpJMzjiyjTKaStkDKdVoXwZ7TcFFrGvUvID2Xi+aFFpPW9tNXPel6ezA/1xhIcCijz6XTVn3LQYVim5IdW3Q75oRrWQ6gZN6gQ5WcWUeZ/aw579Tqm/w9VQed2FWl5MlX1oSGuD7mN6N1llMPZEHYdQOfu6Mvk028KM+m4+GaRB2zPim9rIL8PJeHyvWY5DP9yyfaoguh+7NPWpQ8779cz3n+Jr9UUU9Zz2VedbkJ3KWNPEm4IaKw8GW0dkFPWNyopZ5nYF6xITAPygGVvc/CFL+rCfUrtkf6VN7Lyoi1DGERULw86V6CcqliSr+jdRZQl9u4yv3XNItKyV0ba9sB6FqajzWmjPuh8km+wOpZSaEr4gjYGhoSjk7lRxqjCb/4N+hvgb50dIbd0RUiDCww3NaDPYqmJjPdCPDfJDXJI0zhc9HZcPV9mig98y0Dh39JA90fI6RcnRkCjHBL3LpS7meNvnwBEazFHtUSFA7X9NQD49M+LQ8jaxH6miu48hXLp4WgQ3otug2RC2HtLUQQ763MJY4SfUxWk9a/iy7DL4LzKG2Scag0IxiQ2xc5+ooMNF68VQqIrOmho89uHOf6OCZXfXou7vWlhFtF9Vwljns/3ntpwn7cm3Be7LcKvxuiSFuydpwSDDjUlDtRqIyuZ3hNo/1URsAnGl5HWEHpXWdh0v2LEMXcGAHfgbSVGFCP2x9GRiTgkVAatzdzIut3c8BrA+TSiy7O6Y/KgY6149Ub7mYrhrh/hbkdGsfSI7R5lwLZNHrT2yAKWJ9BngFj9COdr+Egw5qD6HBhUP4x+Vx4yz5IFbH1yytF228+TqWaHLu+yV5ubsUcq047WkjmXUMGt47ZBMmgKsZb+asU2+VyurWQu2Caf34dbz7Smu2bWXdbFC8Ys5QvA8yVzADIq/Cztm9S3DZ0VC++DBgWoIIOiYlH8QnKPPimSQVPMi40lD7y+TgFQ+dtWZaz0G/IyKyFhOnHmDcO3Jv+rWCIp1tf0KG5d0xZw0PA98b9pixt0Vcp2+P/vAJThp4JBB+v5HlqOW9e0BfhbyiZvl2gz56bGdAX4W+UcuSUSu6nh08av6/hWvYBbl4SEhESC/wJuJW5HWKQZ+wAAAABJRU5ErkJggg==');
//}
`;

export const LoadingFon = styled.div`
  ${(props: { loaded?: boolean }) => {
    if (!props.loaded) {
      return loadingStyle;
    }
  }};
`;
