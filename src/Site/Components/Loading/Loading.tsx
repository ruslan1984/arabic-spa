import React, { FC, useEffect, useState } from "react";
import { addListener } from "process";
import { LoadingFon } from "./elements";

export const Loading: FC = () => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.onload = function () {
        setLoaded(true);
      };
    }
  });

  return (
    <>
      <LoadingFon loaded={loaded} />
    </>
  );
};

export default Loading;
