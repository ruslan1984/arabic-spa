import React, { FC, useEffect, useState } from "react";
import { TDictionary } from "./types";
import DictionaryList from "./DictionaryList";
import DictionaryProvider from "./context";
import Pagination from "@components/Pagination";
import { getPagesCount, getPageData, saveData, removeData } from "./ls";

const MyDictionary: FC = () => {
  const [pagesCount, setPagesCount] = useState(0);
  const [page, setPage] = useState(1);
  const [dictionaryList, setCurrentDictionaryList] = useState<TDictionary[]>(
    []
  );

  useEffect(() => {
    const count = getPagesCount();
    setPagesCount(count || 0);
  }, [dictionaryList]);

  useEffect(() => {
    const current = getPageData<TDictionary>(page);
    setCurrentDictionaryList(current);
  }, [page]);

  const save = (newData: TDictionary) => {
    saveData<TDictionary>(newData);
    const count = getPagesCount();
    setPagesCount(count || 0);
  };

  const remove = (dictionaryId: number) => {
    removeData<TDictionary>(dictionaryId);
    const count = getPagesCount();
    setPagesCount(count || 0);
  };

  const pageClick = (page: number) => {
    setPage(page);
  };

  return (
    <>
      <DictionaryProvider initData={dictionaryList} remove={remove} save={save}>
        <DictionaryList />
      </DictionaryProvider>
      <Pagination
        pagesCount={pagesCount}
        currentPage={page}
        pageClick={pageClick}
      />
    </>
  );
};

export default MyDictionary;
