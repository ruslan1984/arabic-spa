export type { TTestWord } from "@site/types";
import { TTestWord } from "@site/types";

export type TWordLearning = {
  id: number;
  hide?: boolean;
} & TTestWord;

export type TDictionary = {
  id: number;
  dictionaryName: string;
  words: TWordLearning[];
  mode?: TMode;
};

export type TMode = "edit" | "show";
