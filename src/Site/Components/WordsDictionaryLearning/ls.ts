const filedName = "words";
const wordsCount = 3;

function getWords<T>(): T[] {
  const wordsStr = localStorage.getItem(filedName) || "";
  if (!wordsStr) return [];
  return JSON.parse(wordsStr) || [];
}
function saveWords<T>(data: T[]): void {
  const stringNewDictionaryStr = JSON.stringify(data);
  localStorage.setItem(filedName, stringNewDictionaryStr);
}

export const add = <T>(newData: T): void => {
  const data: T[] = getWords<T>();
  const newDataList = [newData, ...data];
  saveWords<T>(newDataList);
};

export const getPageData = <T>(page: number): T[] => {
  const data: T[] = getWords<T>();
  const start = (page - 1) * wordsCount;
  const end = (page - 1) * wordsCount + wordsCount;
  return data?.slice(start, end) || [];
};

export const saveData = <T extends { id: number }>(newData: T): void => {
  const data: T[] = getWords<T>();
  const exist = data.some(({ id }) => id === newData.id);
  let newList;
  if (exist) {
    newList = data.map((item) => (item.id === newData.id ? newData : item));
  } else {
    newList = [newData, ...data];
  }
  saveWords<T>(newList);
};

export const removeData = <T extends { id: number }>(id: number): void => {
  const data: T[] = getWords<T>();
  const newList = data.filter((item) => item.id !== id);
  saveWords<T>(newList);
};

export const getPagesCount = (): number => {
  const data = getWords();
  if (data.length < 1) return 0;
  return Math.ceil((data?.length || 1) / wordsCount);
};
