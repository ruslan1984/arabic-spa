import React, {
  FC,
  memo,
  useEffect,
  useContext,
  createContext,
  useState,
} from "react";
import { TDictionary, TMode } from "./types";
import { getMaxId } from "@helpers/functions";

type TDictionaryContext = {
  dictionaryList?: TDictionary[];
  setDictionaryMode: (dictionaryId: number, mode: TMode) => void;
  saveDictionary: (newData: TDictionary) => void;
  hideDictionaryWord: (dictionaryId: number, wordId: number) => void;
  removeDictionary: (dictionaryId: number) => void;
};

const DictionaryContext = createContext<TDictionaryContext>({
  dictionaryList: [],
  setDictionaryMode: () => {},
  saveDictionary: () => {},
  hideDictionaryWord: () => {},
  removeDictionary: () => {},
});

export const useDictionadyContext = (): TDictionaryContext =>
  useContext(DictionaryContext);

interface IDictionary {
  children: JSX.Element;
  initData: TDictionary[];
  save?: (newData: TDictionary) => void;
  remove?: (dictionaryId: number) => void;
}

const DictionaryProvider: FC<IDictionary> = ({
  children,
  initData,
  save,
  remove,
}: IDictionary): JSX.Element => {
  const [data, setData] = useState<TDictionary[]>(initData);

  useEffect(() => {
    setData(initData || []);
  }, [initData]);

  const saveDictionary = (newData: TDictionary) => {
    newData = { ...newData, mode: "show" };

    let newList;
    if (newData.id < 0) {
      const id = getMaxId(data) + 1;
      newData = { ...newData, id };
      newList = [newData, ...data];
    } else {
      newList = data.map((item) => (item.id === newData.id ? newData : item));
    }
    setData(newList);
    save && save(newData);
  };

  const hideDictionaryWord = (dictionaryId: number, wordId: number) => {
    const newList = data.map((item) =>
      item.id === dictionaryId
        ? {
            ...item,
            words: item.words.map((word) =>
              word.id === wordId ? { ...word, hide: !Boolean(word.hide) } : word
            ),
          }
        : item
    );

    setData(newList);
    const newData = newList.find(({ id }) => id === dictionaryId);
    if (!newData) return;
    save && save(newData);
  };

  const removeDictionary = (dictionaryId: number) => {
    const newList = data.filter((item) => item.id !== dictionaryId);
    setData(newList);
    remove && remove(dictionaryId);
  };

  const setDictionaryMode = (dictionaryId: number, mode: TMode) => {
    const newList = data.map((item) =>
      item.id === dictionaryId ? { ...item, mode } : item
    );
    setData(newList);
  };

  return (
    <DictionaryContext.Provider
      value={{
        dictionaryList: data,
        setDictionaryMode,
        saveDictionary,
        hideDictionaryWord,
        removeDictionary,
      }}
    >
      {children}
    </DictionaryContext.Provider>
  );
};

export default memo(DictionaryProvider);
