import React from "react";
import ShowMode from "./Dictionary/ShowMode";
import EditMode from "./Dictionary/EditMode";
import Index from "./DictionaryList";
import { TWordLearning, TDictionary } from "./types";
import { variables } from "@site/variables";
import DictionaryProvider from "./context";

export default {
  title: "WordsLearning",
  //decorators: [withKnobs],
};

const words: TWordLearning[] = [
  { id: 1, word: "word", translate: "translate" },
  { id: 2, word: "word1", translate: "translate1" },
  { id: 3, word: "word3", translate: "translate3" },
  { id: 4, word: "word4", translate: "translate4" },
  { id: 5, word: "word5", translate: "translate5" },
  { id: 6, word: "word6", translate: "translate6" },
];

const dictionaryList: TDictionary[] = [
  {
    id: 1,
    dictionaryName: "Тестовый словарь 1",
    words: [
      { id: 1, word: "word", translate: "translate" },
      { id: 2, word: "word1", translate: "translate1" },
      { id: 3, word: "word3", translate: "translate3" },
      { id: 4, word: "word4", translate: "translate4" },
      { id: 5, word: "word5", translate: "translate5" },
      { id: 6, word: "word6", translate: "translate6" },
    ],
  },
  {
    id: 2,
    dictionaryName: "Тестовый словарь2",
    words: [
      { id: 1, word: "word", translate: "translate" },
      { id: 2, word: "word1", translate: "translate1" },
      { id: 3, word: "word3", translate: "translate3" },
      { id: 4, word: "word4", translate: "translate4" },
      { id: 5, word: "word5", translate: "translate5" },
      { id: 6, word: "word6", translate: "translate6" },
    ],
  },
  {
    id: 3,
    dictionaryName: "Тестовый словарь3",
    words: [
      { id: 1, word: "word", translate: "translate" },
      { id: 2, word: "word1", translate: "translate1" },
      { id: 3, word: "word3", translate: "translate3" },
      { id: 4, word: "word4", translate: "translate4" },
      { id: 5, word: "word5", translate: "translate5" },
      { id: 6, word: "word6", translate: "translate6" },
    ],
  },
  {
    id: 4,
    dictionaryName: "Тестовый словарь4",
    words: [
      { id: 1, word: "word", translate: "translate" },
      { id: 2, word: "word1", translate: "translate1" },
      { id: 3, word: "word3", translate: "translate3" },
      { id: 4, word: "word4", translate: "translate4" },
      { id: 5, word: "word5", translate: "translate5" },
      { id: 6, word: "word6", translate: "translate6" },
    ],
  },

  {
    id: 5,
    dictionaryName: "Тестовый словарь5",
    words: [
      { id: 1, word: "word", translate: "translate" },
      { id: 2, word: "word1", translate: "translate1" },
      { id: 3, word: "word3", translate: "translate3" },
      { id: 4, word: "word4", translate: "translate4" },
      { id: 5, word: "word5", translate: "translate5" },
      { id: 6, word: "word6", translate: "translate6" },
    ],
  },
  {
    id: 6,
    dictionaryName: "Тестовый словарь6",
    words: [
      { id: 1, word: "word", translate: "translate" },
      { id: 2, word: "word1", translate: "translate1" },
      { id: 3, word: "word3", translate: "translate3" },
      { id: 4, word: "word4", translate: "translate4" },
      { id: 5, word: "word5", translate: "translate5" },
      { id: 6, word: "word6", translate: "translate6" },
    ],
  },
  {
    id: 7,
    dictionaryName: "Тестовый словарь7",
    words: [
      { id: 1, word: "word", translate: "translate" },
      { id: 2, word: "word1", translate: "translate1" },
      { id: 3, word: "word3", translate: "translate3" },
      { id: 4, word: "word4", translate: "translate4" },
      { id: 5, word: "word5", translate: "translate5" },
      { id: 6, word: "word6", translate: "translate6" },
    ],
  },
];

export const Show = () => (
  <>
    <style>{variables}</style>
    <DictionaryProvider initData={dictionaryList}>
      <ShowMode
        dictionaryId={1}
        dictionaryName={"Тестовый словарь"}
        words={words}
      />
    </DictionaryProvider>
  </>
);

export const Edit = () => (
  <>
    <style>{variables}</style>
    <DictionaryProvider initData={dictionaryList}>
      <EditMode
        dictionaryId={1}
        dictionaryName={"Тестовый словарь"}
        words={words}
      />
    </DictionaryProvider>
  </>
);

export const List = () => (
  <>
    <style>{variables}</style>
    <DictionaryProvider initData={dictionaryList}>
      <Index />
    </DictionaryProvider>
  </>
);
