import React, { FC } from "react";
import { TWordLearning } from "../types";
import WordsLearningTest, {
  TTestWord,
} from "@site/Components/WordsLearningTest";

interface IWordsTest {
  words: TWordLearning[];
}

const WordsTest: FC<IWordsTest> = ({ words }: IWordsTest) => {
  const testWords: TTestWord[] = words
    .filter((item) => !item?.hide)
    .map(({ word, translate }) => ({ word, translate }));

  return <WordsLearningTest words={testWords} />;
};

export default WordsTest;
