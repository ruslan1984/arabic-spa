import styled from "@emotion/styled";

export const Header = styled.div`
  padding: 10px;
  text-align: right;
  margin-bottom: 10px;
  text-align: center;
`;

export const Container = styled.div`
  max-width: 800px;
  margin: 0 auto;
  min-width: 270px;
`;
