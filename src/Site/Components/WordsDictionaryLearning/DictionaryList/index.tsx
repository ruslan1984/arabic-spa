import React, { FC, useState } from "react";
import Presenter from "./Presenter";
import { useDictionadyContext } from "../context";

const DictionaryList: FC = () => {
  const [showAdd, setShowAdd] = useState(false);
  const { dictionaryList } = useDictionadyContext();

  const addDictionaryClick = () => {
    setShowAdd(true);
  };
  const onClose = () => {
    setShowAdd(false);
  };
  return (
    <>
      <Presenter
        showAdd={showAdd}
        addDictionary={addDictionaryClick}
        onClose={onClose}
        dictionaryList={dictionaryList || []}
      />
    </>
  );
};

export default DictionaryList;
