import React, { FC, memo } from "react";
import { TDictionary } from "../types";
import Dictionary from "../Dictionary";
import { Header, Container } from "../styles";
import { Button } from "@site/Components/Button";
import EditMode from "../Dictionary/EditMode";

interface IPresenter {
  dictionaryList: TDictionary[];
  showAdd: boolean;
  addDictionary: () => void;
  onClose?: () => void;
}

const Presenter: FC<IPresenter> = ({
  dictionaryList,
  showAdd,
  addDictionary,
  onClose,
}: IPresenter) => {
  const hideAddBtn = dictionaryList.some(({ id }) => id < 0);
  return (
    <Container>
      {dictionaryList.map(({ id, dictionaryName, words, mode }) => (
        <Dictionary
          key={id}
          id={id}
          mode={mode}
          dictionaryName={dictionaryName}
          words={words}
        />
      ))}
      {showAdd && (
        <EditMode
          onClose={onClose}
          dictionaryId={-1}
          dictionaryName={""}
          words={[
            {
              id: 1,
              word: "",
              translate: "",
            },
            {
              id: 2,
              word: "",
              translate: "",
            },
          ]}
        />
      )}
      {!showAdd && (
        <Header>
          <Button fullWidth onClick={addDictionary}>
            Добавить словарь
          </Button>
        </Header>
      )}
    </Container>
  );
};

export default memo(
  Presenter,
  (cur, next) =>
    cur.dictionaryList === next.dictionaryList && cur.showAdd === next.showAdd
);
