import styled from "@emotion/styled";
import { tabletXs } from "@site/media";

export const ButtonsBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: end;
  gap: 10px;
  margin-bottom: -10px;
  margin-top: 5px;
  @media (max-width: ${tabletXs()}) {
    gap: 20px;
  }
`;

export const WordLine = styled.div`
  display: flex;
  gap: 2px;
  border-bottom: 1px solid #eee;
  flex: 1;
`;

export const WordListBlock = styled.div`
  display: flex;
  align-items: baseline;
  gap: 15px;
  @media (max-width: ${tabletXs()}) {
    display: block;
  }
`;
export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 0;
  gap: 5px;
`;
export const AddWordBlock = styled.div`
  padding: 10px;
  text-align: right;
`;
