import React, { FC, useRef, useState, useEffect, ChangeEvent } from "react";
import Presenter from "./Presenter";
import { TWordLearning } from "../../types";
import { useDictionadyContext } from "../../context";
import { getMaxId } from "@helpers/functions";

interface IPresenter {
  dictionaryId: number;
  dictionaryName: string;
  words: TWordLearning[];
  onClose?: () => void;
}

const EditMode: FC<IPresenter> = ({
  words,
  dictionaryName,
  dictionaryId,
  onClose,
}: IPresenter) => {
  const { setDictionaryMode, saveDictionary, removeDictionary } =
    useDictionadyContext();
  const [currentWords, setCurrentWords] = useState(words);
  const [removeConfirm, setRemoveConfirm] = useState(false);
  const [dictionaryNameError, setDictionaryNameError] = useState("");

  useEffect(() => {
    if (removeConfirm) {
      setTimeout(() => {
        setRemoveConfirm(false);
      }, 5000);
    }
  }, [removeConfirm]);

  const dictionaryNameRef = useRef<HTMLInputElement>(null);

  dictionaryNameRef.current?.addEventListener("click", () => {
    setDictionaryNameError("");
  });

  const onSave = () => {
    const dictionaryName = dictionaryNameRef.current?.value || "";
    if (!dictionaryName.trim()) {
      setDictionaryNameError("Введите название словаря");
      return;
    }
    const words = currentWords.filter(
      ({ translate, word }) => translate.trim() && word.trim()
    );

    const newDictionary = {
      id: dictionaryId,
      dictionaryName,
      words,
    };
    saveDictionary(newDictionary);
    onClose && onClose();
  };

  const onCloseClick = () => {
    setDictionaryMode(dictionaryId, "show");
    onClose && onClose();
  };

  const onAddWord = () => {
    const maxId = getMaxId(currentWords);
    const newLine = {
      id: maxId + 1,
      word: "",
      translate: "",
    };
    setCurrentWords([...currentWords, newLine]);
  };

  const onRemoveWord = (id: number) => () => {
    const newWords = currentWords.filter((item) => item.id !== id);
    setCurrentWords(newWords);
  };

  const onRemoveDictionary = () => {
    if (removeConfirm) {
      removeDictionary(dictionaryId);
    } else {
      setRemoveConfirm(true);
    }
  };

  const updateWord = (id: number) => (e: ChangeEvent<HTMLInputElement>) => {
    const word = e.target.value;
    const newWords = currentWords.map((item) =>
      item.id === id ? { ...item, word } : item
    );
    setCurrentWords(newWords);
  };

  const updateTransalte =
    (id: number) => (e: ChangeEvent<HTMLInputElement>) => {
      const translate = e.target.value;
      const newWords = currentWords.map((item) =>
        item.id === id ? { ...item, translate } : item
      );
      setCurrentWords(newWords);
    };

  const onHide = (id: number) => () => {
    const newWords = currentWords.map((item) =>
      item.id === id ? { ...item, hide: !Boolean(item.hide) } : item
    );
    setCurrentWords(newWords);
  };

  return (
    <Presenter
      dictionaryName={dictionaryName}
      dictionaryNameRef={dictionaryNameRef}
      words={currentWords}
      removeConfirm={removeConfirm}
      dictionaryId={dictionaryId}
      onAddWord={onAddWord}
      onClose={onCloseClick}
      onSave={onSave}
      onHide={onHide}
      onRemoveWord={onRemoveWord}
      onRemoveDictionary={onRemoveDictionary}
      updateWord={updateWord}
      updateTransalte={updateTransalte}
      dictionaryNameError={dictionaryNameError}
    />
  );
};

export default EditMode;
