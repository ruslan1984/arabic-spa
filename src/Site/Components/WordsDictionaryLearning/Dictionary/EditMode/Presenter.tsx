import React, { FC, ChangeEvent, RefObject, memo } from "react";
import Input from "@components/Input";
import {
  Button,
  RoundButton,
  RemoveIcon,
  SecondaryButton,
  ErrorButton,
} from "@site/Components/Button";
import {
  ButtonsBlock,
  WordLine,
  WordListBlock,
  Footer,
  AddWordBlock,
} from "./styles";
import { TWordLearning } from "../../types";

interface IPresenter {
  words: TWordLearning[];
  dictionaryName: string;
  dictionaryNameRef: RefObject<HTMLInputElement>;
  removeConfirm: boolean;
  dictionaryNameError?: string;
  dictionaryId: number;
  onSave: () => void;
  onClose: () => void;
  onAddWord: () => void;
  onRemoveDictionary: () => void;
  onRemoveWord: (id: number) => () => void;
  onHide: (id: number) => () => void;
  updateWord: (id: number) => (e: ChangeEvent<HTMLInputElement>) => void;
  updateTransalte: (id: number) => (e: ChangeEvent<HTMLInputElement>) => void;
}
const Presenter: FC<IPresenter> = ({
  words,
  dictionaryName,
  dictionaryNameRef,
  removeConfirm,
  dictionaryNameError,
  dictionaryId,
  onAddWord,
  onSave,
  onClose,
  onRemoveWord,
  updateWord,
  updateTransalte,
  onRemoveDictionary,
}: IPresenter) => {
  return (
    <>
      <Input
        inputRef={dictionaryNameRef}
        name="dictionaryName"
        label="Название словаря"
        defaultValue={dictionaryName}
        error={dictionaryNameError}
      />
      {words.map(({ id, word, translate }) => (
        <WordListBlock key={id}>
          <ButtonsBlock>
            <RemoveIcon onClick={onRemoveWord(id)} />
          </ButtonsBlock>
          <WordLine key={id}>
            <Input
              onBlur={updateWord(id)}
              name="word"
              label="Слово"
              defaultValue={word}
            />
            <Input
              onBlur={updateTransalte(id)}
              name="translate"
              label="Перевод"
              defaultValue={translate}
            />
          </WordLine>
        </WordListBlock>
      ))}
      <AddWordBlock>
        <RoundButton onClick={onAddWord}>+</RoundButton>
      </AddWordBlock>
      <Footer>
        <SecondaryButton fullWidth onClick={onClose}>
          Закрыть
        </SecondaryButton>
        <Button fullWidth onClick={onSave}>
          Сохранить
        </Button>
      </Footer>
      {dictionaryId > 0 && (
        <ErrorButton fullWidth onClick={onRemoveDictionary}>
          {removeConfirm ? "Точно, удалить?" : "Удалить словарь"}
        </ErrorButton>
      )}
    </>
  );
};

export default memo(
  Presenter
  // (prev, cur) =>
  //   prev.removeConfirm === cur.removeConfirm &&
  //   prev.words === cur.words &&
  //   prev.dictionaryNameError === cur.dictionaryNameError &&
  //   prev.dictionaryName === cur.dictionaryName
);
