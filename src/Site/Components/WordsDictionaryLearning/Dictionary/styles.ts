import styled from "@emotion/styled";

export const DictionaryBlock = styled.div`
  margin-bottom: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #ddd;
`;
