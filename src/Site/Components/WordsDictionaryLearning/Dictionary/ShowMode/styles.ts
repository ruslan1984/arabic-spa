import styled from "@emotion/styled";
import { tabletXs } from "@site/media";

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 5px;
`;
export const Title = styled.div`
  font-weight: bold;
  font-size: 1.5rem;
  margin-bottom: 10px;
`;

export const WordLine = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #eee;
  gap: 5px;
  flex: 1;
  cursor: pointer;
  ${({ hide }: { hide?: boolean }) => hide && "background: #eee; color: #999;"}
`;

export const Word = styled.div`
  padding: 10px;
  min-width: 100px;
  display: flex;
  flex: 1;
  font-size: 1.3rem;
`;

export const WordsList = styled.div`
  margin: 20px auto 30px;
`;
