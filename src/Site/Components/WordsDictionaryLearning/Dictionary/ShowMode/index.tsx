import React, { FC, useCallback } from "react";
import Presenter from "./Presenter";
import { TWordLearning } from "../../types";
import { useDictionadyContext } from "../../context";

interface IShowMode {
  dictionaryId: number;
  dictionaryName: string;
  words: TWordLearning[];
}

const ShowMode: FC<IShowMode> = ({
  words,
  dictionaryName,
  dictionaryId,
}: IShowMode) => {
  const { setDictionaryMode, hideDictionaryWord } = useDictionadyContext();

  const onHide = (wordId: number) => () => {
    hideDictionaryWord(dictionaryId, wordId);
  };

  const onChangeMode = () => {
    setDictionaryMode(dictionaryId, "edit");
  };
  return (
    <Presenter
      dictionaryName={dictionaryName}
      words={words}
      onHide={onHide}
      onChangeMode={onChangeMode}
    />
  );
};

export default ShowMode;
