import React, { FC, memo } from "react";
import { TWordLearning } from "../../types";
import { EyeIcon, EditIcon } from "@site/Components/Button";
import { Title, WordLine, Word, Header, WordsList } from "./styles";
import WordsTest from "../../WordsTest";

interface IPresenter {
  dictionaryName: string;
  words: TWordLearning[];
  onHide: (id: number) => () => void;
  onChangeMode?: () => void;
}

const Presenter: FC<IPresenter> = ({
  dictionaryName,
  words,
  onHide,
  onChangeMode,
}: IPresenter) => {
  return (
    <>
      <Header>
        <Title>{dictionaryName}</Title>
        <EditIcon onClick={onChangeMode} />
      </Header>
      <WordsList>
        {words.map(({ id, word, translate, hide }) => (
          <WordLine key={id} hide={hide} onClick={onHide(id)}>
            <Word>{word}</Word>
            <Word>{translate}</Word>
            <EyeIcon hide={hide} />
          </WordLine>
        ))}
      </WordsList>
      <WordsTest words={words} />
    </>
  );
};

export default memo(Presenter);
