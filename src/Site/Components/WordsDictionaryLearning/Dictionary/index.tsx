import React, { FC } from "react";
import EditMode from "./EditMode";
import ShowMode from "./ShowMode";
import { TDictionary } from "../types";
import { DictionaryBlock } from "./styles";

const Dictionary: FC<TDictionary> = ({
  id,
  words,
  dictionaryName,
  mode,
}: TDictionary) => {
  return (
    <DictionaryBlock>
      {mode === "edit" ? (
        <EditMode
          words={words}
          dictionaryId={id}
          dictionaryName={dictionaryName}
        />
      ) : (
        <ShowMode
          words={words}
          dictionaryId={id}
          dictionaryName={dictionaryName}
        />
      )}
    </DictionaryBlock>
  );
};

export default Dictionary;
