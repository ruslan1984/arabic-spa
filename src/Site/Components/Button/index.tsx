import React, { FC, ButtonHTMLAttributes } from "react";
import {
  DefaultButton,
  RoundButton as RoundBtn,
  RemoveIcon as RemoveIcn,
  SecondaryButton as SecondaryBtn,
  EyeIcon as EyeIcn,
  ErrorButton as ErrorBtn,
} from "./styles";
import removeIcon from "@assets/icons/remove_basket.svg";
import openEye from "@assets/icons/eye/open-eye.png";
import closeEye from "@assets/icons/eye/close-eye.png";
import editIcon from "@assets/icons/edit.png";

interface IButton extends ButtonHTMLAttributes<HTMLButtonElement> {
  loading?: boolean;
  fullWidth?: boolean;
}
interface IIcon extends ButtonHTMLAttributes<HTMLDivElement> {
  loading?: boolean;
}

export const Button = ({
  loading,
  fullWidth,
  ...props
}: IButton): JSX.Element => (
  <DefaultButton fullWidth={fullWidth} disabled={loading} {...props} />
);

export const RoundButton = ({ loading, ...props }: IButton): JSX.Element => (
  <RoundBtn disabled={loading} {...props} />
);

export const EditIcon = ({ loading, ...props }: IButton): JSX.Element => (
  <RemoveIcn disabled={loading} {...props}>
    <img src={editIcon.src} alt="" />
  </RemoveIcn>
);

export const RemoveIcon = ({ loading, ...props }: IButton): JSX.Element => (
  <RemoveIcn disabled={loading} {...props}>
    <img src={removeIcon.src} alt="" />
  </RemoveIcn>
);

interface IEyeButton extends IIcon {
  hide?: boolean;
}

export const EyeIcon: FC<IEyeButton> = ({
  loading,
  hide,
  ...props
}: IEyeButton): JSX.Element => (
  <EyeIcn disabled={loading} {...props}>
    {hide ? (
      <img src={closeEye.src} alt="" />
    ) : (
      <img src={openEye.src} alt="" />
    )}
  </EyeIcn>
);

export const SecondaryButton = ({
  loading,
  fullWidth,
  ...props
}: IButton): JSX.Element => (
  <SecondaryBtn
    fullWidth={fullWidth}
    loading={loading}
    disabled={loading}
    {...props}
  />
);

export const ErrorButton = ({
  loading,
  fullWidth,
  ...props
}: IButton): JSX.Element => (
  <ErrorBtn
    fullWidth={fullWidth}
    loading={loading}
    disabled={loading}
    {...props}
  />
);
