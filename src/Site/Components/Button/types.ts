export type TButtonType = Partial<Record<"fullWidth" | "loading", boolean>>;
