import styled from "@emotion/styled";
import { TButtonType } from "./types";

const defaultStyle = `
  border: none;
  color: white;
  cursor: pointer;
  transition: 0.3s;
  font-size: 1.3rem;
`;

export const DefaultButton = styled.button`
  ${defaultStyle}
  padding: 10px 15px;
  border-radius: 5px;
  font-weight: 100;
  background: var(--bthBg);
  &:hover {
    background: var(--bthBg-hover);
  }
  box-shadow: 1px 2px 5px 0px #8d8c8c;
  ${({ fullWidth }: TButtonType) => fullWidth && "width: 100%;"}
`;

export const RoundButton = styled.button`
  ${defaultStyle}
  border-radius: 25px;
  width: 50px;
  height: 50px;
  font-size: 1.3rem;
  box-shadow: 1px 2px 5px 0px #8d8c8c;
  ${({ loading }: TButtonType) =>
    loading
      ? "background: #eee;"
      : "background: var(--bthBg); &:hover {background: var(--bthBg-hover);}"}
`;

export const RemoveIcon = styled.button`
  ${defaultStyle}
  border-radius: 25px;
  width: 35px;
  height: 35px;
  ${({ loading }: TButtonType) => !loading && "&:hover {background: #ddd;}"};
`;

export const EyeIcon = styled.div`
  ${defaultStyle}
  border-radius: 25px;
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  ${({ loading }: TButtonType) => loading && "&:hover {background: #ddd;}"}
`;

export const SecondaryButton = styled.button`
  ${defaultStyle}
  background: white;
  color: var(--bthBg);
  border: 1px solid var(--bthBg);
  border-radius: 5px;
  padding: 10px;
  box-shadow: 1px 2px 5px 0px #8d8c8c;
  &:hover {
    background: #fafafa;
  }
  ${({ loading, fullWidth }: TButtonType) =>
    (loading && "&:hover {background: white;}") ||
    (fullWidth && "width: 100%;")}
`;

export const ErrorButton = styled.button`
  ${defaultStyle}
  background: #c00505;
  color: white;
  border-radius: 5px;
  padding: 10px;
  box-shadow: 1px 2px 5px 0px #8d8c8c;
  &:hover {
    background: #930303;
  }
  ${({ loading, fullWidth }: TButtonType) =>
    (loading && "background: #eee; &:hover {background: #eee;}") ||
    (fullWidth && "width: 100%;")}
`;
