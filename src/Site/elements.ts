import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { tabletXs, mobileMd } from "@site/media";

export const PageFlex = styled.div`
  display: flex;
`;

export const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 5px;
  @media (min-width: ${mobileMd()}) {
    padding: 10px;
  }
  @media (min-width: ${tabletXs()}) {
    padding: 15px;
  }
`;

const inputStyle = css`
  padding: 10px;
  border: 1px solid var(--bthBg-hover);
  width: 100%;
  transition: 0.2s;
  &:focus {
    outline: none;
    border: 1px solid var(--bthMenu-hover);
  }
`;
export const Input = styled.input`
  ${inputStyle}
`;

export const Button = styled.button`
  padding: 10px;
  cursor: pointer;
  color: white;
  border: none;
  width: 100%;
  font-size: 18px;
  transition: 0.5s;
  background: var(--bthBg);
  border-radius: 5px;
  &:hover {
    background: var(--bthBg-hover);
  }
  &:focus {
    outline: none;
  }
`;
export const Label = styled.label`
  margin: 10px 0;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
`;

export const ErrorMessageFormik = styled.div`
  color: red;
  font-size: 14px;
`;

export const Li = styled.li`
  a {
    padding: 5px;
  }
`;
