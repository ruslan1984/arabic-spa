import styled, { StyledComponent } from "@emotion/styled";
import { tabletSm, mobileMd, mobileSm } from "@site/media";
import { closeIcon } from "@/components/Elements/icons";

export const ArabicText = styled.div`
  font-size: 1.7rem;
  text-align: right;
  p {
    font-size: 1.7rem;
    text-indent: 0;
    text-align: right;
  }
`;

export const Block = styled.div`
  padding-top: 40px;
  margin-bottom: 30px;
`;

export const Harf = styled.div`
  font-size: 7rem;
`;
export const HarfWithImg = styled.div<{ loading: boolean; name?: string }>`
  font-size: 7rem;
  border-radius: 10px;
  padding: 10px 10px 30px 10px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  transition: 0.5s;
  @media (max-width: ${tabletSm()}) {
    font-size: 4rem;
  }
  @media (max-width: ${mobileMd()}) {
    padding: 10px 5px;
  }
  @media (max-width: ${mobileSm()}) {
    font-size: 2.8rem;
  }

  ${({ loading }: { loading?: boolean }) => {
    if (loading) {
      return "color: #d8d5d5";
    } else {
      return "&:hover {background-color: var(--bthMenu-hover);}";
    }
  }}
`;

export const HarfWithHaraka = styled.div`
  font-size: 7rem;
  margin-bottom: 20px;
  @media (max-width: ${tabletSm()}) {
    font-size: 4rem;
  }
  @media (max-width: ${mobileMd()}) {
    padding: 10px 5px;
  }
  @media (max-width: ${mobileSm()}) {
    font-size: 3rem;
  }
`;

export const HarfLine = styled.div`
  display: flex;
  justify-content: space-around;
`;
export const HarfBlock = styled.div`
  text-align: center;
`;
export const ImgBlock = styled.div<{ active: boolean; name?: string }>`
  position: fixed;
  display: none;
  flex-direction: column;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  align-items: center;
  justify-content: center;
  z-index: 1500;
  cursor: pointer;
  padding: 5px;
  ${({ active }: { active: boolean }) => active && " display: flex;"}
  &:after {
    position: fixed;
    content: "";
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background: #000;
    opacity: 0.4;
    z-index: 400;
  }
  &:before {
    content: "";
    margin-bottom: 10px;
    width: 40px;
    height: 40px;
    right: 20px;
    top: 2px;
    background-color: white;
    background-image: url(${closeIcon});
    background-size: 65%;
    background-repeat: no-repeat;
    background-position: center;
    box-shadow: 0px 0px 20px #5c5c5c;
    z-index: 401;
    opacity: 1;
    border-radius: 20px;
  }
`;

const fone = `
  z-index: 500;
  box-shadow: 0px 0px 20px 5c5c5c;
  position: relative;
  border-radius: 25px;
`;
export const Img: StyledComponent<
  Partial<HTMLImageElement & { onClick: () => Promise<void> }>
> = styled.img(fone);

export const Object = styled.object(fone);

export const Icon = styled.img<{ name?: string }>`
  width: 50px;
  @media (max-width: ${tabletSm()}) {
    width: 30px;
  }
`;

export const HarfTitle = styled.div`
  display: flex;
  justify-content: center;
  text-align: center;
  margin: 5px;
  font-size: 1rem;
  @media (max-width: ${tabletSm()}) {
    font-size: 0.7rem;
    margin: 2px 1px;
  }
`;

export const HarfSoundItem = styled.div`
  margin-bottom: 60px;
  @media (max-width: ${tabletSm()}) {
    margin-bottom: 20px;
  }
`;
