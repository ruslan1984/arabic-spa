import React, { FC, useState, useEffect } from "react";
import {
  HarfWithImg,
  Icon,
  HarfTitle,
  HarfSoundItem,
  HarfWithHaraka,
} from "@site/Orthography/Detail/styles";
import {
  SoundNameType,
  UpdatedSoundDataType,
} from "@modules/Orthography/Detail/Sound/types";
import volume from "@assets/icons/volume.svg";
import { Query } from "@helpers/graphql";

interface IHarfSoundBlock {
  id: number;
  name: SoundNameType;
  title: string;
  haraka?: string;
  symbol?: string;
}

const HarfSoundBlock: FC<IHarfSoundBlock> = ({
  id,
  name,
  haraka,
  symbol,
  title,
}: IHarfSoundBlock) => {
  const [sound, setSound] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setSound("");
  }, [id]);

  const playSound = async (name: SoundNameType) => {
    let soundData: string | null | undefined = "";
    try {
      const query = `{ orthography(id: ${id}) {${name}}}`;
      setLoading(true);
      const fetchData: { orthography: UpdatedSoundDataType } = await Query(
        query
      );
      soundData = fetchData?.orthography[name];
      if (soundData) {
        const audio = new Audio(soundData);
        await audio.load();
        audio.play();
      }
    } finally {
      setLoading(false);
    }
    return soundData;
  };
  const play = async (sound: string) => {
    const audio = new Audio(sound);
    await audio.load();
    audio.play();
  };
  const onPlay = async () => {
    if (!sound) {
      const soundData: string | null | undefined = await playSound(name);
      soundData && setSound(soundData);
    } else {
      await play(sound);
    }
  };
  return (
    <HarfSoundItem>
      <HarfWithImg loading={loading} name={name} onClick={onPlay}>
        <HarfWithHaraka>
          <>
            {symbol}
            {haraka}
          </>
        </HarfWithHaraka>
        <Icon src={volume.src} alt="" />
        <HarfTitle>{title}</HarfTitle>
      </HarfWithImg>
    </HarfSoundItem>
  );
};

export default HarfSoundBlock;
