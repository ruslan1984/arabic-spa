import React, { FC } from "react";
import { HarfLine } from "../styles";
import HarfSoundBlock from "./HarfSoundBlock";
import { THarfSound } from "@site/Orthography/types";

interface IHarfSound extends THarfSound {
  id: number;
  harfName: string;
  harfSoundSymbol: string;
}

const HarfSound: FC<IHarfSound> = ({
  id,
  harfName,
  harfNameSound,
  harfFathaSound,
  harfKesraSound,
  harfDammaSound,
  harfSoundSymbol,
}) => (
  <>
    {harfNameSound && (
      <HarfSoundBlock
        title="Название буквы"
        id={id}
        name="harfNameSound"
        symbol={harfName}
      />
    )}
    <HarfLine>
      {harfDammaSound && (
        <HarfSoundBlock
          id={id}
          symbol={harfSoundSymbol}
          name="harfDammaSound"
          haraka="ُ"
          title="с даммой"
        />
      )}
      {harfKesraSound && (
        <HarfSoundBlock
          id={id}
          symbol={harfSoundSymbol}
          name="harfKesraSound"
          haraka="ِ"
          title="с касрой"
        />
      )}
      {harfFathaSound && (
        <HarfSoundBlock
          id={id}
          symbol={harfSoundSymbol}
          name="harfFathaSound"
          haraka="َ"
          title="с фатхой"
        />
      )}
    </HarfLine>
  </>
);

export default HarfSound;
