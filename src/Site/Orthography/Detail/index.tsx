import React, { FC } from "react";
import LayoutDetail from "@site/Layouts/LayoutDetail";
import { Block } from "./styles";
import HarfImg from "./HarfImg";
import HarfSound from "./HarfSound";
import {
  TSeo,
  TContact,
  TMobileMenu,
  TScrollMenu,
  TMenuItem,
} from "@site/types";
import { TOrthographyDetail } from "@site/Orthography/types";

export interface IOrthographyDetail {
  seo: TSeo;
  list: TMenuItem[];
  currentPage: string;
  nextPage: string;
  page: string;
  scrollMenu: TScrollMenu;
  detail: TOrthographyDetail;
  contact: TContact;
  isAlphabet: boolean;
  menu: TMobileMenu[];
}

const OrthographyDetail: FC<IOrthographyDetail> = ({
  seo,
  list,
  currentPage,
  nextPage,
  page,
  scrollMenu,
  detail,
  contact,
  menu,
}: IOrthographyDetail) => {
  return (
    <LayoutDetail
      seo={seo}
      list={list}
      currentPage={currentPage}
      nextPage={nextPage}
      url={page}
      scrollMenu={scrollMenu}
      contact={contact}
      isAlphabet={detail.isAlphabet}
      menu={menu}
    >
      {detail && (
        <>
          <h1>{detail.name}</h1>
          <HarfImg
            id={detail.id}
            harfName={detail.harfName}
            harfFree={detail.harfFree}
            harfLast={detail.harfLast}
            harfCenter={detail.harfCenter}
            harfFirst={detail.harfFirst}
          />
          {detail.textAbout && (
            <>
              <Block id="textAbout">
                <h2>Произношение</h2>
                <HarfSound
                  id={detail.id}
                  harfName={detail.harfName}
                  harfNameSound={detail.harfNameSound}
                  harfFathaSound={detail.harfFathaSound}
                  harfKesraSound={detail.harfKesraSound}
                  harfDammaSound={detail.harfDammaSound}
                  harfSakenSound={detail.harfSakenSound}
                  harfSoundSymbol={detail.harfSoundSymbol}
                />
                <div dangerouslySetInnerHTML={{ __html: detail.textAbout }} />
              </Block>
            </>
          )}
          <Block id="textForReading">
            <h2>Для чтения</h2>
            <div
              dangerouslySetInnerHTML={{
                __html: detail.textForReading,
              }}
            ></div>
          </Block>
        </>
      )}
    </LayoutDetail>
  );
};

export default OrthographyDetail;
