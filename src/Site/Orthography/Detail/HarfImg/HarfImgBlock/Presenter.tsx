import React, { FC } from "react";
import {
  HarfWithImg,
  ImgBlock,
  Img,
  Icon,
  HarfTitle,
  Object,
} from "@site/Orthography/Detail/styles";
import feather from "@assets/icons/hand-with-pen.png";

interface IPresenter {
  loading: boolean;
  name: string;
  harf: string;
  title?: string;
  showHideImage: () => Promise<void>;
  showHarf: boolean;
  harfImg: string;
  isSVG: boolean;
}

const Presenter: FC<IPresenter> = ({
  loading,
  name,
  harf,
  title,
  showHideImage,
  showHarf,
  harfImg,
  isSVG,
}: IPresenter) => (
  <>
    <HarfWithImg loading={Boolean(loading)} name={name} onClick={showHideImage}>
      {harf}
      <Icon src={feather.src} alt="" />
      {title && <HarfTitle>{title}</HarfTitle>}
    </HarfWithImg>
    <ImgBlock
      name={name}
      active={Boolean(showHarf && harfImg)}
      onClick={showHideImage}
    >
      {isSVG ? (
        !loading && <Object name={harf} type="image/svg+xml" data={harfImg} />
      ) : (
        <Img name={name} src={harfImg} onClick={showHideImage} alt={name} />
      )}
    </ImgBlock>
  </>
);

export default Presenter;
