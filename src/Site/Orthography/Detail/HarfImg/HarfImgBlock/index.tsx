import React, { FC, useState, useCallback, useEffect } from "react";
import { Query } from "@helpers/graphql";
import Presenter from "./Presenter";
import { THarfImg, THarfImgKeys } from "@site/Orthography/types";

interface IHarfImgBlock {
  id: number;
  name: THarfImgKeys;
  harf: string;
  title?: string;
}

const HarfImgBlock: FC<IHarfImgBlock> = ({
  id,
  title,
  name,
  harf,
}: IHarfImgBlock) => {
  const [showHarf, setShowHarf] = useState(false);
  const [harfImg, setHarfImg] = useState("");
  const [loading, setLoading] = useState(false);
  const [isSVG, setIsSVG] = useState(false);

  useEffect(() => {
    setHarfImg("");
  }, [id]);

  const getHarf = useCallback(async () => {
    let data: string = "";
    try {
      setLoading(true);
      const query = `{ orthography(id: ${id}) {${name}}}`;
      const fetchData: { orthography: THarfImg } = await Query(query);
      data = fetchData?.orthography[name];
    } finally {
      setLoading(false);
    }
    return data;
  }, [id]);

  const showHideImage = async () => {
    if (!harfImg) {
      const harfData: string = await getHarf();
      const dataType = harfData?.substring(0, harfData.indexOf(";"));
      dataType === "data:image/svg+xml" ? setIsSVG(true) : setIsSVG(false);
      setHarfImg(harfData);
    }
    setShowHarf(!showHarf);
  };

  return (
    <Presenter
      loading={loading}
      name={name}
      harf={harf}
      title={title}
      showHarf={showHarf}
      harfImg={harfImg}
      isSVG={isSVG}
      showHideImage={showHideImage}
    />
  );
};

export default HarfImgBlock;
