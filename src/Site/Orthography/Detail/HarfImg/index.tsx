import React, { FC } from "react";
import {
  HarfBlock,
  Harf,
  HarfLine,
  Block,
} from "@site/Orthography/Detail/styles";
import HarfImgBlock from "./HarfImgBlock";
import { THarf } from "@site/Orthography/types";

interface IHarfImg extends THarf {
  id: number;
}

const HarfImg: FC<IHarfImg> = ({
  id,
  harfName,
  harfFree,
  harfLast,
  harfCenter,
  harfFirst,
}: IHarfImg) => {
  return (
    <HarfBlock>
      <Harf>{harfName}</Harf>
      {harfFree && (
        <Block id="harf">
          <h2>Написание</h2>
          <HarfImgBlock id={id} harf={harfFree} name="harfFreeImg" />
          <div>Начертание букв при соединении с</div>
          <HarfLine>
            <div>
              <HarfImgBlock
                id={id}
                title="предыдущей"
                harf={harfLast}
                name="harfLastImg"
              />
            </div>
            <div>
              <HarfImgBlock
                id={id}
                title="двух сторон"
                harf={harfCenter}
                name="harfCenterImg"
              />
            </div>
            <div>
              <HarfImgBlock
                id={id}
                title="последующей"
                harf={harfFirst}
                name="harfFirstImg"
              />
            </div>
          </HarfLine>
        </Block>
      )}
    </HarfBlock>
  );
};

export default HarfImg;
