import React, { FC } from "react";
import Link from "next/link";
import LayoutList from "@site/Layouts/LayoutList";
import { Li } from "@site/elements";
import { TSeo, TContact, TMobileMenu, TOrthographyMenuItem } from "@site/types";

export interface IOrthographyList {
  seo: TSeo;
  url: string;
  contact: TContact;
  menu: TMobileMenu[];
  list: TOrthographyMenuItem[];
}

const OrthographyList: FC<IOrthographyList> = ({
  seo,
  url,
  contact,
  menu,
  list,
}: IOrthographyList) => (
  <LayoutList seo={seo} url={url} contact={contact} menu={menu}>
    <h1>Арабские буквы</h1>
    <ul>
      {list &&
        list.map((item: TOrthographyMenuItem) => {
          return (
            <Li key={item.id}>
              <Link href={"/" + url + "/" + item.code}>
                <a>{item.name}</a>
              </Link>
            </Li>
          );
        })}
    </ul>
  </LayoutList>
);

export default OrthographyList;
