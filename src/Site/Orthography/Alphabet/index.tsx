import React, { FC } from "react";
import Link from "next/link";
import LayoutDetail from "@site/Layouts/LayoutDetail";
import {
  Block,
  AlphabetBlock,
  Harf,
  HarfBlock,
  Transcription,
  ReadComment,
  ReadCommentArrow,
  ButtonsBlock,
} from "./styles";
import {
  TSeo,
  TContact,
  TScrollMenu,
  TMobileMenu,
  TOrthographyMenuItem,
} from "@site/types";
import { TOrthographyDetail } from "@site/Orthography/types";

import HarfSoundBlock from "./HarfSoundBlock";
import HarfImgBlock from "./HarfImgBlock";

interface IAlphabet {
  seo: TSeo;
  list: TOrthographyMenuItem[];
  currentPage: string;
  nextPage: string;
  page: string;
  scrollMenu: TScrollMenu;
  detail: TOrthographyDetail;
  contact: TContact;
  isAlphabet: boolean;
  menu: TMobileMenu[];
}

export const Alphabet: FC<IAlphabet> = ({
  seo,
  list,
  currentPage,
  nextPage,
  page,
  scrollMenu,
  detail,
  contact,
  menu,
}: IAlphabet) => {
  return (
    <LayoutDetail
      seo={seo}
      list={list}
      currentPage={currentPage}
      nextPage={nextPage}
      url={page}
      scrollMenu={scrollMenu}
      contact={contact}
      isAlphabet={detail.isAlphabet}
      menu={menu}
    >
      {detail && (
        <>
          <h1 id="alfavit">{detail.name}</h1>
          <ReadComment>
            <div>Читать справа налево</div>
            <ReadCommentArrow />
          </ReadComment>

          <AlphabetBlock dir="rtl">
            {list
              .filter(({ isAlphabet }: TOrthographyMenuItem) => isAlphabet)
              .map(
                ({
                  id,
                  harfFree,
                  code,
                  transcription,
                }: TOrthographyMenuItem) => (
                  <HarfBlock key={id}>
                    <Harf>
                      <Link href={"/" + page + "/" + code}>{harfFree}</Link>
                      <Link href={"/" + page + "/" + code}>
                        <Transcription>{transcription}</Transcription>
                      </Link>
                    </Harf>
                    <ButtonsBlock>
                      <HarfSoundBlock id={id} />
                      <HarfImgBlock id={id} />
                    </ButtonsBlock>
                  </HarfBlock>
                )
              )}
          </AlphabetBlock>
          <Block id="textForReading">
            <h2>Для чтения</h2>
            <div
              dangerouslySetInnerHTML={{
                __html: detail.textForReading,
              }}
            ></div>
          </Block>
        </>
      )}
    </LayoutDetail>
  );
};

export default Alphabet;
