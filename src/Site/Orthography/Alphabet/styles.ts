import styled from "@emotion/styled";
import { tabletSm, mobileMd, mobileSm, mobileLg } from "@site/media";

export const Block = styled.div`
  padding-top: 40px;
  margin-bottom: 30px;
`;

export const AlphabetBlock = styled.bdo`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const Harf = styled.div`
  font-size: 5rem;
  cursor: pointer;
  padding: 0.5rem 2rem;
  text-align: center;
  background-color: #eee;
  background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA70lEQVR4nO2VMUoEURBE9wgi5pqKoCKKuomZYGSiuYkewMAbTfer2oE5iWfQXNTAQBBxZWCDjXaHkS+ubEFnn35Qv7p7MFhq4QQcSPqQNJ5XwKfti16gzDxvm9i+tX0zq4A34O5HoKqq1ua9lfT0v0B1Xa83TbNSBGR71fZxr+ZdQbZ3gUdgVAwEHAGvk2jfS7oGLtvKzNPMHEbEdmZu9AZl5qakly6zJWncAnuDgOeOQ/wVEVu9rYuIw2nrbF9NbDuzfdL+X2tb5xTOCgOwI+mhaBim4x0R+8VBvzawi7vrXPpMAHuS3osfvqX+nL4B2XqkeQk144MAAAAASUVORK5CYII=");
  background-repeat: no-repeat;
  background-position: right top;
  background-size: 30px;
  border-radius: 10px;
  margin-bottom: 5px;
  border-radius: 10px;
  transition: 0.5s;
  &:hover {
    background-color: #dfdfdf;
    transform: scale(1.05) translateY(-15px);
  }
  a {
    color: #797979;
  }
  @media (max-width: ${tabletSm()}) {
    font-size: 5rem;
    padding: 0.5rem 1rem;
  }
  @media (max-width: ${mobileMd()}) {
    font-size: 4rem;
  }
  @media (max-width: ${mobileSm()}) {
    font-size: 3.5rem;
  }
`;

export const HarfBlock = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 1rem 0.5rem;
  border-radius: 14px;
  transition: 0.5s;
  justify-content: space-between;
  @media (max-width: ${tabletSm()}) {
    margin: 0.5rem;
  }
  @media (max-width: ${mobileLg()}) {
    margin: 0.25rem;
  }
`;

export const Icon = styled.img`
  width: 50px;
  @media (max-width: ${tabletSm()}) {
    width: 30px;
  }
`;

export const HarfWithImg = styled.div<{ loading: boolean; name?: string }>`
  border-radius: 10px;
  cursor: pointer;
  transition: 0.5s;
  padding: 0.1rem;
  text-align: center;
  background-color: #eee;
  flex: 1;
  margin-bottom: 0.5rem;
  &:hover {
    background-color: #dfdfdf;
  }
  ${({ loading }: { loading?: boolean }) => loading && "color: #d8d5d5"}}
`;

export const Transcription = styled.div`
  font-size: 1rem;
`;

export const ReadComment = styled.div`
  font-size: 1rem;
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: end;
`;
export const ReadCommentArrow = styled.div`
  width: 0;
  height: 0;
  border-top: 8px solid transparent;
  border-bottom: 8px solid transparent;
  border-right: 25px solid #bfbfbf;
  position: relative;
  left: -200px;
  margin-bottom: 20px;
  &:after {
    content: "";
    right: 0;
    background: #bfbfbf;
    width: 200px;
    height: 1px;
    display: block;
    position: relative;
  }
`;

export const ButtonsBlock = styled.div`
  display: flex;
  gap: 2px;
  width: 100%;
`;
