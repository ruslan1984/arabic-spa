import React, { FC, useState, useCallback, useEffect } from "react";
import { Query } from "@helpers/graphql";
import { ImgBlock, Img, Icon, Object } from "@site/Orthography/Detail/styles";
import { HarfWithImg } from "./styles";
import feather from "@assets/icons/hand-with-pen.png";

interface IHarfImgBlock {
  id: number;
}

const HarfImgBlock: FC<IHarfImgBlock> = ({ id }: IHarfImgBlock) => {
  const [showHarf, setShowHarf] = useState(false);
  const [harfImg, setHarfImg] = useState("");
  const [loading, setLoading] = useState(false);
  const [isSVG, setIsSVG] = useState(false);

  useEffect(() => {
    setHarfImg("");
  }, [id]);

  const getHarf = useCallback(async () => {
    let data = "";
    try {
      setLoading(true);
      const query = `{ orthography(id: ${id}) {harfFreeImg}}`;
      const fetchData: { orthography: { harfFreeImg: string } } = await Query(
        query
      );
      data = fetchData?.orthography["harfFreeImg"];
    } finally {
      setLoading(false);
    }
    return data;
  }, [id]);

  const showHideImage = async () => {
    if (!harfImg) {
      const harfData: string = await getHarf();
      const dataType = harfData?.substring(0, harfData.indexOf(";"));
      dataType === "data:image/svg+xml" ? setIsSVG(true) : setIsSVG(false);
      setHarfImg(harfData);
    }
    setShowHarf(!showHarf);
  };

  return (
    <>
      <HarfWithImg loading={Boolean(loading)} onClick={showHideImage}>
        <Icon src={feather.src} alt="" />
      </HarfWithImg>
      <ImgBlock active={Boolean(showHarf && harfImg)} onClick={showHideImage}>
        {isSVG ? (
          !loading && <Object type="image/svg+xml" data={harfImg} />
        ) : (
          <Img src={harfImg} alt={""} />
        )}
      </ImgBlock>
    </>
  );
};

export default HarfImgBlock;
