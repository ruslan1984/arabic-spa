import React, { FC, useState, useEffect } from "react";
import { HarfWithImg, Icon } from "./styles";
import { UpdatedSoundDataType } from "@modules/Orthography/Detail/Sound/types";
import volume from "@assets/icons/volume.svg";
import { Query } from "@helpers/graphql";

interface IHarfSoundBlock {
  id: number;
}

const HarfSoundBlock: FC<IHarfSoundBlock> = ({ id }: IHarfSoundBlock) => {
  const [sound, setSound] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setSound("");
  }, [id]);

  const playSound = async () => {
    let soundData: string | null | undefined = "";
    try {
      const query = `{ orthography(id: ${id}) {harfNameSound}}`;
      setLoading(true);
      const fetchData: { orthography: UpdatedSoundDataType } = await Query(
        query
      );
      soundData = fetchData?.orthography["harfNameSound"];
      if (soundData) {
        const audio = new Audio(soundData);
        audio.load();
        audio.play();
      }
    } finally {
      setLoading(false);
    }
    return soundData;
  };
  const play = (sound: string): void => {
    const audio = new Audio(sound);
    audio.load();
    audio.play();
  };
  const onPlay = async () => {
    if (!sound) {
      const soundData: string | null | undefined = await playSound();
      soundData && setSound(soundData);
    } else {
      play(sound);
    }
  };
  return (
    <HarfWithImg loading={loading} name="harfNameSound" onClick={onPlay}>
      <Icon src={volume.src} alt="" />
    </HarfWithImg>
  );
};

export default HarfSoundBlock;
