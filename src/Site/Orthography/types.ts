export type TOrthographyDetail = {
  id: number;
  created_at: string;
  updated_at: string;
  name: string;
  title?: null;
  code: string;
  imgTell?: string;
  textAbout: string;
  textForReading: string;
  deletedAt?: string;
  sort: number;
  harfSoundSymbol: string;
  isAlphabet: boolean;
  transcription: string;
} & THarf &
  THarfImg &
  THarfSound;

export type THarfImgKeys =
  | "harfFreeImg"
  | "harfFirstImg"
  | "harfCenterImg"
  | "harfLastImg";
export type THarfImg = Record<THarfImgKeys, string>;

export type THarfSound = Record<
  | "harfNameSound"
  | "harfFathaSound"
  | "harfKesraSound"
  | "harfDammaSound"
  | "harfSakenSound",
  string
>;

export type THarf = Record<
  "harfName" | "harfFree" | "harfFirst" | "harfCenter" | "harfLast",
  string
>;
