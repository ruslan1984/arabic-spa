import styled from "@emotion/styled";

export const IndexH2 = styled.h2`
  font-size: 2.3rem;
`;
