import React, { FC, useEffect, useState } from "react";
import cn from "classnames";
import Link from "next/link";
import {
  IndexHeader,
  IndexHeaderUl,
  IndexHeaderH1,
  BeginBtn,
  IndexText,
} from "./styles";

export const Main: FC = () => {
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    setLoaded(true);
  }, []);
  return (
    <IndexHeader className={cn({ active: loaded })}>
      <IndexText>
        <div>
          <IndexHeaderH1>Арабский Онлайн</IndexHeaderH1>
          <IndexHeaderUl>
            <li>
              <Link href="/arabskie-bukvy">
                <a>Арабские буквы</a>
              </Link>
            </li>
            <li>
              <Link href="/grammatika">
                <a>Грамматика</a>
              </Link>
            </li>
            <li>
              <Link href="/arabic-texts/1">
                <a>Арабскийе тексты</a>
              </Link>
            </li>
            <li>
              <Link href="/arabic-words">
                <a>Заучивание слов</a>
              </Link>
            </li>
          </IndexHeaderUl>
        </div>
        <BeginBtn>
          <Link href="/arabskie-bukvy/arabskoe-pismo">
            <a>Начать обучение</a>
          </Link>
        </BeginBtn>
      </IndexText>
      {/*<Loading />*/}
    </IndexHeader>
  );
};
