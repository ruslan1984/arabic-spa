import React, { FC } from "react";
import Link from "next/link";
import { LiTitle, UlList, UlMain, LiLink, DetailLink } from "./styles";
import { TMobileMenu, TMenuItem } from "@site/types";

interface IMenu {
  menu: TMobileMenu[];
}

export const Menu: FC<IMenu> = ({ menu }: IMenu) => {
  return (
    <>
      {menu?.map((item: TMobileMenu, index) => {
        const list = item.list?.map((item1: TMenuItem) => (
          <LiLink key={item.code + item1.code}>
            <Link href={"/" + item.code + "/" + item1.code}>
              <a>{item1.name}</a>
            </Link>
          </LiLink>
        ));

        return (
          <React.Fragment key={item.code + index}>
            {item.list ? (
              <UlMain key={item.code}>
                <>
                  <LiTitle>{item.name}</LiTitle>
                  <UlList>{list}</UlList>
                </>
              </UlMain>
            ) : (
              <DetailLink key={item.code}>
                <Link href={"/" + item.code}>
                  <a>{item.name}</a>
                </Link>
              </DetailLink>
            )}
          </React.Fragment>
        );
      })}
    </>
  );
};

export default Menu;
