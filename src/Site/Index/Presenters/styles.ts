import styled from "@emotion/styled";
import { keyframes } from "@emotion/react";
import Link from "next/link";
import {
  indexMiddle,
  mobileSm,
  tabletMd,
  tabletLg,
  tabletSm,
} from "@site/media";

export const IndexHeader = styled.div`
  width: 100%;
  height: 100vh;
  padding: 100px;
  display: flex;
  align-items: flex-end;
  opacity: 0;
  transition: opacity 2s;
  &.active {
    opacity: 1;
  }
  @media (max-width: ${indexMiddle(-1)}) {
    padding: 30px;
    align-items: center;
  }
  @media (max-width: 320px) {
    padding: 10px;
  }
`;
export const IndexHeaderH1 = styled.h1`
  font-size: 55px;
  font-weight: 900;
  color: #2b292c;
  text-shadow: 7px 6px 7px #9e9e9e;
  font-family: "Poiret One", cursive;
  @media (max-width: ${indexMiddle(-1)}) {
    text-align: left;
  }
`;
export const IndexHeaderUl = styled.ul`
  font-size: 30px;
  padding-left: 30px;
  min-height: 80px;
  margin-bottom: 80px;
  @media (max-width: ${indexMiddle(-1)}) {
    padding-left: 0;
  }
  a {
    font-family: "Poiret One", cursive;
    transition: 0.6s;
    display: flex;
    font-size: 30px;
    @media (min-width: ${tabletLg(1)}) {
      &:hover {
        font-weight: 700;
        font-size: 35px;
        transform: translateX(10px);
        text-shadow: 2px 6px 3px #9e9e9e;
      }
    }
  }
  li {
    min-height: 45px;
  }
`;
export const UlList = styled.ul`
  margin-bottom: 70px;
  column-count: 1;
  @media (min-width: ${tabletSm()}) {
    column-count: 2;
  }
  @media (min-width: ${tabletMd()}) {
    column-count: 3;
  }
  @media (min-width: ${tabletLg()}) {
    column-count: 4;
  }
`;
export const LiTitle = styled.li`
  font-size: 30px;
  font-weight: 400;
  margin-bottom: 13px;
  text-align: center;
  margin-bottom: 28px;
  margin-top: 70px;
`;
export const UlMain = styled.ul`
  margin: 0 auto;
  max-width: 400px;
  @media (min-width: ${tabletSm()}) {
    // margin: 70px 0;
    max-width: 100%;
  }
`;

export const LiLink = styled.li`
  padding: 5px 8px 5px 0;
  a {
    font-size: 1.2rem;
  }
  &:hover {
    text-decoration: underline;
  }
`;
export const BeginBtn = styled.div`
  width: 100%;
  a {
    display: flex;
    padding: 20px 30px;
    background: var(--bthBg);
    border-radius: 6px;
    box-shadow: 2px 3px 12px #a29696;
    color: white;
    font-family: "Poiret One";
    font-size: 30px;
    font-weight: 700;
    transition: 0.5s;
    display: flex;
    justify-content: center;
    @media (max-width: ${mobileSm(-1)}) {
      font-size: 25px;
    }
    &:hover {
      background: var(--bthBg-hover);
    }
  }
`;

const mainAnimation = keyframes`
  0%{
    opacity: 0;
	}
	100%{
		opacity: 1;
	}  
`;

export const IndexText = styled.div`
    max-width: 500px;
    margin: 0 auto;
    display: flex;
    flex-wrap: wrap;
		align-content: space-between;
		animation ${mainAnimation} .5s ease-in-out;
`;

export const DetailLink = styled.div`
  font-size: 1.4rem;
  display: block;
  margin-bottom: 16px;
  &:hover {
    text-decoration: underline;
  }
`;
