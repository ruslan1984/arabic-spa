import styled from "@emotion/styled";
import { tabletXs, tabletLg } from "@site/media";

export const FooterStyle = styled.div`
  display: flex;
  margin: 0;
  padding: 50px;
  border-top: 1px solid var(--borderColor);
  justify-content: center;
  @media (max-width: ${tabletLg()}) {
    flex-direction: column;
    display: none;
  }
`;

export const MenuUl = styled.div`
  display: flex;
  @media (max-width: ${tabletXs()}) {
    flex-direction: column;
  }
`;

export const MenuLi = styled.div`
  padding: 10px;
  font-size: 22px;
  font-weight: 100;
  border-radius: 20px;
  transition: 0.5s;
  &:hover {
    background: var(--bthMenu-hover);
  }
`;
