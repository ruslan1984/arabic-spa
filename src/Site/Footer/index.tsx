import React, { FC } from "react";
import Link from "next/link";
import { FooterStyle, MenuUl, MenuLi } from "./styles";
import { metrincaYa, MetrincaGoogle } from "./metrica";

export const Footer: FC = () => (
  <>
    <FooterStyle>
      <MenuUl>
        <MenuLi>
          <Link href="/arabskie-bukvy">
            <a>Буквы</a>
          </Link>
        </MenuLi>
        <MenuLi>
          <Link href="/grammatika">
            <a>Грамматика</a>
          </Link>
        </MenuLi>
        <MenuLi>
          <Link href="/arabic-texts/1">
            <a>Арабские тексты</a>
          </Link>
        </MenuLi>
        <MenuLi>
          <Link href="/arabic-words/1">
            <a>Слова уроков</a>
          </Link>
        </MenuLi>
        <MenuLi>
          <Link href="/arabic-texts">
            <a>Заучивание слов</a>
          </Link>
        </MenuLi>
        <MenuLi>
          <Link href="/repetitor">
            <a>Репетитор</a>
          </Link>
        </MenuLi>
        <MenuLi>
          <Link href="/im-help">
            <a>Содействие</a>
          </Link>
        </MenuLi>
      </MenuUl>
    </FooterStyle>
    {(() => {
      if (typeof document === "undefined") return null;
      metrincaYa();
      return <MetrincaGoogle />;
    })()}
  </>
);

export default Footer;
