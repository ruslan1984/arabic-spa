export const desktopLg = (num = 0): string => String(1920 + num + "px");
export const desktopMd = (num = 0): string => String(1600 + num + "px");
export const desktopSm = (num = 0): string => String(1440 + num + "px");
export const desktopXs = (num = 0): string => String(1280 + num + "px");

export const tabletLg = (num = 0): string => String(1024 + num + "px");
export const tabletMd = (num = 0): string => String(911 + num + "px");
export const tabletSm = (num = 0): string => String(768 + num + "px");
export const tabletXs = (num = 0): string => String(480 + num + "px");

export const mobileLg = (num = 0): string => String(411 + num + "px");
export const mobileMd = (num = 0): string => String(360 + num + "px");
export const mobileSm = (num = 0): string => String(320 + num + "px");
export const mobileXs = (num = 0): string => String(280 + num + "px");

export const indexMiddle = (num = 0): string => String(700 + num + "px");
