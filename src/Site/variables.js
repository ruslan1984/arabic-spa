export const variables = ` 
:root {
  --main-bg: #fffef7;
  --bthBg: #bd853a;
  --bthBg-hover: #d6a25d;
  --bthMenu-hover: #f3dbbb;
  --borderColor: #dadada;
}`;
