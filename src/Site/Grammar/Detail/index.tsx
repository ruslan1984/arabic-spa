import React, { FC } from "react";
import { WordsList } from "@site/Detail/WordsList";
import WordsTest from "@site/Detail/WordsTest";
import QuestionsList from "@site/Detail/QuestionsList";
import LayoutDetail from "@site/Layouts/LayoutDetail";
import { Testing } from "@modules/Test/Testing";
import ArabicSentence from "@modules/ArabicText";
import { Block, ArabicText } from "./styles";
import {
  TSeo,
  TMobileMenu,
  TGrammarMenuItem,
  TScrollMenu,
  TContact,
  TGrammarDetail,
  TTest,
  TWordList,
  TQuestion,
} from "./types";

export interface IGrammarDetail {
  seo: TSeo;
  list: TGrammarMenuItem[];
  page: string;
  currentPage: string;
  nextPage: string;
  scrollMenu: TScrollMenu;
  contact: TContact;
  menu: TMobileMenu[];
  detail: TGrammarDetail;
  test: TTest[];
  words: TWordList;
  questions: TQuestion[];
}

export const Detail: FC<IGrammarDetail> = ({
  seo,
  list,
  currentPage,
  nextPage,
  page,
  scrollMenu,
  contact,
  menu,
  detail,
  test,
  words,
  questions,
}: IGrammarDetail) => (
  <>
    <LayoutDetail
      seo={seo}
      list={list}
      currentPage={currentPage}
      nextPage={nextPage}
      url={page}
      scrollMenu={scrollMenu}
      contact={contact}
      isAlphabet={false}
      menu={menu}
    >
      {detail && (
        <>
          {detail.grammarText && (
            <>
              <h1 id="grammarText">{detail.name}</h1>
              <h2>Грамматика</h2>
              <div
                dangerouslySetInnerHTML={{ __html: detail.grammarText }}
              ></div>
            </>
          )}
          {test.length > 0 && (
            <Block id="test">
              <h2>Тест</h2>
              <Testing data={test} />
            </Block>
          )}
          {detail.arabicText && !detail.hideArabicText && (
            <Block id="arabicText">
              <h2>Арабский текст</h2>
              <ArabicText
                dangerouslySetInnerHTML={{ __html: detail.arabicText }}
              />
            </Block>
          )}
          {!detail.hideArabicSentence &&
            detail?.arabicSentence &&
            detail?.arabicSentence?.length > 0 && (
              <Block id="arabicText">
                <h2>Арабский текст</h2>
                <ArabicSentence arabicText={detail.arabicSentence} />
              </Block>
            )}
          {(words.ism || words.figl || words.harf) && (
            <Block id="words">
              <h2>Слова</h2>
              <WordsList words={words} />
              <WordsTest words={words} />
            </Block>
          )}
          {questions.length > 0 && (
            <Block id="questions">
              <h2>Вопросы</h2>
              <QuestionsList questions={questions} />
            </Block>
          )}
        </>
      )}
    </LayoutDetail>
  </>
);

export default Detail;
