export type {
  TSeo,
  TMobileMenu,
  TGrammarMenuItem,
  TScrollMenu,
  TContact,
  TWordList,
  TQuestion,
  TArabicSentence,
} from "@site/types";
export type { TTest } from "@modules/Test/types";
import { TTest } from "@modules/Test/types";
import { TArabicSentence } from "@site/types";

export type TGrammarDetail = {
  id: number;
  name: string;
  code: string;
  arabicText?: string;
  grammarText?: string;
  arabicSentence?: TArabicSentence;
  hideArabicText?: boolean;
  hideArabicSentence?: boolean;
  test: TTest[];
};
