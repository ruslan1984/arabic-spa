import styled from "@emotion/styled";

export const ArabicText = styled.div`
  font-size: 1.7rem;
  text-align: right;
  p {
    font-size: 1.7rem;
    text-indent: 0;
    text-align: right;
  }
`;

export const Block = styled.div`
  padding-top: 40px;
`;
