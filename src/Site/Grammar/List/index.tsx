import React, { FC } from "react";
import Link from "next/link";
import LayoutList from "@site/Layouts/LayoutList";
import { Li } from "./elements";
import { TSeo, TContact, TMobileMenu, TGrammarMenuItem } from "@site/types";

export interface IGrammarList {
  seo: TSeo;
  url: string;
  contact: TContact;
  menu: TMobileMenu[];
  list: TGrammarMenuItem[];
}

export const GrammarList: FC<IGrammarList> = ({
  seo,
  url,
  contact,
  menu,
  list,
}: IGrammarList) => (
  <LayoutList seo={seo} url={url} contact={contact} menu={menu}>
    <h1>Грамматика арабского языка</h1>
    <ul>
      {list &&
        list.map((item: TGrammarMenuItem) => {
          return (
            <Li key={item.id}>
              <Link href={"/" + url + "/" + item.code}>
                <a>{item.name}</a>
              </Link>
            </Li>
          );
        })}
    </ul>
  </LayoutList>
);

export default GrammarList;
