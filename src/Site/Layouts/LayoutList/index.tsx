import React, { FC } from "react";
import { ListPage } from "./elements";
import LayoutPage from "@site/Layouts/LayoutPage";
import { TContact, TMobileMenu, TSeo } from "@site/types";

interface ILayoutList {
  seo: TSeo;
  url: string;
  contact: TContact;
  menu: TMobileMenu[];
  children: JSX.Element | JSX.Element[];
}

const LayoutList: FC<ILayoutList> = ({
  seo,
  url,
  contact,
  menu,
  children,
}: ILayoutList) => (
  <LayoutPage seo={seo} url={url} contact={contact} menu={menu}>
    <ListPage>{children}</ListPage>
  </LayoutPage>
);

export default LayoutList;
