import styled from "@emotion/styled";

export const ListPage = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 10px;
  a {
    font-size: 1.3rem;
    display: flex;
    margin: 3px 0;
    &:hover {
      text-decoration: underline;
    }
  }
`;
