import styled from "@emotion/styled";
import {
  mobileXs,
  desktopXs,
  tabletLg,
  desktopSm,
  mobileLg,
} from "@site/media";

const scrollItem = `
  display: flex;
  padding: 2px;
  transition: 0.3s;
  font-weight: 300;
  text-align: left;
  align-items: center;
  padding: 0;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
  @media (max-width: ${tabletLg()}) {
    text-align: center;
    justify-content: center;
    padding: 3px 4px;
    flex-direction: column;
  }
  @media (max-width: ${mobileLg()}) {
    font-size: 12px;
  }
  @media (max-width: ${mobileXs()}) {
    font-size: 10px;
  }
`;

const icon = `
  font-size: 1.1rem;
  margin-right: 0.8rem;
  display: flex;
  @media (max-width: ${tabletLg()}) {
    text-align: center;
    justify-content: center;
    flex-direction: column;
    margin: 0;
  }
`;

export const ScrollContainer = styled.div`
  min-width: 200px;
  padding: 10px 0;
  background: var(--main-bg);
  z-index: 1000;
  @media (min-width: ${desktopXs()}) {
    min-width: 250px;
  }
  @media (max-width: ${tabletLg()}) {
    position: fixed;
    display: flex;
    bottom: 0;
    width: 100%;
    padding: 0;
    box-shadow: 1px 0px 5px #b9b9b9;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 5px;
  }
`;
export const ScrollMenu = styled.div`
  position: sticky;
  top: 70px;
  border-left: 1px solid var(--borderColor);
  padding: 20px;
  flex: 1;
  @media (max-width: ${tabletLg()}) {
    padding: 0;
    display: flex;
    justify-content: space-around;
  }
`;

export const ScrollItem = styled.a`
  ${scrollItem}
`;

export const ScrollItemLink = styled.div`
  ${scrollItem}
`;

export const MenuBtn = styled.a`
  display: flex;
  align-items: center;
  margin-right: 15px;
  width: 20px;
  height: 20px;
  background-size: contain;
  background-image: url(data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTEyIDUxMiIgaGVpZ2h0PSI1MTIiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Im00NjQuODgzIDY0LjI2N2gtNDE3Ljc2NmMtMjUuOTggMC00Ny4xMTcgMjEuMTM2LTQ3LjExNyA0Ny4xNDkgMCAyNS45OCAyMS4xMzcgNDcuMTE3IDQ3LjExNyA0Ny4xMTdoNDE3Ljc2NmMyNS45OCAwIDQ3LjExNy0yMS4xMzcgNDcuMTE3LTQ3LjExNyAwLTI2LjAxMy0yMS4xMzctNDcuMTQ5LTQ3LjExNy00Ny4xNDl6Ii8+PHBhdGggZD0ibTQ2NC44ODMgMjA4Ljg2N2gtNDE3Ljc2NmMtMjUuOTggMC00Ny4xMTcgMjEuMTM2LTQ3LjExNyA0Ny4xNDkgMCAyNS45OCAyMS4xMzcgNDcuMTE3IDQ3LjExNyA0Ny4xMTdoNDE3Ljc2NmMyNS45OCAwIDQ3LjExNy0yMS4xMzcgNDcuMTE3LTQ3LjExNyAwLTI2LjAxMy0yMS4xMzctNDcuMTQ5LTQ3LjExNy00Ny4xNDl6Ii8+PHBhdGggZD0ibTQ2NC44ODMgMzUzLjQ2N2gtNDE3Ljc2NmMtMjUuOTggMC00Ny4xMTcgMjEuMTM3LTQ3LjExNyA0Ny4xNDkgMCAyNS45OCAyMS4xMzcgNDcuMTE3IDQ3LjExNyA0Ny4xMTdoNDE3Ljc2NmMyNS45OCAwIDQ3LjExNy0yMS4xMzcgNDcuMTE3LTQ3LjExNyAwLTI2LjAxMi0yMS4xMzctNDcuMTQ5LTQ3LjExNy00Ny4xNDl6Ii8+PC9zdmc+);
  @media (min-width: ${tabletLg(+1)}) {
    display: none;
  }
`;
export const OnPageText = styled.div`
  font-weight: 700;
  margin-bottom: 10px;
  @media (max-width: ${tabletLg()}) {
    display: none;
  }
`;

export const NextPage = styled.a`
    background: var(--bthBg);
    padding: 20px 25px;
    font-size: 23px;
    border-radius: 3px;
    cursor: pointer;
    display: inline-block;
    transition: 0.5s;
    color: #ffffff;
    font-weight: 200;
    box-shadow: 4px 3px 16px 2px #cecdcd;
    &:hover{
        background: var(--bthBg-hover);
    };
}
`;
export const PageFooter = styled.div`
  text-align: center;
  margin-top: 40px;
  margin-bottom: 80px;
`;

export const Article = styled.article`
  max-width: 100%;
  width: 100%;
  margin: 0 auto;
  @media (min-width: ${tabletLg(+1)}) {
    max-width: 650px;
  }
  @media (min-width: ${desktopXs()}) {
    max-width: 800px;
    padding: 0 30px;
  }
  @media (min-width: ${desktopSm()}) {
    max-width: 1100px;
  }
`;

export const DetailPage = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const MobileHarfs = styled.div`
  display: flex;
  flex-direction: column;
  position: sticky;
  top: 32px;
  box-shadow: -4px 0px 6px #a3a3a3;
  height: calc(100vh - 65px);
  overflow-x: hidden;
  overflow-y: scroll;
  align-items: center;
  padding-top: 0.5rem;
  padding-bottom: 1.5rem;
  scrollbar-width: none;
  min-width: 2.5rem;
  &::-webkit-scrollbar {
    display: none;
  }
  a {
    display: block;
    font-size: 1.5rem;
    padding-left: 0.2rem;
    padding-right: 0.2rem;
  }
  @media (min-width: ${tabletLg(+1)}) {
    display: none;
  }
`;

export const Content = styled.div`
  flex: 1;
  padding: 0 10px 2.5rem;
  border-bottom: 1px solid var(--borderColor);
`;

export const ActiveItem = styled.div`
  background: var(--bthMenu-hover);
  border-radius: 15px;
  width: 90%;
  text-align: center;
  display: flex;
  justify-content: center;
`;

export const AlphItem = styled.div`
  border-top: 1px solid var(--borderColor);
`;

export const Icon = styled.div`
  ${icon}
  color: #9a9a9a;
`;
export const PageIcon = styled.div`
  ${icon}
  color: var(--bthBg);
`;

export const ScrollText = styled.div`
  margin-bottom: 5px;
  @media (max-width: ${tabletLg()}) {
    font-size: 0.8rem;
    margin-bottom: 0;
    line-height: 1;
  }
`;
