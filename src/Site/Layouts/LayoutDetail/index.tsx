import React from "react";
import Link from "next/link";
import Icons from "@components/Elements/icons";
import LayoutPage from "@site/Layouts/LayoutPage";
import { LessenMenu } from "@site/Detail/LessenMenu";
import {
  Article,
  ScrollMenu,
  ScrollItem,
  ScrollContainer,
  OnPageText,
  NextPage,
  PageFooter,
  DetailPage,
  MobileHarfs,
  Content,
  ActiveItem,
  ScrollItemLink,
  Icon,
  ScrollText,
  PageIcon,
} from "./elements";

import {
  TSeo,
  TContact,
  TMobileMenu,
  TScrollMenu,
  TMenuItem,
  TOrthographyMenuItem,
  TScrollMenuItem,
} from "@site/types";

interface ILayoutDetail {
  seo: TSeo;
  url: string;
  contact: TContact;
  currentPage: string;
  children: JSX.Element;
  nextPage: string;
  isAlphabet: boolean;
  list: TMenuItem[];
  scrollMenu: TScrollMenu;
  menu: TMobileMenu[];
}

const LayoutDetail = ({
  seo,
  url,
  contact,
  list,
  currentPage,
  children,
  nextPage,
  scrollMenu,
  isAlphabet,
  menu,
}: ILayoutDetail) => (
  <LayoutPage seo={seo} url={url} contact={contact} menu={menu}>
    <LessenMenu url={url} list={list} currentPage={currentPage} />
    <Article>
      <DetailPage>
        <Content>{children}</Content>
        {isAlphabet ? (
          <MobileHarfs>
            {list
              .filter((item: TOrthographyMenuItem) => item.isAlphabet)
              .map(({ code, harfFree, id }: TOrthographyMenuItem) => {
                if (code === currentPage) {
                  return (
                    <React.Fragment key={id}>
                      <ActiveItem>
                        <Link href={"/" + url + "/" + code}>{harfFree}</Link>
                      </ActiveItem>
                    </React.Fragment>
                  );
                }
                return (
                  <React.Fragment key={id}>
                    <Link href={"/" + url + "/" + code}>{harfFree}</Link>
                  </React.Fragment>
                );
              })}
          </MobileHarfs>
        ) : null}
      </DetailPage>
      <PageFooter>
        {nextPage && (
          <Link href={"/" + url + "/" + nextPage}>
            <NextPage>Следующий урок</NextPage>
          </Link>
        )}
      </PageFooter>
    </Article>
    <ScrollContainer>
      <ScrollMenu>
        {scrollMenu && (
          <>
            <OnPageText>На этой странице</OnPageText>
            {Array.from(scrollMenu).map((item: TScrollMenuItem) => (
              <React.Fragment key={item.code}>
                {item?.page ? (
                  <Link href={item.code}>
                    <ScrollItemLink>
                      <PageIcon>{Icons[item.icon]}</PageIcon>
                      <ScrollText>{item.name}</ScrollText>
                    </ScrollItemLink>
                  </Link>
                ) : (
                  <ScrollItem key={item.code} href={item.code}>
                    <Icon>{Icons[item.icon]}</Icon>
                    <ScrollText>{item.name}</ScrollText>
                  </ScrollItem>
                )}
              </React.Fragment>
            ))}
          </>
        )}
      </ScrollMenu>
    </ScrollContainer>
  </LayoutPage>
);

export default LayoutDetail;
