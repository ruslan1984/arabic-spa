import React, { FC } from "react";
import Header from "@site/Header";
import Footer from "@site/Footer";
import { Page } from "./styles";
import { TSeo, TMobileMenu, TContact } from "@site/types";

interface ILayoutPage {
  seo: TSeo;
  url: string;
  contact: TContact;
  menu: TMobileMenu[];
  children: JSX.Element | JSX.Element[] | string;
}

const LayoutPage: FC<ILayoutPage> = ({
  seo,
  url,
  contact,
  menu,
  children,
}: ILayoutPage) => (
  <>
    <Header seo={seo} url={url} contact={contact} menu={menu} />
    <Page>{children}</Page>
    <Footer />
  </>
);

export default LayoutPage;
