import styled from "@emotion/styled";
import { tabletLg } from "@site/media";

export const Page = styled.div`
  display: flex;
  padding-top: 60px;
  @media (max-width: ${tabletLg()}) {
    padding-top: 35px;
    flex-wrap: wrap;
  }
`;
