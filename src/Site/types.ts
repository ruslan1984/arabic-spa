export type TSeo = {
  name: string;
  title: string;
  metaKeywords: string;
  metaDescription: string;
};

// Мобильное Меню
export type TMobileMenu = {
  list: TMobileMenu[];
} & TMenuItem;

export type TMenuItem = TGrammarMenuItem | TOrthographyMenuItem;

export type TGrammarMenuItem = {
  id: number;
  code: string;
  name: string;
  sort: number;
};

export type TOrthographyMenuItem = {
  harfFree?: string;
  isAlphabet?: number;
  transcription?: string;
} & TGrammarMenuItem;

// Контакты
export type TContact = Partial<
  Record<"phone" | "email" | "whatsapp" | "sendto", string>
>;

export type TScrollMenuItem = Record<"code" | "name" | "icon", string> & {
  page?: boolean;
};
export type TScrollMenu = TScrollMenuItem[];

/* **** Words types **** */
export type TWordType = "figl" | "ism" | "harf";
export type TFigSimpol = "A" | "I" | "U";
export type TRigRusSimpol = "А" | "И" | "У";

export type TWord = {
  id: number;
  created_at: string;
  updated_at: string;
  arWord: string;
  rusWord: string;
  wordType: TWordType;
  lessonId: number;
  arWordMn?: string;
  rusWordMn?: string;
  deletedAt?: string;
  figSimpol: TFigSimpol;
  figRusSimpol: TRigRusSimpol;
};

export type TWordList = { [key in TWordType]: TWord[] };
/* ************* */

export type TQuestion = {
  id: number;
  created_at: string;
  updated_at: string;
  deletedAt?: string;
  name: string;
  text: string;
};
/* **** Арабские тексты **** */

export type TArabicSentence = TArabicSentenceItem[];

export type TArabicSentenceItem = {
  id: number;
  ar: string;
  rus: string;
  comment?: string;
};

/* ************* */

export type TTestWord = { word: string; translate: string };
