import styled from "@emotion/styled";
import { desktopMd, desktopXs, tabletLg } from "@site/media";

export const LessenMenuStyle = styled.ul`
  min-width: 230px;
  max-width: 230px;
  margin: 0;
  position: sticky;
  top: 40px;
  border-right: 1px solid var(--borderColor);
  padding: 60px 10px 80px;
  display: none;
  @media (min-width: ${tabletLg(1)}) {
    padding: 20px 10px 80px;
    height: calc(100vh - 40px);
    overflow: scroll;
    display: block;
  }
  @media (min-width: ${desktopXs()}) {
    min-width: 300px;
    max-width: 300px;
    padding: 20px 20px 80px 30px;
  }
  @media (min-width: ${desktopMd()}) {
    min-width: 400px;
    max-width: 400px;
    padding: 40px 40px 80px;
  }
  @media (max-width: ${tabletLg()}) {
    min-width: 100%;
    max-width: 100%;
    order: 1;
  }

  // height: calc(100vh - 40px);
  // overflow-y: scroll;
  // ::-webkit-scrollbar {
  //     -webkit-appearance: none;
  //     width: 7px;
  //   }
  // ::-webkit-scrollbar-thumb {
  //   border-radius: 4px;
  //   background-color: rgba(0, 0, 0, .5);
  //   -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);
  // }
`;

export const Li = styled.li`
  padding: 5px 0;
  position: relative;
  a {
    display: flex;
    transition: 0.3s;
    font-size: 1.1rem;
    will-change: transform;
    &:hover {
      //font-weight: 500;
      transform: translateX(10px);
    }
    ${(props: { selected?: boolean }) => {
      if (props.selected) {
        return "font-weight: 500; transform: translateX(10px); font-size: 1.2rem";
      }
    }};
  }
`;
