import Link from "next/link";
import { LessenMenuStyle, Li } from "./styles";
import { TMenuItem } from "@site/types";

interface ILessenMenu {
  url: string;
  list: TMenuItem[];
  currentPage: string;
}

export const LessenMenu = ({ url, list, currentPage }: ILessenMenu) => (
  <LessenMenuStyle id="menu">
    {list?.map((item: TMenuItem) => {
      return (
        <Li key={item.id} selected={item.code == currentPage}>
          <Link href={"/" + url + "/" + item.code}>
            <a>{item.name}</a>
          </Link>
        </Li>
      );
    })}
  </LessenMenuStyle>
);

export default LessenMenu;
