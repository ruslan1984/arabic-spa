import React, { FC } from "react";
import { TTestWord, TWordList } from "./types";
import WordsLearningTest from "@site/Components/WordsLearningTest";

interface IWordsTest {
  words: TWordList;
}

const WordsTest: FC<IWordsTest> = ({ words }: IWordsTest) => {
  const testWords: TTestWord[] = [];
  words?.ism?.forEach(({ arWord, rusWord, arWordMn, rusWordMn }) => {
    if (arWord && rusWord) {
      testWords.push({
        word: rusWord,
        translate: arWord,
      });
    }
    if (arWordMn && rusWordMn) {
      testWords.push({
        word: rusWordMn,
        translate: arWordMn,
      });
    }
  });
  words?.figl?.forEach(({ arWord, rusWord, arWordMn, rusWordMn }) => {
    if (arWord && rusWord) {
      testWords.push({
        word: rusWord,
        translate: arWord,
      });
    }
    if (arWordMn && rusWordMn) {
      testWords.push({
        word: rusWordMn,
        translate: arWordMn,
      });
    }
  });
  words?.harf?.forEach(({ arWord, rusWord, arWordMn, rusWordMn }) => {
    if (arWord && rusWord) {
      testWords.push({
        word: rusWord,
        translate: arWord,
      });
    }
    if (arWordMn && rusWordMn) {
      testWords.push({
        word: rusWordMn,
        translate: arWordMn,
      });
    }
  });

  return <WordsLearningTest words={testWords} />;
};

export default WordsTest;
