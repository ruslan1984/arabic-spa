import React, { FC } from "react";
import { WordLine, Word, WordsBlock, WordHeader } from "./styles";
import { TWordList, TWord } from "@site/types";

interface IWordsList {
  words: TWordList;
}

export const WordsList: FC<IWordsList> = ({ words }: IWordsList) =>
  (
    <>
      {words.ism && (
        <WordsBlock>
          <WordLine>
            <WordHeader>Мн число</WordHeader>
            <WordHeader>Ед число</WordHeader>
            <WordHeader>Имена</WordHeader>
          </WordLine>
          {words?.ism.map((item: TWord) => {
            return (
              <WordLine key={item.id}>
                <Word>{item.arWordMn}</Word>
                <Word>{item.arWord}</Word>
                <Word>{item.rusWord}</Word>
              </WordLine>
            );
          })}
        </WordsBlock>
      )}
      {words.figl && (
        <WordsBlock>
          <WordLine>
            <WordHeader></WordHeader>
            <WordHeader>Ед число</WordHeader>
            <WordHeader>Глаголы</WordHeader>
          </WordLine>
          {words?.figl.map((item: TWord) => {
            return (
              <WordLine key={item.id}>
                <Word>{item.figRusSimpol}</Word>
                <Word>{item.arWord}</Word>
                <Word>{item.rusWord}</Word>
              </WordLine>
            );
          })}
        </WordsBlock>
      )}
      {words.harf && (
        <WordsBlock>
          <WordLine>
            <WordHeader></WordHeader>
            <WordHeader>Ед число</WordHeader>
            <WordHeader>Частицы</WordHeader>
          </WordLine>
          {words?.harf.map((item: TWord) => {
            return (
              <WordLine key={item.id}>
                <Word></Word>
                <Word>{item.arWord}</Word>
                <Word>{item.rusWord}</Word>
              </WordLine>
            );
          })}
        </WordsBlock>
      )}
    </>
  ) || null;

export default WordsList;
