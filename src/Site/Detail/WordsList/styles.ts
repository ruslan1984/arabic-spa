import styled from "@emotion/styled";
import { mobileLg, tabletSm } from "@site/media";

export const WordLine = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  &::nth-of-type(1) {
    background: var(--bthMenu-hover);
  }
`;

export const WordStyie = styled.div`
  padding: 5px;
  text-align: center;
  font-size: 1.7rem;
  width: 30%;
  &:last-child {
    width: 40%;
    text-align: right;
  }
`;

export const Word = styled(WordStyie)`
  @media (max-width: ${tabletSm()}) {
    &:last-child {
      font-size: 1.3rem;
    }
  }
  @media (max-width: ${mobileLg()}) {
    &:last-child {
      font-size: 1rem;
    }
  }
`;

export const WordHeader = styled(WordStyie)`
  background: #eee;
  min-height: 40px;
  @media (max-width: ${mobileLg()}) {
     {
      font-size: 1.3rem;
    }
  }
`;

export const WordsBlock = styled.div`
  margin: 30px 0;
`;
