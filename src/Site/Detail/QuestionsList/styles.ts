import styled from "@emotion/styled";

export const Question = styled.div`
  display: flex;
  font-weight: 700;
  font-size: 1.4rem;
  margin-bottom: 26px;
`;
export const Num = styled.div`
  margin-right: 5px;
`;
export const Answers = styled.div`
  font-size: 1.6rem;
  p {
    font-size: 1.6rem;
  }
`;

export const QuestionBlock = styled.div`
  margin-bottom: 50px;
`;
