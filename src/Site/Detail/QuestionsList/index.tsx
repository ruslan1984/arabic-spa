import React, { FC } from "react";
import { Question, Answers, Num, QuestionBlock } from "./styles";
import { TQuestion } from "@site/types";

interface IQuestionsList {
  questions?: TQuestion[];
}

const QuestionsList: FC<IQuestionsList> = ({
  questions,
}: IQuestionsList): JSX.Element => {
  if (!questions) return <></>;
  return (
    <>
      {questions?.map((item: TQuestion, number: number) => (
        <QuestionBlock key={item.id}>
          <Question>
            <Num> {String(number + 1)}</Num>
            <div dangerouslySetInnerHTML={{ __html: item.name }} />
          </Question>
          <Answers dangerouslySetInnerHTML={{ __html: item.text }} />
        </QuestionBlock>
      ))}
    </>
  );
};

export default QuestionsList;
