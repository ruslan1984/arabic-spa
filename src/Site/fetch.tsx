import { fetchOk } from "@/helpers/fetch";

const headers = {
  "Content-Type": "application/json;charset=utf-8",
};

const host = process?.env?.REACT_APP_HOST ?? "";

export const fetchGet = async (url: string) => {
  let fetchData;
  try {
    fetchData = await fetch(host + url, {
      headers,
    });
    const status: number = await fetchData.status;
    if (!fetchOk(status)) {
      throw new Error("status " + fetchData.status);
    }
    if (status === 204) {
      return { status };
    } else {
      const body = await fetchData.json();
      return { body, status };
    }
  } catch (err) {
    console.error(err);
    return { status: 404 };
  }
};

export const fetchPost = async (
  url: string,
  data: { [key in string]?: string }
) => {
  let fetchData;
  try {
    fetchData = await fetch(host + url, {
      method: "POST",
      headers,
      body: JSON.stringify(data),
    });
  } catch (err) {
    console.error(err);
    return { status: 404 };
  }
  if (fetchOk(fetchData.status)) {
    const status = await fetchData.status;
    if (status === 204) {
      return { status };
    } else {
      const body = await fetchData.json();
      return { body, status };
    }
  } else {
    return { status: 404 };
  }
};
