import React, { FC } from "react";
import Link from "next/link";
import LayoutPage from "@site/Layouts/LayoutPage";
import { WordsPageLink, H1, Container } from "@site/SitePages/styles";
import { TSeo, TMobileMenu, TContact } from "@site/types";
import WordsDictionaryLearning from "@site/Components/WordsDictionaryLearning";

interface IArabicTexts {
  url: string;
  seo: TSeo;
  menu: TMobileMenu[];
  contact: TContact;
  code?: string;
}

export const ArabicTexts: FC<IArabicTexts> = ({
  seo,
  contact,
  menu,
  url,
}: IArabicTexts) => {
  return (
    <LayoutPage seo={seo} contact={contact} menu={menu} url={url}>
      <Container>
        <WordsPageLink>
          <Link href="/arabic-words/1">Словари уроков</Link>
        </WordsPageLink>
        <H1>Заучивание слов</H1>
        <WordsDictionaryLearning />
      </Container>
    </LayoutPage>
  );
};

export default ArabicTexts;
