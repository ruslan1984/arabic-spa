import styled from "@emotion/styled";

export const Container = styled.div`
  max-width: 1200px;
  padding: 5px;
  margin: 0 auto;
  width: 100%;
`;

export const Title = styled.div`
  font-size: 1.7rem;
  margin-bottom: 10px;
  text-align: center;
`;

export const H1 = styled.h1`
  text-align: center;
  font-size: 2rem;
  font-style: italic;
  margin-bottom: 30px;
`;

export const WordsPageLink = styled.div`
  text-decoration: underline;
  padding: 10px;
  font-size: 1.3rem;
`;
