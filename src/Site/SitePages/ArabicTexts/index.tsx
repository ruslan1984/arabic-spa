import React, { FC } from "react";
import Link from "next/link";
import Pagination from "@components/Pagination";
import LayoutPage from "@site/Layouts/LayoutPage";
import ArabicSentence from "@modules/ArabicText";
import { Title, H1, Container } from "@site/SitePages/styles";
import { ArabicTextBlock } from "./styles";
import {
  TSeo,
  TArabicTextsData,
  TArabicText,
  TMobileMenu,
  TContact,
} from "@site/SitePages/ArabicTexts/types";
import { useRouter } from "next/router";

interface IArabicTexts {
  url: string;
  seo: TSeo;
  data: TArabicTextsData;
  menu: TMobileMenu[];
  contact: TContact;
}

export const ArabicTexts: FC<IArabicTexts> = ({
  seo,
  contact,
  data,
  menu,
  url,
}: IArabicTexts) => {
  const router = useRouter();
  const pageClick = (page: number) => {
    router.push(`/${url}/${page}`);
  };
  return (
    <LayoutPage seo={seo} contact={contact} menu={menu} url={url}>
      <Container>
        <>
          <H1>{seo.name}</H1>
          {data.arabicTexts.map(
            ({ code, name, arabicSentence }: TArabicText) => (
              <ArabicTextBlock key={code}>
                <Title>
                  Арабский текст урока{" "}
                  <u>
                    <Link href={`/grammatika/${code}`}>
                      <a>{name}</a>
                    </Link>
                  </u>
                </Title>
                <ArabicSentence arabicText={arabicSentence} />
              </ArabicTextBlock>
            )
          )}
          <Pagination
            pagesCount={data.pagesCount}
            currentPage={Number(data.page)}
            pageClick={pageClick}
          />
        </>
      </Container>
    </LayoutPage>
  );
};

export default ArabicTexts;
