export type { TSeo, TMobileMenu, TContact } from "@site/types";
import { TArabicSentence } from "@site/types";

export type TArabicTextsData = {
  pagesCount: number;
  page: number;
  arabicTexts: TArabicText[];
};

export type TArabicText = {
  id: number;
  name: string;
  code: string;
  arabicSentence: TArabicSentence;
};
