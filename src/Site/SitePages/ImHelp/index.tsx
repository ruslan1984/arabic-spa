import React, { FC } from "react";
import LayoutPage from "@site/Layouts/LayoutPage";
import { Title, Container, Img, Content, Btn, Footer } from "./styles";
import { TSeo, TMobileMenu, TContact } from "@site/types";
import Image from "next/image";
import myImg from "./img/my_photo.jpg";
interface IImHelp {
  seo: TSeo;
  contact: TContact;
  menu: TMobileMenu[];
  url: string;
}

export const ImHelp: FC<IImHelp> = ({ url, seo, contact, menu }: IImHelp) => {
  return (
    <LayoutPage seo={seo} contact={contact} menu={menu} url={url}>
      <Container>
        <Title>Ассаламу галейкум дорогие братья и сестры!</Title>
        <Content>
          <Img>
            <Image src={myImg} width={100} height={100} layout="intrinsic" />
          </Img>
          <div>
            Очень рад, что интересуетесь изучением арабского языка и очень рад,
            что посетили наш сайт. Меня зовут Мухаметзянов Руслан. Я инициатор и
            разработчик проекта <u>Арабский Онлайн</u>. Планируется доработать
            инструменты для удобного изучения языка и доработать методическую
            часть. Я рассчитываю на Вашу помощь. Вместе мы можем больше!
            <Footer>
              <Btn target="_blank" href="https://pay.cloudtips.ru/p/20909a3b">
                Я помогу!
              </Btn>
            </Footer>
          </div>
        </Content>
      </Container>
    </LayoutPage>
  );
};

export default ImHelp;
