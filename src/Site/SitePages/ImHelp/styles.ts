import styled from "@emotion/styled";
import { tabletSm } from "@site/media";

export const Img = styled.div`
  min-width: 80px;
  max-width: 80px;
  min-height: 80px;
  max-height: 80px;
  width: 80px
  height: 80px;
  border-radius: 50%;
  background-size: contain;
  margin-right: 40px;
  margin-top: 5px;
  margin-bottom: 15px;
  display: flex;
  overflow: hidden;
  @media (max-width: ${tabletSm()}) {
    margin-right: 15px;
  }
`;

export const Title = styled.div`
  text-align: center;
  font-size: 1.5rem;
  font-family: "Poiret One", cursive;
  font-style: italic;
  margin-bottom: 20px;
`;
export const Container = styled.div`
  font-style: italic;
  max-width: 800px;
  padding: 15px;
  margin: 30px auto;
  width: 100%;
  font-family: "Poiret One", cursive;
  font-size: 1.2rem;
`;

export const Content = styled.div`
  display: flex;
  font-family: inherit;
  @media (max-width: ${tabletSm()}) {
    flex-direction: column;
    align-items: center;
    text-align: center;
  }
`;
export const Btn = styled.a`
  display: block;
  background: var(--bthBg);
  padding: 20px 80px;
  font-size: 23px;
  border-radius: 3px;
  cursor: pointer;
  display: inline-block;
  transition: 0.5s;
  color: #ffffff;
  font-weight: 200;
  box-shadow: 4px 3px 16px 2px #cecdcd;
  margin: 10px auto;
  &:hover {
    background: var(--bthBg-hover);
  }
`;

export const Footer = styled.div`
  text-align: center;
  margin: 30px 0;
`;
