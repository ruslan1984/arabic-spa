import { TWordList } from "@site/types";

export type TArabicWordsData = {
  words: {
    list: TWordList;
    name: string;
    code: string;
  }[];
  pagesCount: number;
  page: number;
};

export type { TSeo, TMobileMenu, TContact } from "@site/types";
