import styled from "@emotion/styled";

export const ArabicWordBlock = styled.div`
  padding-bottom: 20px;
  margin-bottom: 35px;
  border-bottom: 1px solid #eee;
`;
