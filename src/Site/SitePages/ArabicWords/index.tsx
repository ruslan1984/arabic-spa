import React, { FC } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import WordsList from "@site/Detail/WordsList";
import WordsTest from "@site/Detail/WordsTest";
import Pagination from "@components/Pagination";
import LayoutPage from "@site/Layouts/LayoutPage";
import { Title, H1, Container, WordsPageLink } from "@site/SitePages/styles";
import { ArabicWordBlock } from "./styles";
import { TArabicWordsData, TSeo, TMobileMenu, TContact } from "./types";

interface IArabicTexts {
  url: string;
  seo: TSeo;
  data: TArabicWordsData;
  menu: TMobileMenu[];
  contact: TContact;
  code?: string;
}

export const ArabicTexts: FC<IArabicTexts> = ({
  seo,
  contact,
  data,
  menu,
  url,
}: IArabicTexts) => {
  const router = useRouter();
  const pageClick = (page: number) => {
    router.push(`/${url}/${page}`);
  };
  return (
    <LayoutPage seo={seo} contact={contact} menu={menu} url={url}>
      <Container>
        <>
          <WordsPageLink>
            <Link href="/arabic-words">Заучивание слов</Link>
          </WordsPageLink>
          <H1>{seo.name}</H1>
          {data.words.map((item) => (
            <ArabicWordBlock key={item.code}>
              <Title>
                Слова урока{" "}
                <u>
                  <Link href={`/grammatika/${item.code}`}>
                    <a>{item.name}</a>
                  </Link>
                </u>
              </Title>
              <WordsList words={item.list} />
              <WordsTest words={item.list} />
            </ArabicWordBlock>
          ))}
          <Pagination
            pagesCount={data.pagesCount}
            currentPage={Number(data.page)}
            pageClick={pageClick}
          />
        </>
      </Container>
    </LayoutPage>
  );
};

export default ArabicTexts;
