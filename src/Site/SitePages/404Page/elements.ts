import styled from "@emotion/styled";
import { tabletLg, mobileLg } from "@site/media";

export const Page = styled.div`
  text-align: center;
  padding: 50px 20px;
  @media (min-width: ${tabletLg(+1)}) {
    padding: 100px 50px;
  }
`;

export const Title = styled.div`
  font-size: 8rem;
  @media (min-width: ${tabletLg(+1)}) {
    font-size: 20rem;
  }
`;

export const Text = styled.div`
  font-size: 2rem;
  @media (min-width: ${tabletLg(+1)}) {
    font-size: 3rem;
  }
`;

export const UlTopMenu = styled.ul`
  margin: 0;
  padding: 1px;
`;

export const LiTopMenu = styled.li`
  a {
    color: #000;
    text-decoration: none;
    display: block;
    margin: 0 1px;
    padding: 5px;
    border-radius: 26px;
    transition: 0.5s;
    font-size: 2rem;
    @media (min-width: ${mobileLg(+1)}) {
      padding: 5px 15px;
    }
    @media (min-width: ${tabletLg(+1)}) {
      padding: 10px 20px;
    }

    &:hover {
      background: var(--bthMenu-hover);
    }
  }
`;
