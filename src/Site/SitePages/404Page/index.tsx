import React from "react";
import Link from "next/link";
import Header from "@site/Header";
import Footer from "@site/Footer";
import { Title, UlTopMenu, LiTopMenu, Page, Text } from "./elements";
import { TSeo, TMobileMenu, TContact } from "@site/types";

interface PropsIndex {
  menu: TMobileMenu[];
  seo: TSeo;
  url: string;
  contact: TContact;
}

const Page404 = ({ menu, seo, url, contact }: PropsIndex) => (
  <>
    <Header
      seo={{ ...seo, title: "Cтраница не найдена" }}
      url={url}
      contact={contact}
      menu={menu}
    />
    <Page>
      <Text>Cтраница не найдена</Text>
      <Title>404</Title>
      <hr />
      <UlTopMenu>
        <LiTopMenu>
          <Link href="/arabskie-bukvy">
            <a>Буквы</a>
          </Link>
        </LiTopMenu>
        <LiTopMenu>
          <Link href="/grammatika">
            <a>Грамматика</a>
          </Link>
        </LiTopMenu>
        <LiTopMenu>
          <Link href="/repetitor">
            <a>Репетитор</a>
          </Link>
        </LiTopMenu>
      </UlTopMenu>
    </Page>
    <Footer />
  </>
);

export default Page404;
