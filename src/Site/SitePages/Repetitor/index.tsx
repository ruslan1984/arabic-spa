import React, { FC, useState, useRef } from "react";
import { fetchPost } from "@site/fetch";
import { fetchOk } from "@/helpers/fetch";
import LayoutPage from "@site/Layouts/LayoutPage";
import { Container } from "@site/elements";
import { RepetitorForm } from "./Presenters/RepetitorForm";
import { FormOk, FormError } from "./styles";
import { TSeo, TMobileMenu, TContact } from "@site/types";

export interface IRepetitor {
  seo: TSeo;
  contact: TContact;
  menu: TMobileMenu[];
  url: string;
}

export const Repetitor: FC<IRepetitor> = ({
  seo,
  contact,
  menu,
  url,
}: IRepetitor) => {
  const [page, setPage] = useState(0);
  const nameRef = useRef<HTMLInputElement>(null);
  const phoneRef = useRef<HTMLInputElement>(null);

  const onSubmit = async () => {
    const name: string | undefined = nameRef.current?.value;
    const phone: string | undefined = phoneRef.current?.value;
    const fetchData = await fetchPost("/api/mail/tutor", { name, phone });
    if (fetchOk(fetchData.status)) {
      setPage(1);
    } else {
      setPage(2);
    }
  };

  return (
    <LayoutPage seo={seo} contact={contact} menu={menu} url={url}>
      <Container>
        {page === 0 && (
          <RepetitorForm
            nameRef={nameRef}
            phoneRef={phoneRef}
            onSubmit={onSubmit}
          />
        )}
        {page === 1 && <FormOk>Ваша заявка отправлена</FormOk>}
        {page === 2 && <FormError>Произошла ошибка</FormError>}
      </Container>
    </LayoutPage>
  );
};

export default Repetitor;
