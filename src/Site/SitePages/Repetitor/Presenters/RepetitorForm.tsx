import React, { FC, FormEvent, RefObject, useState } from "react";
import { Button } from "@site/elements";
import { RepetitorBlock } from "./styles";
import Input from "@components/Input";

interface RepetitorFormProps {
  nameRef: RefObject<HTMLInputElement>;
  phoneRef: RefObject<HTMLInputElement>;
  onSubmit: () => void;
}

export const RepetitorForm: FC<RepetitorFormProps> = ({
  nameRef,
  phoneRef,
  onSubmit,
}: RepetitorFormProps) => {
  const [nameError, setNameError] = useState("");
  const [phoneError, setPhoneError] = useState("");
  const onSubmitClick = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const name: string | undefined = nameRef.current?.value;
    const phone: string | undefined = phoneRef.current?.value;
    if (!name) {
      setNameError("Заполните поле");
    }
    if (!phone) {
      setPhoneError("Заполните поле");
    }
    if (!name || !phone) return;
    onSubmit();
  };

  const onFocusClick = () => {
    setNameError("");
    setPhoneError("");
  };

  return (
    <RepetitorBlock>
      <h1>Репетитор по арабскому</h1>
      <form onSubmit={onSubmitClick}>
        <Input
          inputRef={nameRef}
          onFocus={onFocusClick}
          name="name"
          type="text"
          label="Ваше имя"
          error={nameError}
        />
        <Input
          inputRef={phoneRef}
          onFocus={onFocusClick}
          name="phone"
          type="tel"
          label="Ваш телефон"
          error={phoneError}
        />
        <Button type="submit">Подать заявку</Button>
      </form>
    </RepetitorBlock>
  );
};

export default RepetitorForm;
