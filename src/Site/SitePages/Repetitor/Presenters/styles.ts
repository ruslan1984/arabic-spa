import styled from "@emotion/styled";

export const RepetitorBlock = styled.div`
  max-width: 500px;
  margin: 50px 0;
`;

export const ErrorBlock = styled.div`
  max-height: 50px;
  margin: 10px 0;
`;
