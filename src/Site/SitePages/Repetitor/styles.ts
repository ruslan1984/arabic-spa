import styled from "@emotion/styled";

export const FormOk = styled.div`
  font-size: 50px;
  padding: 100px 0;
`;

export const FormError = styled.div`
  font-size: 50px;
  color: red;
  padding: 100px 0;
`;
