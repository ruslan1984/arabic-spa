import styled from "@emotion/styled";
import { FaTelegram } from "react-icons/fa";
import { tabletLg, mobileLg } from "@site/media";
import { imHelpIcon } from "@assets/icons/imHelp";
import { whatsAppIcon } from "@assets/icons/whatsApp";

export const HeaderStyle = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-shadow: 1px 0px 5px #b9b9b9;
  padding: 0 5px;
  z-index: 1400;
  position: fixed;
  left: 0;
  width: 100%;
  background: var(--main-bg);
  min-height: 35px;
  @media (min-width: ${tabletLg(+1)}) {
    padding: 5px 30px;
  }
`;

export const UlTopMenu = styled.ul`
  margin: 0;
  padding: 1px;
  display: none;
  @media (min-width: ${tabletLg(+1)}) {
    padding: 5px;
    display: flex;
  }
`;

export const LiTopMenu = styled.li`
  a {
    color: #111;
    text-decoration: none;
    display: block;
    margin: 0 1px;
    padding: 5px;
    border-radius: 26px;
    transition: 0.5s;
    @media (min-width: ${mobileLg(+1)}) {
      padding: 5px 15px;
    }
    @media (min-width: ${tabletLg(+1)}) {
      padding: 10px 20px;
    }
    &:hover {
      background: var(--bthMenu-hover);
      color: #676767;
    }
    ${({ active, isBorder }: { active?: boolean; isBorder?: boolean }) =>
      (active && "background: var(--bthMenu-hover);") ||
      (isBorder && "border: 1px solid var(--bthMenu-hover);")}};
  }
`;

export const Logo = styled.div`
  width: 100px;
  a {
    font-family: "Poiret One", cursive;
    text-align: center;
    font-weight: 600;
    font-size: 16px;
    line-height: 0.9;
    @media (min-width: ${tabletLg(+1)}) {
      font-size: 22px;
    }
  }
`;
export const Contacts = styled.div`
  display: flex;
  justify-content: center;
  @media (min-width: ${tabletLg(+1)}) {
    min-width: 100px;
  }
`;

export const WhotsApp = styled.a`
  background-repeat: no-repeat;
  width: 25px;
  height: 25px;
  display: flex;
  background-size: contain;
  background-image: url(${whatsAppIcon});
  margin-right: 5px;
  @media (min-width: ${tabletLg(+1)}) {
    width: 35px;
    height: 35px;
  }
`;

export const Telegram = styled(FaTelegram)`
  font-size: 25px;
  color: #3ea5e4;
  @media (min-width: ${tabletLg(+1)}) {
    font-size: 35px;
  }
`;

export const RightBlock = styled.div`
  display: flex;
  width: 100px;
  justify-content: flex-end;
  @media (min-width: ${tabletLg(+1)}) {
    display: none;
  }
`;

export const ImHelpIcon = styled.div`
  background-repeat: no-repeat;
  width: 25px;
  height: 25px;
  display: flex;
  background-size: contain;
  background-image: url(${imHelpIcon});
  margin-right: 5px;
  cursor: pointer;
  margin-left: 25px;
  @media (min-width: ${tabletLg(+1)}) {
    width: 35px;
    height: 35px;
  }
`;
