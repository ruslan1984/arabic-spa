import React, { FC } from "react";
import Link from "next/link";

import {
  HeaderStyle,
  UlTopMenu,
  LiTopMenu,
  Logo,
  Contacts,
  RightBlock,
  WhotsApp,
  Telegram,
  ImHelpIcon,
} from "./styles";

import MobileMenu from "../MobileMenu";
import { TSeo, TMobileMenu, TContact } from "@site/types";
import Head from "./Head";

interface IHeader {
  seo: TSeo;
  url: string;
  contact: TContact;
  menu: TMobileMenu[];
}
export const Header: FC<IHeader> = ({ seo, url, contact, menu }: IHeader) => (
  <>
    <Head seo={seo} />
    <HeaderStyle>
      <Logo>
        <Link href="/">
          <a>Арабский онлайн</a>
        </Link>
      </Logo>
      <UlTopMenu>
        <LiTopMenu active={url === "arabskie-bukvy"}>
          <Link href="/arabskie-bukvy">
            <a>Буквы</a>
          </Link>
        </LiTopMenu>
        <LiTopMenu active={url === "grammatika"}>
          <Link href="/grammatika">
            <a>Грамматика</a>
          </Link>
        </LiTopMenu>
        <LiTopMenu active={url === "arabic-texts"}>
          <Link href="/arabic-texts/1">
            <a>Тексты</a>
          </Link>
        </LiTopMenu>
        <LiTopMenu active={url === "arabic-words"}>
          <Link href="/arabic-words/1">
            <a>Слова</a>
          </Link>
        </LiTopMenu>
        <LiTopMenu active={url === "repetitor"}>
          <Link href="/repetitor">
            <a>Репетитор</a>
          </Link>
        </LiTopMenu>

        <LiTopMenu active={url === "im-help"} isBorder>
          <Link href="/im-help">
            <a>Я помогу</a>
          </Link>
        </LiTopMenu>
      </UlTopMenu>

      <Contacts>
        {contact?.whatsapp && (
          <WhotsApp href={`https://wa.me/${contact.whatsapp}`} />
        )}
        <a href="https://tele.click/+79673702540">
          <Telegram />
        </a>
        <Link href="/im-help">
          <ImHelpIcon />
        </Link>
      </Contacts>
      <RightBlock>
        <MobileMenu menu={menu} />
      </RightBlock>
    </HeaderStyle>
  </>
);

export default Header;
