import React, { FC } from "react";
import HeadSeo from "next/head";
import { desktopSm, mobileMd, tabletMd, tabletLg } from "@site/media";
import { injectGlobal } from "@emotion/css";
import { TSeo } from "@site/types";
import { variables } from "@site/variables";

interface IHead {
  seo: TSeo;
}

const Head: FC<IHead> = ({ seo }: IHead) => (
  <HeadSeo>
    <>
      <title>{seo?.title || "Арабский язык"}</title>
      <link rel="shortcut icon" href="/favicon.svg" />
      <meta
        name="description"
        content={seo?.metaDescription || "Арабский язык"}
      />
      <meta name="keywords" content={seo?.metaKeywords || "Арабский язык"} />
      <meta name="viewport" content="initial-scale=1.0" />

      {injectGlobal`
                      @import url('https://fonts.googleapis.com/css2?family=Poiret+One&display=swap');
                      //@font-face {
                      //	font-family: 'Poiret One';
                      //	src: url('fonts/PoiretOne/PoiretOne-Regular.ttf');
                      //}
                      ${variables}
            * {
              box-sizing: border-box;
              padding: 0;
              font-family: inherit;
            }
  
    body{
        padding:0;
        margin:0;
        background: var(--main-bg);
        font-family: Sarabun, sans-serif;
        font-size: 14px;
        @media( min-width: ${mobileMd()} ){
          font-size: 16px;
        }
        @media (min-width: ${desktopSm()}){
          font-size: 18px;
        }
        @media (max-width: ${tabletLg()}){
          &::-webkit-scrollbar {
            display: none;
          }
          scrollbar-width: none;
        }
        
    }
    li{
      list-style: none;
          font-weight: 400;
          @media( min-width: ${tabletMd()} ){
              font-weight: 100;
          }
    }
    a{
      color:black;
      text-decoration: none;
    }
    table{
      margin: 10px 0;
      width: 100%;
      text-align: center;
      overflow-x: scroll;
      thead {
        background: #f3dbbb;
      }
      th {
        padding: 10px 2px;
        background: #bd853a;
        color: white;
        font-size: 1rem;
        font-weight: 400;
      }
      td{
        padding: 5px 2px;
      }
      tbody {
        background: #eac28d;
      }
      p { 
        text-indent: 0;
      }
    }
    h1{
      text-align: center;
      font-weight: 500;
      font-size: 1.5rem;
      @media( min-width: ${mobileMd(+1)} ){
        font-size: 2.1rem;
      }
    }
    h2{
      text-align: center;
      font-weight: 500;
      font-size: 1.4rem;
      @media( min-width: ${mobileMd(+1)} ){
        font-size: 1.8rem;
      }
      
    }
    .text-huge{
      font-size: 1.8rem;
    }
    object,
    img {
      max-width:100%;
      max-height: 85vh;
    }
    p{
      line-height: 1.5;
      font-weight: 400;
      text-align: justify;
      text-indent: 20px;
      font-size: 1.2rem;
          margin: 5px 0;
          
          @media( min-width: ${tabletMd()} ){
              font-weight: 200;
          }
    }
    .ql-align-center {
      text-align: center;
    }
  `}
    </>
  </HeadSeo>
);

export default Head;
