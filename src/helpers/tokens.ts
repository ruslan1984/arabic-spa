const authToken = "token";

export const getToken = (): string | null => {
  if (typeof window !== "undefined") {
    return localStorage.getItem(authToken);
  }
  return null;
};

export const setToken = (token: string): void => {
  if (typeof window !== "undefined") {
    localStorage.setItem(authToken, token);
  }
};

export const isAuth = (): boolean => {
  if (typeof window !== "undefined") {
    return localStorage.getItem(authToken) !== null;
  }
  return false;
};

export const clearToken = (): void => {
  if (typeof window !== "undefined") {
    localStorage.removeItem(authToken);
  }
};
