export const removeTags = (str: string): string => {
  if (str === undefined) return "";
  return str?.replace(/<\/?[^>]+>/g, "");
};

export const sleep = (x: number) => new Promise((r) => setTimeout(r, x));

export const shuffle = <T>(array: T[]): T[] => {
  if (array.length < 1) return [];
  return [...array].sort(() => Math.random() - 0.5);
};

export const objToString = (data: any | undefined) => {
  if (data && Object.keys(data).length > 0) {
    return Object.entries(data).reduce((res, [key, value]) => {
      let str: string | any;
      if (typeof value === "string") {
        str = (value as string).replaceAll('"', "'");
        return res
          ? `${res}, ${key}: "${str ?? ""}"`
          : `${key}: "${str ?? ""}"`;
      } else {
        str = !value;
        return res ? `${res}, ${key}: ${value}` : `${key}: ${value}`;
      }
    }, "");
  }
};

export const strToObj = (data: any | undefined) => {
  if (data && Object.keys(data).length > 0) {
    const obj: any = {};
    return Object.entries(data).reduce((res, [key, value]) => {
      if (value && key && typeof value === "string") {
        const str: string = (value as string).replaceAll("'", '"');
        res[key] = str;
      } else {
        res[key] = value;
      }
      return res;
    }, obj);
  }
};

export const getMaxId = (data: { id: number }[]): number => {
  return data.reduce((cur, next) => (next.id > cur ? next.id : cur), 0);
};
export const getMinId = (data: { id: number }[]): number => {
  return data.reduce((cur, next) => (next.id < cur ? next.id : cur), -1);
};
