import { getToken } from "@helpers/tokens";
const headers = {
  "Content-Type": "application/json;charset=utf-8",
  Authorization: `Bearer ${getToken()}`,
};

export const fetchOk = (status: number) => status >= 200 && status < 300;

export async function fetchGet(url: string) {
  try {
    const fetchData = await fetch(process.env.REACT_APP_HOST + url, {
      headers,
    });
    const status: number = await fetchData.status;

    if (!fetchOk(fetchData.status)) {
      throw new Error("status " + fetchData.status);
    }
    if (status === 204) {
      return null;
    } else {
      const data = await fetchData.json();
      return data;
    }
  } catch (err) {
    throw err;
  }
}

export const fetchUpdate = async (
  method: string,
  url: string,
  updateData = null
) => {
  const header: any = {
    method,
    headers,
  };
  if (updateData) {
    header.body = JSON.stringify(updateData);
  }
  const fetchData = await fetch(process.env.REACT_APP_HOST + url, {
    ...header,
  });
  const status = await fetchData.status;
  if (!fetchOk(status)) {
    throw new Error("status " + status);
  }
  if (status === 204) {
    return null;
  } else {
    const data = await fetchData.json();
    return data;
  }
};

export const fetchPut = async (url: string, updateData: any = null) => {
  return await fetchUpdate("PUT", url, updateData);
};

export const fetchPost = async (url: string, updateData: any = null) => {
  return await fetchUpdate("POST", url, updateData);
};

export const fetchDelete = async (url: string) => {
  const fetchData = await fetch(process.env.REACT_APP_HOST + url, {
    method: "DELETE",
    headers,
  });
  const status = await fetchData.status;
  if (!fetchOk(status)) {
    throw new Error("status " + status);
  }
};
