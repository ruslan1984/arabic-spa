import { getToken } from "@helpers/tokens";

const headers = {
  "Content-Type": "application/json;charset=utf-8",
  Authorization: `Bearer ${getToken()}`,
};

const host = process.env.REACT_APP_HOST || "";
export async function Query(query: string) {
  try {
    return await fetch(host + "/graphql", {
      method: "POST",
      headers,
      body: JSON.stringify({
        query: `${query}`,
      }),
    })
      .then((response) => response.json())
      .then((result) => result.data)
      .catch((e) => {
        console.error(e.mesage);
      });
  } catch (err) {
    throw err;
  }
}
