import React from "react";
//import ReactDOM from "react-dom";
import ReactDOM from "react-dom/client";
import createSagaMiddleware from "redux-saga";
import { configureStore } from "@reduxjs/toolkit";
import { fork } from "redux-saga/effects";
import { Provider } from "react-redux";
import App from "@/App";
import reducer from "@store/reducers";
import { loginSaga } from "@modules/Auth/saga";
import { grammarListSaga } from "@modules/Grammar/List/saga";
import { grammarDetailSaga } from "@modules/Grammar/Detail/Base/saga";
import { grammarArabicSentenceSaga } from "@modules/Grammar/Detail/ArabicSentence/saga";
import { testCardSaga } from "@modules/Test/TestCard/saga";
import { testListSaga } from "@modules/Test/TestList/saga";
import { wordsGrammarSaga } from "@modules/Words/saga";
import { questionsGrammarSaga } from "@components/Questions/saga";
import { pageSettingsSaga } from "@components/Settings/saga";
import { orthographyListSaga } from "@modules/Orthography/List/saga";
import { orthographyDetailSaga } from "@modules/Orthography/Detail/Base/saga";
import { orthographyDetailHarfSaga } from "@modules/Orthography/Detail/Harf/saga";
import { orthographyDetailSoundSaga } from "@modules/Orthography/Detail/Sound/saga";
import { settingsSaga } from "@screens/Admin/Settings/saga";

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer,
  middleware: [sagaMiddleware],
});

function* rootSaga() {
  yield fork(loginSaga);
  yield fork(grammarListSaga);
  yield fork(grammarDetailSaga);
  yield fork(testCardSaga);
  yield fork(testListSaga);
  yield fork(wordsGrammarSaga);
  yield fork(questionsGrammarSaga);
  yield fork(pageSettingsSaga);
  yield fork(orthographyListSaga);
  yield fork(orthographyDetailSaga);
  yield fork(orthographyDetailHarfSaga);
  yield fork(orthographyDetailSoundSaga);
  yield fork(settingsSaga);
  yield fork(grammarArabicSentenceSaga);
}

sagaMiddleware.run(rootSaga);

const root = ReactDOM.createRoot(document.querySelector("#root") as Element);
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>
);
