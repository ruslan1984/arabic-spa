module.exports = {
  swcMinify: true,
  reactStrictMode: true,
  // experimental: {
  //   runtime: "nodejs",
  //   serverComponents: true,
  //   concurrentFeatures: true,
  //   // reactRoot: true,
  // },
  images: {
    loader: "akamai",
    path: "",
  },
  env: {
    REACT_APP_HOST: process.env.REACT_APP_HOST,
  },
  webpack(config, option) {
    config.module.rules.push({
      // test: ["jpg", "png", "svg"],
      // issuer: {
      //   test: ["js", "jsx", "ts", "tsx"],
      // },
      // use: ["url-loader"],
    });
    config.infrastructureLogging = { debug: /PackFileCache/ };
    return config;
  },
};
