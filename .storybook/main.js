const path = require("path");
const custom = require("../webpack.config.js");
const webpack = require("webpack");
// const webpackRules = require("../webpackRules");

module.exports = {
  stories: ["../src/**/*.stories.tsx"],
  addons: [
    "@storybook/addon-actions",
    "@storybook/addon-links",
    //'@storybook/addon-knobs/register',
    "@storybook/addon-storysource",
    "@storybook/addon-viewport",
    "yarn add --dev storybook-addon-next",
    "storybook-mobile",
    "storybook-css-modules-preset",
    "@storybook/addon-essentials",
    {
      name: "@storybook/addon-docs",
      options: {
        configureJSX: true,
        babelOptions: {},
        sourceLoaderOptions: null,
      },
    },
  ],
  features: { storyStoreV7: true },
  webpackFinal: async (config, { configType }) => {
    //config.plugins.push( new webpack.HotModuleReplacementPlugin() );

    config.module.rules.push({
      test: /\.stories\.tsx$/,
      loaders: [
        {
          loader: require.resolve("@storybook/source-loader"),
          options: { parser: "typescript" },
        },
      ],
      enforce: "pre",
    });
    config.module.rules.push({
      test: /\.tsx?$/,
      include: path.resolve(__dirname, "src"),
      use: [
        require.resolve("babel-loader"),
        {
          loader: require.resolve("react-docgen-typescript-loader"),
          options: {
            // Provide the path to your tsconfig.json so that your stories can
            // display types from outside each individual story.
            tsconfigPath: path.resolve(__dirname, "../tsconfig.json"),
          },
        },
      ],
    });
    // 2b. Run `source-loader` on story files to show their source code
    // automatically in `DocsPage` or the `Source` doc block.
    config.module.rules.push({
      test: /\.(stories|story)\.[tj]sx?$/,
      loader: require.resolve("@storybook/source-loader"),
      exclude: [/node_modules/],
      enforce: "pre",
    });
    config.module.rules.push({
      test: /.css$/i,
      use: [
        { loader: "style-loader" },
        {
          loader: "css-loader",
          options: {
            modules: true,
          },
        },
      ],
      exclude: /node_modules/,
    });
    config.module.rules.push({
      test: /\.(png|jp(e*)g|svg|gif)$/,
      use: ["url-loader"],
      exclude: /node_modules/,
    });
    return {
      ...config,
      resolve: custom.resolve,
      module: {
        ...config.module,
        rules: custom.module.rules,
      },
    };
  },
};
