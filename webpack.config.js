const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");

module.exports = {
  entry: "./src/index.tsx",
  devtool: "source-map",
  mode: "development",
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".json"],
    alias: {
      "@": path.resolve(__dirname, "src/"),
      "@screens": path.resolve(__dirname, "src/screens"),
      "@modules": path.resolve(__dirname, "src/modules"),
      "@adminScreen": path.resolve(__dirname, "src/screens/Admin"),
      "@authScreen": path.resolve(__dirname, "src/screens/Auth"),
      "@grammarScreen": path.resolve(__dirname, "src/screens/Admin/Grammar"),
      "@orthographyScreen": path.resolve(
        __dirname,
        "src/screens/Admin/Orthography"
      ),
      //"@orth": path.resolve( __dirname, 'src/screens/Admin/Orthography' ),
      "@store": path.resolve(__dirname, "src/store"),
      "@routes": path.resolve(__dirname, "src/RouteApp"),
      "@components": path.resolve(__dirname, "src/components"),
      "@pages": path.resolve(__dirname, "src/pages"),
      "@site": path.resolve(__dirname, "src/Site"),
      "@assets": path.resolve(__dirname, "src/assets"),
      "@helpers": path.resolve(__dirname, "src/helpers"),
      types: path.resolve(__dirname, "src/types"),
      components: path.resolve(__dirname, "src"),
    },
  },
  output: {
    path: path.join(__dirname, "../../public/admin1/"),
    filename: "script.js",
  },
  module: {
    rules: [
      {
        test: /.css$/i,
        use: [
          { loader: "style-loader" },
          {
            loader: "css-loader",
            options: {
              modules: true,
            },
          },
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jp(e*)g|svg|gif)$/,
        use: ["url-loader"],
        exclude: /node_modules/,
      },
      {
        test: /\.(js|ts)x?$/,
        loader: require.resolve("babel-loader"),
        exclude: /node_modules/,
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
  },
  performance: {
    hints: false,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      inject: false,
      hash: true,
    }),
    new Dotenv(),
  ],
};
