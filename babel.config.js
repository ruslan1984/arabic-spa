module.exports = {
  presets: [
    //[
    //  "@babel/preset-env", { targets: { node: "current" } }
    //],
    "@babel/preset-react",
    //"@babel/preset-typescript",
    //"@emotion/babel-preset-css-prop",
    [
      "next/babel",
      {
        "preset-react": {
          runtime: "automatic",
          importSource: "@emotion/react",
        },
      },
    ],
  ],
  //plugins: [
  //  "@babel/plugin-proposal-class-properties",
  //  "@emotion",
  //  "@babel/plugin-proposal-do-expressions",
  //  //"@emotion/babel-plugin",
  //],
  //env: {
  //  production: {
  //    plugins: ["@emotion"],
  //  },
  //  development: {
  //    plugins: [["@emotion", { sourceMap: true }]],
  //  },
  //},
};
